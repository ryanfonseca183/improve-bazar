<?php

namespace App\View\Components;

use Illuminate\View\Component;

class OrderProductsList extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $pedido;
    public $produtos;



    public function __construct($pedido, $produtos)
    {
        $this->pedido = $pedido;
        $this->produtos = $produtos;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.order-products-list');
    }
}
