<?php

namespace App\View\Components;

use Illuminate\View\Component;

class OrderSummary extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $summary;
    
    public function __construct($summary)
    {
        $this->summary = $summary;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.order-summary');
    }
}
