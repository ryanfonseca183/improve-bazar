<?php

namespace App\View\Components;

use Illuminate\View\Component;

class OrderProduct extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $pedido;
    public $produto;


    public function __construct($pedido, $produto)
    {
        $this->pedido = $pedido;
        $this->produto = $produto;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.order-product');
    }
}
