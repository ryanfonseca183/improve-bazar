<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ProductsSection extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $produtos;
    public $nome;
    
    public function __construct($produtos, $nome)
    {
        $this->produtos = $produtos;
        $this->nome = $nome;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.products-section');
    }
}
