<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ColorAttributeGroup extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $atr;
    public function __construct($attribute)
    {
        $this->atr = $attribute;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.color-attribute-group');
    }
}
