<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Step extends Component
{


    public $current;
    public $messages = ['Classificação', 'Características', 'Dados gerais', 'Fotos'];
    public $bg_colors = ['bg-orange', 'bg-dark-yellow', 'bg-light-yellow'];
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($current)
    {
        $this->current = $current;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.step', ['messages' =>$this->messages, 'bg_colors' =>$this->bg_colors]);
    }
}
