<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ProductsShopSection extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $produtos;
    public $action;
    public function __construct($produtos, $action)
    {
        $this->produtos = $produtos;
        $this->action = $action;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.products-shop-section');
    }
}
