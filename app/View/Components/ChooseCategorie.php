<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ChooseCategorie extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $categorias;
    public $selected;
    public $action;

    public function __construct($categorias, $action = 'next',  $selected = null)
    {
        $this->categorias = $categorias;
        $this->selected = $selected;
        $this->action = $action;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.choose-categorie');
    }
}
