<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ProductWrapper extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $produto;

    public function __construct($produto)
    {
        $this->produto = $produto;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.product-wrapper');
    }
}
