<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SearchFilter extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $atr;
    public $filters;

    public function __construct($atr, $filters)
    {
        $this->atr = $atr;
        $this->filters = $filters;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.search-filter');
    }
}
