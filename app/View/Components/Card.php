<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Card extends Component
{

    public $cardTitle;
    public $cardRole;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($cardTitle, $cardRole = null)
    {
        $this->cardTitle = $cardTitle;
        $this->cardRole = $cardRole;

        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.card');
    }
}
