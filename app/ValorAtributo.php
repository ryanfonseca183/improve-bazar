<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValorAtributo extends Model
{
    protected $table = "valores_atributos";
    public $timestamps = false;
    
    public function atributo() {
       return $this->belongsTo('App\Atributo', 'atributos_id');
    }
}
