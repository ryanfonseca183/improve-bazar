<?php

namespace App\Policies;

use App\EnderecoUsuario;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnderecoUsuarioPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return mixed
     */
    public function view(User $user, EnderecoUsuario $enderecoUsuario)
    {
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return mixed
     */
    public function update(User $user, EnderecoUsuario $enderecoUsuario)
    {
        return $user->id == $enderecoUsuario->users_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return mixed
     */
    public function delete(User $user, EnderecoUsuario $enderecoUsuario)
    {
        return $user->id == $enderecoUsuario->users_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return mixed
     */
    public function restore(User $user, EnderecoUsuario $enderecoUsuario)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return mixed
     */
    public function forceDelete(User $user, EnderecoUsuario $enderecoUsuario)
    {
        //
    }
}
