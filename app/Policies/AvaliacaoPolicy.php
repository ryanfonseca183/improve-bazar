<?php

namespace App\Policies;

use App\Avaliacao;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AvaliacaoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function store(User $user, $order, $produto)
    {
        if($produto->produtos_id != null) $id = $produto->produtos_id; else $id = $produto->id;

        // VERIFICA SE O USUÁRIO AUTENTICADO FEZ A COMPRA, SE O PRODUTO QUE ESTÁ SENDO AVALIADO É UM ITEM DA COMPRA E SE ALGUMA AVALIAÇÃO JÁ FOI FEITA.
        return $order->users_id == $user->id && $order->produtos()->where('itens_pedidos.produtos_id', $produto->id)->exists() && Avaliacao::where('users_id', $user->id)->where('produtos_id', $id)->doesntExist();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Avaliacao  $avaliacao
     * @return mixed
     */
    public function update(User $user, Avaliacao $avaliacao)
    {
        return $avaliacao->users_id = $user->id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Avaliacao  $avaliacao
     * @return mixed
     */
    public function forceDelete(User $user, Avaliacao $avaliacao)
    {
        //
    }
}
