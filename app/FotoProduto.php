<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoProduto extends Model
{
    protected $table = "fotos_produtos";
    public $timestamps = false;
}
