<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnderecoPedido extends Model
{
    protected $table = "enderecos_pedidos";
    public $timestamps = false;
}
