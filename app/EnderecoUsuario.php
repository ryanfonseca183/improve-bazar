<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnderecoUsuario extends Model
{
    protected $table = "enderecos_usuarios";
    public $timestamps = false;
}
