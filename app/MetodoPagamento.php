<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodoPagamento extends Model
{
    protected $table = "metodos_pagamento";
    public $timestamps = false;
}
