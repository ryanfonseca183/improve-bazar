<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;  

class Subcategoria extends Model
{
    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categorias_id');
    }
    public function atributos()
    {
        return $this->hasMany('App\Atributo', 'subcategorias_id');
    }
   public function produtos() {
       return $this->hasMany('App\Produto', 'subcategorias_id');
   }
   public function getCreatedAtAttribute($value) {
        $date = Carbon::parse($value);
        return $date->format("d-m-Y");
    }
}
