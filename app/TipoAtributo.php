<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtributo extends Model
{
    protected $table = 'tipos_atributos';
    public $timestamps = false;

    protected $filliable = ['backend_type', 'frontend_type', 'frontend_type_label'];
}
