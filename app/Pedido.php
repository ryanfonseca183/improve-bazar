<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    public function produtos()
    {
        return $this->belongsToMany('App\Produto', 'itens_pedidos', 'pedidos_id', 'produtos_id')->withPivot('id', 'quantidade', 'preco', 'total');
    }
    public function endereco()
    {
        return $this->hasOne('App\EnderecoPedido', 'pedidos_id');
    }
    public function metodoPagamento() {
        return $this->belongsTo('App\MetodoPagamento', 'metodos_pagamento_id');
    }
}
