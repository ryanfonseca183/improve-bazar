<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\EnderecoUsuario' => 'App\Policies\EnderecoUsuarioPolicy',
        'App\Avaliacao' => 'App\Policies\AvaliacaoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-dashboard-panel', function($user) {
            return $user->is_admin == 1;
        });
        Gate::define('view-order', function($user, $order) {
            return $order->users_id == $user->id;
        });
    }
}
