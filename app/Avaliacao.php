<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $table = "avaliacoes";
    public function customer() {
        return $this->belongsTo('App\User', 'users_id');
    }
    public function produto() {
        return $this->belongsTo('App\Produto');
    }
    public function getCreatedAtAttribute($value) {
        $value = date_parse($value);
        return $value['day'] ."/" . $value['month'] ."/". $value['year'] ." as ". $value['hour'] .":". $value['minute'];
    }
}
