<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    public $timestamps = false;
    protected $table = "favoritos";

    public function produtos() {
        return $this->BelongsToMany('App\Produto', 'favoritos_has_produtos', 'favoritos_id', 'produtos_id');
    }
}
