<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable 
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'primeiro_nome', 'cpf', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function enderecos()
    {
        return $this->hasMany('App\EnderecoUsuario', 'users_id');
    }
    public function getUpdatedAtAttribute($value) {
        $value = date_parse($value);
        return $value['day'] ."/" . $value['month'] ."/". $value['year'] ." as ". $value['hour'] .":". $value['minute'];
    }
    public function getEmailVerifiedAtAttribute($value) {
        if($value != null) {
            $value = date_parse($value);
            return $value['day'] ."/" . $value['month'] ."/". $value['year'] ." as ". $value['hour'] .":". $value['minute'];
        } else 
            return $value;
    }
    public function sacola() {
        return $this->hasOne('App\Sacola', 'users_id');
    }
    public function favoritos() {
        return $this->hasOne('App\Favorito', 'users_id');
    }
    public function pedidos() {
        return $this->hasMany('App\Pedido', 'users_id');
    }
    public function avaliacoes() {
        return $this->hasMany('App\Avaliacao', 'users_id');
    }
}
