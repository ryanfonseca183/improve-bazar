<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Categoria extends Model
{
    public function subcategorias()
    {
        return $this->hasMany('App\Subcategoria', 'categorias_id');
    }
    public function getCreatedAtAttribute($value) {
        $date = Carbon::parse($value);
        return $date->format("d-m-Y");
    }
}
