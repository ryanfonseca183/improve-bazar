<?php

namespace App\Http\Middleware;

use Closure;
use App\Produto;

class checkProductStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $produto = $request->produto;
        if($produto->produtos_id != null) {
            return redirect()->route('shop.produto', $produto->produtos_id);
        } else if(!$produto->is_visible || $produto->is_deleted) {
            return redirect()->route('shop.produto.unavaliable');
        } 
        return $next($request);
    }
}
