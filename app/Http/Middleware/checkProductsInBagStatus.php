<?php

namespace App\Http\Middleware;

use Closure;

class checkProductsInBagStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->sacola->produtos()->where('is_avaliable', 1)->count() == 0) {
            return redirect()->route('user.bag');
        }
        return $next($request);
    }
}
