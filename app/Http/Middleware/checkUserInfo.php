<?php

namespace App\Http\Middleware;

use Closure;

class checkUserInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        //Verifica se existe algum endereço de entrega cadastrado
        if(!$user->enderecos()->where('is_main', 1)->exists()) {
            return redirect()->route('shop.checkout.address');
        }
        //Verifica os dados pessoais do usuário
        if(!$user->cpf && !$user->name){
            return redirect()->route('shop.checkout.personal-info');
        }
        return $next($request);
    }
}
