<?php

namespace App\Http\Middleware;

use Closure;

class checkUserBag
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->sacola->produtos()->count() == 0) {
            return redirect()->route('user.bag.empty');
        }
        return $next($request);
    }
}
