<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Subcategoria;
use App\Categoria;


class SubcategoriaController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'categoria' => 'required|integer|exists:App\Categoria,id',
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
                'unique:App\Subcategoria,nome',
            ],
        ]);

        //Gera uma nova instancia de categoria e define os valores das propriedades
        $subcategoria = new Subcategoria();
        $subcategoria->categorias_id = $validated['categoria'];
        $subcategoria->nome = Str::title($validated['nome']);
        $subcategoria->save();

        //Executa o método index do controlador.
        return response()->json([
            'subcategoria' => $subcategoria
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategoria $subcategoria)
    {
        $validated = $request->validate([
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
                'unique:App\Subcategoria,nome',
            ],
        ]);
        $subcategoria->nome = Str::title($validated['nome']);
        $subcategoria->save();

        return response()->json([
            'subcategoria' => $subcategoria
        ]);
    }
    public function updateParent(Request $request, Subcategoria $subcategoria)
    {
        $validated = $request->validate([
            'categoria' => 'required|integer|exists:App\Categoria,id',
        ]);
        $subcategoria->categorias_id = $validated['categoria'];
        $subcategoria->save();

        return response()->json([
            'subcategoria' => $subcategoria
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategoria $subcategoria)
    {
        $produtos = $subcategoria->produtos;
        foreach($produtos as $prod) {
            foreach($prod->fotosProduto as $fto) {
                Storage::delete($fto->caminho);
            }
        }
        $subcategoria->delete();
    }
}
