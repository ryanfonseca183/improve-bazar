<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Produto;
use App\Categoria;
use App\Subcategoria;
use App\Atributo;
use App\FotoProduto;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Validator;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $produtos = Produto::select('id', 'nome', 'sku', 'preco', 'is_avaliable', 'is_visible', 'subcategorias_id')
        ->where('produtos_id', null)
        ->where('is_deleted', 0)
        ->get();
        return view('dashboard.produto.produto-index', ['produtos' => $produtos]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //Deleta possíveis fotos do diretório temporário
        $files = Storage::files('temp-photos');
        if(count($files) != 0) {
            Storage::delete($files);
        }

        if($request->has('subcategoria')) {
            $storeProduto = session()->get('storeProduto');
            //Recupera a subcategoria selecionada
            $subcategoria = Subcategoria::findOrFail($request->query('subcategoria'));

            //Recupera a categoria da subcategoria selecionada
            $categoria = $subcategoria->categoria;

            //Recupera os atributos da subcategoria selecionada
            $atributos = $subcategoria->atributos()->where('is_visible', 1)->get();

            //Recupera os atributos configuráveis
            $configurables = $atributos->where('is_configurable', 1);

            //Recupera os atributos simples
            $simples = $atributos->where('is_configurable', 0);

            //Armazena os dados em sessão
            $storeProduto['subcategoria'] = $subcategoria;
            $storeProduto['categoria'] = $categoria;
            $storeProduto['simple'] = $simples;
            $storeProduto['configurable'] = $configurables;
            $storeProduto['valGenAttr'] = [];
            $storeProduto['valSimAttr'] = [];
            $storeProduto['variations'] = [];
            $storeProduto['photos'] = [];

            session(['storeProduto' => $storeProduto]);

            return view('dashboard.produto.produto-create');
        } else {
            $storeProduto = [];
            $categorias = Categoria::withCount('subcategorias')->get();
            $storeProduto['categorias'] = $categorias;

            $request->session()->put('storeProduto', $storeProduto);

            return view('dashboard.produto.produto-create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storeProduto = $request->session()->get('storeProduto');
        $valGenAttr = $storeProduto['valGenAttr'];
        $valSimAttr = $storeProduto['valSimAttr'];
        $variations = $storeProduto['variations'];
        //Armazenando o produto no banco
        $produto = new Produto();
        $produto->subcategorias_id = $storeProduto['subcategoria']->id;
        $produto->nome = $valGenAttr['nome'];
        $produto->descricao = isset($valGenAttr['descricao']) ? $valGenAttr['descricao'] : null;
        $produto->preco = count($variations) != 0 ? 0 : $valGenAttr['preco'];
        $produto->sku = count($variations) != 0 ? "" : $valGenAttr['sku'];
        $produto->product_type = count($variations) != 0 ? 2 : 1;
        $produto->quantidade = count($variations) != 0 ? 1 : $valGenAttr['estoque'];
        $produto->peso = $valGenAttr['peso'];
        $produto->comprimento = $valGenAttr['comprimento'];
        $produto->largura = $valGenAttr['largura'];
        $produto->altura = $valGenAttr['altura'];
        $produto->save();

        //CRIA AS VARIAÇÕES COM BASE NOS ATRIBUTOS CONFIGURÁVEIS
        if(count($variations) != 0) {
            foreach($variations as $var) {
                $variacao = $produto->replicate();
                foreach($var['valores'] as $val) {
                    if($val[2]) $attrValue = $val[2]; else if($val[1] != "Não") $attrValue = $val[1];
                    $variacao->nome .= (" " . $attrValue);
                }
                $variacao->produtos_id = $produto->id;
                $variacao->sku = $var['sku'];
                $variacao->product_type = 1;
                $variacao->quantidade = $var['quantidade'];
                $variacao->preco = $var['preco'];
                $variacao->save();

                //DEFININDO O VALOR DOS ATRIBUTOS SIMPLES PARA CADA VARIAÇÃO
                foreach($valSimAttr as $key => $val) {
                    $variacao->atributos()->attach($key, ['valor' => $val['valor'], 'label' => $val['label']]);
                }
                $valores = $var['valores'];
                //DEFININDO O VALOR DOS ATRIBUTOS CONFIGURÁVEIS PARA A VARIAÇÃO
                foreach($valores as $val) {
                    $variacao->atributos()->attach($val[0], ['valor' => $val[1], 'label' => $val[2]]);
                }
            }
        } else {
            foreach($valSimAttr as $key => $val) {
                $produto->atributos()->attach($key, ['valor' => $val['valor'], 'label' => $val['label']]);
            }
        }
        $path = $storeProduto['photos']['path'];
        $main = $storeProduto['photos']['main'];
        foreach($path as $key => $url) {
            //Movendo as fotos do produto para o diretório permanente
            $name = strstr($url, '/');
            $old = "temp-photos" . $name;
            $new = "product-photos" . $name;
            Storage::move($old, $new);

            //Armazenando o caminho das fotos no banco
            $foto = new FotoProduto();
            $foto->produtos_id =  $produto->id;
            $foto->caminho = $new;
            $foto->is_main = $key == $main ? 1 : 0;
            $foto->save();
        }
        $request->session()->forget('storeProduto');

        return redirect()->route('produto.show', [$produto->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        if($produto->produtos_id) {
            return redirect()->route('produto.show', $produto->produtos_id);
        }
        return view('dashboard.produto.produto-details', ['produto' => $produto]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto)
    {
        // VERIFICANDO SE O PRODUTO POSSUI ALGUM PEDIDO
        $hasOrders = false;
        if($produto->product_type == 2) {
            foreach($produto->filhos as $filho) {
                if($filho->pedidos()->count() > 0) {
                    $hasOrders = true;
                   break;
                }
            }
        } else if($produto->pedidos()->exists()) $hasOrders = true;

        if($hasOrders) {
            // ALTERANDO O STATUS DO PRODUTO PRA "DELETEADO"
            $produto->is_deleted = 1;
            $produto->save();

            //ALTERANDO O ESTADO DAS CONFIGURAÇÕES PARA DELETADO
            $produto->filhos()->update(['is_deleted' => 1]);

            // REMOVENDO O PRODUTO DAS SACOLAS DE COMPRA
            if($produto->filhos()->exists()) 
                foreach($produto->filhos as $filho) $filho->sacolas()->detach();
            else 
                $produto->sacolas()->detach();

            // REMOVENDO O PRODUTO DOS FAVORITOS
            $produto->favoritos()->detach();

        } else {
            //DELETANDO AS FOTOS DO PRODUTO
            foreach($produto->fotosProduto as $ft) $path[] = $ft->caminho;
            Storage::delete($path);

            //DELETANDO O PRODUTO
            $produto->delete();
        }
        return redirect()->route('produto.index');
    }
    // Atributos gerais
    public function setGeneralAttribute(Request $request) {
    
        $storeProduto = $request->session()->get('storeProduto');

        $nome = $request->input('nome');
        $valor = $request->input('valor');

        $storeProduto['valGenAttr'][$nome] = $valor;
        
        session(['storeProduto' => $storeProduto]);
    }
    public function updateGeneralAttribute(Produto $produto, Request $request) {
        $name = $request->name;
        $value = $request->value;
        $cascade = $request->boolean('cascade');
        $produto->$name = $value;
        if($produto->isDirty()) {
            $produto->save();
            if($cascade == true) $produto->filhos()->update([$name => $value]);
        }
    }
    public function updateMultipleGeneralAttributes(Request $request) {
        $produto = Produto::findOrFail($request->product_id);
        $validationRules = [
            'nome' => 'required|string|max:255|min:6',
            'descricao' => 'nullable|string',
            'quantidade' => 'required|integer', 
            'preco' => 'required|numeric|min:1',   
            'same_price' => "nullable|boolean",
            'promo_value' => 'bail|required|integer|between:0,100',
            'promo_starts_at' => 'bail|nullable|required_unless:promo_value,0|date|after_or_equal:' . $produto->promo_starts_min,
            'promo_ends_at' => 'bail|nullable|required_unless:promo_value,0|date|after:promo_starts_at',
        ];
        $applicableRules = [];
        $data = $request->except('_token', '_method', 'product_id');
        //RECUPERA AS REGRAS DE VALIDAÇÃO APLICÁVEIS DEPENDENDO DOS CAMPOS RECEBIDOS
        foreach($data as $key => $value) {
            if(Arr::has($validationRules, $key)) $applicableRules[$key] = $validationRules[$key];
        }
        //REALIZA A VALIDAÇÃO DOS CAMPOS
        $validator = Validator::make($data, $applicableRules);

        //RETORNA OS ERROS DE VALIDAÇÃO
        if($validator->fails()) return response()->json($validator->errors(), 400);

        $parente = $produto->parente;

        $update = [];
        //ATUALIZA OS VALORES DOS CAMPOS
        foreach($data as $key => $value) {
            switch($key) {
                case 'same_price': 
                    if($value == true) $update['preco'] = $data['preco'];
                break;
                case 'quantidade':
                    $produto->quantidade = $value;
                    if($value <= 0) {
                        $produto->is_avaliable = 0; 
                        if($parente) {
                            if($parente->filhos()->sum('quantidade') > 0) $parente->is_avaliable = 1; else $parente->is_avaliable = 0;
                            $parente->save();
                        } 
                    } else {
                        $produto->is_avaliable = 1;
                    }
                break;
                case 'promo_value':
                    $produto->promo_value = $value;
                    $update['promo_value'] = $data['promo_value'];
                    $update['promo_starts_at'] = $data['promo_starts_at'];
                    $update['promo_ends_at'] = $data['promo_ends_at'];
                break;
                default: $produto->$key = $value;
            }
        }

        if($parente) $auxiliar =  $parente; else $auxiliar = $produto;
        if(!empty($update) && $auxiliar->product_type == 2) $auxiliar->filhos()->update($update);

        //SALVE AS ALTERAÇÕES
        $produto->save();

        return response()->json($data);
    }

    // Atributos simples
    public function setSimpleAttribute(Request $request) {
        $storeProduto = $request->session()->get('storeProduto');
       
        $idAtr = $request->input('id');
        $valor = Str::title($request->input('valor'));
        $label = Str::title($request->input('label'));
        $storeProduto['valSimAttr'][$idAtr] = ['valor' => $valor, 'label' => $label];

        session(['storeProduto' => $storeProduto]);

        return response()->json([
            'valSimAttr' =>  $storeProduto['valSimAttr'],
        ]);
    }

    // Atributos configuráveis
    public function setVariation(Request $request) {
        $storeProduto = $request->session()->get('storeProduto');
        $unsavedList = $request->input('list');

        //Monta uma string com os valores recebidos
        $uniqueUnsaved = "";
        foreach($unsavedList['valores'] as $unsavedAtr) {
            $uniqueUnsaved = $uniqueUnsaved . implode($unsavedAtr);
        }
        //Monta uma string com os valores em sessão
        foreach($storeProduto['variations'] as $listVal) {
            $uniqueSave = "";
            foreach($listVal['valores'] as $atr) {
                $uniqueSave = $uniqueSave . implode($atr);
            }
            //Verifica se a string dos valores recebidos é igual a string dos valores em sessão
            if($uniqueUnsaved == $uniqueSave) {
                return response()->json([
                    'hasList' => true,
                ]);
            }
        }
        $storeProduto['variations'][] = $unsavedList;
        $indice = array_key_last($storeProduto['variations']);

        session(['storeProduto' => $storeProduto]);

        return response()->json([
            'hasList' => false,
            'idList' => $indice,
            'listVal' =>  $unsavedList,
        ]);
    }
    public function setVariationsGeneralAttribute(Request $request) {
        $storeProduto = $request->session()->get('storeProduto');
        $values = $request->input('values');
        $name = $request->input('name');
        $variations = $storeProduto['variations'];
        foreach($values as $val) {
            $variations[$val[0]][$name] = $val[1];
        }
        $storeProduto['variations'] = $variations;
        session(['storeProduto' => $storeProduto]);
    }
    public function getVariation(Request $request, $id) {
        $valores = $request->all();
        $parente = Produto::find($id);
        $produtos = $parente->filhos;
        foreach($valores as $key => $val) {
            $produtos = $produtos->filter(function ($produto) use ($val, $key) {
                if($produto->atributos()->where('atributos.label', $key)->first() != null) {
                    return $produto->atributos()->where('atributos.label', $key)->first()->pivot->valor == $val;
                }
            });
        }
        return json_encode($produtos->first());
    }
    public function destroyVariation(Request $request) {
        $id = $request->input('id');
       
        $storeProduto = session()->get('storeProduto');
        unset($storeProduto['variations'][$id]);
        session(['storeProduto' => $storeProduto]);
    }

    // Fotos temporárias do produtos
    public function setPhotos(Request $request) {
        $storeProduto = session()->get('storeProduto');
        $files = $request->file('photos');
        $processedFiles = [];

        foreach($files as $file) {
            $path = $file->store('temp-photos');
            $storeProduto['photos']['path'][] = $path;
            $idPhoto = array_key_last($storeProduto['photos']['path']);
            $processedFiles[] = [$path, $idPhoto];
        }
        if(!isset($storeProduto['photos']['main'])) {
            $firstPhoto = array_key_first($storeProduto['photos']['path']);
            $storeProduto['photos']['main'] = $firstPhoto;
        }

        session(['storeProduto' => $storeProduto]);
        return response()->json([
            'path' => $processedFiles,
            'main' => $storeProduto['photos']['main'],
        ]);
    }
    public function setMainPhoto(Request $request) {
        $id = $request->input('idPhoto');
        $storeProduto = session()->get('storeProduto');

        $storeProduto['photos']['main'] = $id;
        
        session(['storeProduto' => $storeProduto]);
        return response()->json([
            'main' => $id,
        ]);
    }
    public function destroyPhoto(Request $request) {
        $id = $request->input('idPhoto');
        $storeProduto = session()->get('storeProduto');
        $main = $storeProduto['photos']['main'];

        //Deleta do storage
        $photo  = $storeProduto['photos']['path'][$id];
        Storage::delete($photo);

        //Deleta do array em sessão
        unset($storeProduto['photos']['path'][$id]);
        if($id == $main) {
            $storeProduto['photos']['main'] = null;
        }
        session(['storeProduto' => $storeProduto]);

        return response()->json([
            'main' =>  $storeProduto['photos']['main'],
        ]);
    }

    //Fotos permanentes dos produtos
    public function setPhotosToDatabase(Request $request, Produto $produto) {
        $files = $request->file('photos');
        foreach($files as $file) {
            $path = $file->store('product-photos');
            $ft = new FotoProduto();
            $ft->produtos_id = $produto->id;
            $ft->caminho = $path;
            $ft->is_main = 0;
            $ft->save();
            $processedFiles[] = [$path, $ft->id];
        }
        return response()->json([
            'path' => $processedFiles,
        ]);
    }
    public function updateMainPhoto(Produto $produto, Request $request) {
        $mainPhoto = $produto->fotosProduto()->where('is_main', 1)->first();
        $mainPhoto->is_main = 0;
        $mainPhoto->save();

        $photo = $produto->fotosProduto()->where('fotos_produtos.id', $request->idPhoto)->first();
        $photo->is_main = 1;
        $photo->save();
    }
    public function destroyPhotoFromDatabase(Request $request) {
        $photo = FotoProduto::findOrFail($request->idPhoto);
        if($photo->is_main) $hasMain = 0; else $hasMain = 1;
        Storage::delete($photo->caminho);
        $photo->delete();
        return response()->json([
            'hasMain' => $hasMain,
        ]);
    }
}
