<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ValorAtributo;


class ValorAtributoController extends Controller
{
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $atributo)
    {
        $valor = new ValorAtributo();
        $valor->atributos_id =  $atributo;
        $valor->valor = $request->get('valor');
        $valor->label = $request->get('label');
        $valor->save();

        return response()->json([
            'valor' => $valor->valor,
            'label' => $valor->label,
            'id' => $valor->id,
        ]);

    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $valor = ValorAtributo::findOrFail($id);
        $valor->delete();
        
        return response()->json([
            'id' => $valor->atributo->id,
            'is_configurable' =>  $valor->atributo->is_configurable,
        ]);
    }
}
