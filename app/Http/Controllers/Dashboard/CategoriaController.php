<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Str;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Recupera todas as categorias de produtos
        $categorias = Categoria::all();
         //Chama a view de listagem e cadastro das categorias passando os valores do modelo como argumento
         return view('dashboard.categoria.categoria-index', ['categorias' => $categorias]);
    }
    public function getCategorias() {
        $categorias = Categoria::withCount('subcategorias')->get();
        return response()->json([
            'categorias' => $categorias
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
                'unique:App\Categoria,nome',
            ]
        ]);
        //Gera uma instancia do modelo e define as propriedades
        $categoria = new Categoria();
        $categoria->nome = Str::title($validated['nome']);

        //Insere a categoria no banco
        $categoria->save();
        
        //Executa o método index do controlador.
        return response()->json([
            'categoria' => $categoria,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        $validated = $request->validate([
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
                'unique:App\Categoria,nome',
            ]
        ]);
        //Atualiza a categoria
        $categoria->nome = Str::title($validated['nome']);
        $categoria->save();

        return response()->json([
            'categoria' => $categoria,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
        $subcategorias = $categoria->subcategorias;

        foreach($subcategorias as $sub) {
            $produtos = $sub->produtos;
            foreach($produtos as $prod) {
                foreach($prod->fotosProduto as $fto) {
                    Storage::delete($fto->caminho);
                }
            }
        }
       
        $categoria->delete();
    }
    public function getSubcategorias(Request $request) {
        //Recupera a categoria selecionada
        $categoria = Categoria::findOrFail($request->query('categoria'));

        //Recupera as subcategorias da categoria selecionada
        $subcategorias = $categoria->subcategorias;

       return response()->json([
            'subcategorias' => $subcategorias,
       ]);
    }
}
