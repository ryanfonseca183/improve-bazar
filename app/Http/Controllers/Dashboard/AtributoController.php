<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Categoria;
use App\TipoAtributo;
use App\ValorAtributo;
use App\Atributo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AtributoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.atributo.atributo-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Recupera as categorias de produtos
        $categorias = Categoria::withCount('subcategorias')->get();
        
        //Recupera os tipos de atributos
        $tiposAtributos = TipoAtributo::all();
        return view('dashboard.atributo.atributo-create', ['categorias' => $categorias, 'tiposAtributos' => $tiposAtributos]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    public function store(Request $request) {
        //Verifica se os valores dos campos, se preenchidos, são válidos
        $validated = $request->validate([
            'subcategoria' => 'required|integer|exists:App\Subcategoria,id',
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
            ],
            'tipoAtr' => 'required|integer|exists:App\TipoAtributo,id',   
            'is_configurable' => 'exclude_if:tipoAtr,3',
        ]);
        // VERIFICA SE O ATRIBUTO JÁ FOI CADASTRADO PARA A SUBCATEGORIA SELECIONADA
        if(Atributo::where('subcategorias_id', $validated['subcategoria'])->where('nome', $validated['nome'])->exists()) {
            return redirect()->back()->withErrors(['nome' => "O atributo dado já existe para a subcategoria selecionada"]);
        }

        //Gera uma instancia do atributo e define os valores das propriedades
        $atributo = new Atributo();
        $atributo->subcategorias_id = $validated['subcategoria'];
        $atributo->nome = $validated['nome'];
        $replaced = Str::snake($validated['nome']);
        $atributo->label = $replaced;
        $atributo->tipos_atributos_id =  $validated['tipoAtr'];
        $atributo->is_visible = $request->has('is_visible') ? 1 : 0;
        $atributo->is_required = $request->has('is_required') ? 1 : 0;
        $atributo->is_configurable = isset($validated['is_configurable']) ? 1 : 0;
        $atributo->save();

        //Redireciona a página de cadastro dos atributos
        return redirect()->route('atributo.show', [$atributo->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Atributo $atributo) {
        return view('dashboard.atributo.atributo-show', ['atributo' => $atributo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Atributo $atributo)
    {
        $categorias = Categoria::withCount('subcategorias')->get();
        $tipos = TipoAtributo::all();

        //Recupera a categoria da subcategoria do atributo
        $categoria = $atributo->subcategoria->categoria;

        //Recupera as subcategorias da categoria
        $subcategorias = $categoria->subcategorias;
        return view('dashboard.atributo.atributo-update', ['atributo' => $atributo, 'categorias' => $categorias,    'subcategorias' => $subcategorias, 'tiposAtributo' => $tipos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Atributo $atributo)
    {
        //Verifica se os valores dos campos, se preenchidos, são válidos
        $validated = $request->validate([
            'subcategoria' => 'required|integer|exists:App\Subcategoria,id',
            'nome' => [
                'required',
                'regex:/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/',
                'max:60',
            ],
            'tipoAtr' => 'required|integer|exists:App\TipoAtributo,id',   
        ]);
        //VERIFICANDO SE O ATRIBUTO JÁ EXISTE PARA A SUBCATEGORIA DADA
        if(Atributo::where('subcategorias_id', $validated['subcategoria'])->where('nome', $request->nome)->where('id', "!=", $atributo->id)->exists()) {
            return redirect()->back()->withErrors(['nome' => "O atributo dado já existe para a subcategoria selecionada"]);
        }

        // Se a subcategoria ou tipo do atributo mudar, os produtos configuráveis são deletados e os produtos simples tem o atributo desvinculado.
        if($atributo->subcategorias_id != $validated['subcategoria'] || $atributo->tipos_atributos_id != $validated['tipoAtr']) {
            if($atributo->is_configurable == 1) {
                $this->delete_configurable_products($atributo);
            } else {
                $atributo->produtos()->detach();
            }
        }
        //Se o tipo selecionado for diferente do tipo cadastrado, os valores pré-definidos do atributo são deletados.
        if($atributo->tipos_atributos_id != $validated['tipoAtr']) {
            if($validated['tipoAtr'] == 3 && $atributo->is_configurable == 1) {
                $atributo->is_configurable == 0;
            }
            ValorAtributo::where('atributos_id', $atributo->id)->delete();
        }
        $atributo->nome = $validated['nome'];
        $replaced = Str::snake($validated['nome']);
        $atributo->label = $replaced;
        $atributo->subcategorias_id = $validated['subcategoria'];
        $atributo->tipos_atributos_id =  $validated['tipoAtr'];
        $atributo->is_visible = $request->has('is_visible') ? 1 : 0;
        $atributo->is_required = $request->has('is_required') ? 1 : 0;
        $atributo->save();

        //Redireciona a página de cadastro dos atributos
        return redirect()->route('atributo.show', [$atributo->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Atributo $atributo)
    {
        if($atributo->is_configurable == 1) {
            $this->delete_configurable_products($atributo);
        } 
        $atributo->delete();
        return redirect()->route('atributo.index');
    }
    public function getAtributos(Request $request) {
        $atributos = DB::table('atributos')
            ->join('subcategorias', 'subcategorias.id', '=', 'atributos.subcategorias_id')
            ->join('tipos_atributos', 'tipos_atributos.id', '=', 'atributos.tipos_atributos_id')
            ->select('atributos.*', 'subcategorias.nome as subcategoria', 'tipos_atributos.frontend_type_label as tipo')
            ->get();
        return response()->json([
            'atributos' => $atributos
        ]);
    }
    public function setConfig(Request $request, $idAtr) {
        $atributo = Atributo::findOrFail($idAtr);
        $config = $request->get('config_name');
        $is_checked = $request->get('is_checked');
        $atributo->$config = $is_checked == "true" ? 1 : 0;
        $atributo->save();
    }

    public function delete_configurable_products($atributo) {
        $produtos = $atributo->produtos()->whereNotNull('produtos.produtos_id')->get();
        $configurableProducts = collect([]);
        foreach($produtos as $prod) {
            $parente = $prod->parente;
            if(!$configurableProducts->contains($parente)) {
                $configurableProducts->push($parente);
            }
        }
        foreach($configurableProducts as $prod) {
            foreach($prod->fotosProduto as $fto) {
                Storage::delete($fto->caminho);
            }
            $prod->delete();
        }
    }
}
