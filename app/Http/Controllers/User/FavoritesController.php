<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produto;
class FavoritesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        $produtos = $request->user()->favoritos->produtos()->paginate(10);
        return view('user.favorites.user-favorites', ['produtos' => $produtos]);
    }
    //PRODUTOS NA SACOLA
    public function addProductToFavorites(Request $request, Produto $produto) {
        //BUSCA O PRODUTO NA LISTA
        $has = $request->user()->favoritos->produtos()->where('favoritos_has_produtos.produtos_id', $produto->id)->first();
        //SE AINDA NÃO ESTIVER NA LISTA, É ADICIONADO
        if(!isset($has)) $request->user()->favoritos->produtos()->attach($produto->id);
       
        return redirect()->route('user.favorites');
    }
    public function removeProductFromFavorites(Request $request, Produto $produto) {
        $request->user()->favoritos->produtos()->detach($produto->id);

        return redirect()->route('user.favorites');
    }
}
