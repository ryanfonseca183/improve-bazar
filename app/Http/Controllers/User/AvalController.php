<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produto;
use App\Pedido;
use App\Avaliacao;

class AvalController extends Controller
{
    public function create(Request $request, Pedido $order, Produto $produto) {
       $this->authorize('store', [Avaliacao::class, $order, $produto]);
        return view('user.orders.avaliacoes.avaliacao-create', ['pedido' => $order, 'produto' => $produto]);
    }
    public function store(Request $request, Pedido $order, Produto $produto) {
        $validated = $request->validate([
            'valor' => ['required', 'integer', 'in:1,2,3,4,5'],
            'titulo' => ['required', 'string', 'max:60'],
            'mensagem' => ['required', 'string', 'max:255'],
        ]);
        if($produto->parente) $produto = $produto->parente; 
        $avaliacao = new Avaliacao();
        $avaliacao->users_id = $request->user()->id;
        $avaliacao->produtos_id = $produto->id;
        $avaliacao->valor = $validated['valor'];
        $avaliacao->titulo = e($validated['titulo']);
        $avaliacao->mensagem = e($validated['mensagem']);
        $avaliacao->save();

        return redirect()->route('user.orders.show', ['order' => $order->id]);
    }
    public function edit(Pedido $order, Produto $produto, Avaliacao $avaliacao) {
       $this->authorize('update', [$avaliacao, $order, $produto]);

        return view('user.orders.avaliacoes.avaliacao-edit', ['pedido' => $order, 'produto' => $produto, 'avaliacao' => $avaliacao]);
    }
    public function update(Request $request, Pedido $order, Produto $produto, Avaliacao $avaliacao) {
        $validated = $request->validate([
            'valor' => ['required', 'integer', 'in:1,2,3,4,5'],
            'titulo' => ['required', 'string', 'max:60'],
            'mensagem' => ['required', 'string', 'max:255'],
        ]);
       $avaliacao->valor = $validated['valor'];
       $avaliacao->titulo = e($validated['titulo']);
       $avaliacao->mensagem = e($validated['mensagem']);
       $avaliacao->save();

       return redirect()->route('user.orders.show', ['order' => $order->id]);
    }
    public function destroy(Request $request, Pedido $order, Produto $produto, Avaliacao $avaliacao) {
        $avaliacao->delete();

        return redirect()->route('user.orders');
    }
    public function index(Request $request, Produto $produto) {
        $avaliacoes = Avaliacao::where('produtos_id', $produto->id)->paginate(3);
        
        return response()->json([
            'avaliacoes' => $avaliacoes->toJson(),
        ]);
    }
}
