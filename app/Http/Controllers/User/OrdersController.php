<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produto;
use App\Pedido;
use Illuminate\Support\Facades\Gate;

class OrdersController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        $orders = $request->user()->pedidos()->orderBy('id', 'desc')->paginate(5);
        return view('user.orders.user-orders', ['orders' => $orders]);
    }
    public function show(Pedido $order) {
        Gate::authorize('view-order', $order);
        return view('user.orders.user-order-details', ['pedido' => $order]);
    }
}
