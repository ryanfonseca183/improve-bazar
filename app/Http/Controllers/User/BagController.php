<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\EnderecoUsuario;
use FlyingLuscas\Correios\Client;
use FlyingLuscas\Correios\Service;
use Carbon\Carbon;
use App\Categoria;
use App\Produto;

class BagController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        //Recupera os produtos na sacola
        $produtos = $request->user()->sacola->produtos()->orderBy('is_avaliable', 'desc')->get();
        
        //Recupera os produtos disponíveis para compra; 
        $avaliable = $produtos->where('is_avaliable', 1);
        
        //Recupera o endereço de entrega principal, caso haja
        $endereco = $request->user()->enderecos()->where('is_main', 1)->first();

        $sedex['price'] = $sedex['deadline'] = $sedex['date'] = null;
        if($avaliable->count() > 0) {
            if(isset($endereco)) $sedex = $this->calcShipping($endereco->cep, $avaliable);
        }
        //Obtendo os valores totais da sacola
        $summary = $this->calcSummary($avaliable, $sedex);
        $summary['frete']['sedex'] = $sedex;
       
        //Retorna a view de exibição da sacola
        return view('user.bag.user-bag', ['produtos' => $produtos, 'endereco' => $endereco, 'summary' => $summary]);
    }
    public function empty() {
        return view('user.bag.user-bag-empty');
    }
   

    //ENDEREÇOS DE ENTREGA
    public function updateBagAddress(Request $request, EnderecoUsuario $address){
        //Recupera o endereço padrão e define a propriedade is_main pra 0
        $request->user()->enderecos()->update(['is_main' => 0]);
       
        //Define o endereço selecionado como padrão
        $address->is_main = 1;
        $address->save();

        //Recupera os produtos na sacola
        $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();
        if($produtos->count() > 0) {
            //Calcula o frete para o endereço selecionado
            $sedex = $this->calcShipping($address->cep, $produtos);
        }
        
        //Calcula os valores totais da sacola
        $summary = $this->calcSummary($produtos, $sedex);
        $summary['frete']['sedex'] = $sedex;
        return response()->json([
            'address' =>  $address,
            'summary' => $summary,
        ]);
    }

    //PRODUTOS NA SACOLA
    public function addProductToBag(Request $request) {
        $produto = Produto::findOrFail($request->produto_id);

        //BUSCA O PRODUTO NA SACOLA DE COMPRAS
        $bagProduct = $request->user()->sacola->produtos()->where('sacolas_has_produtos.produtos_id', $produto->id)->first();
        //SE AINDA NÃO ESTIVER NA SACOLA, É ADICIONADO
        if(!isset($bagProduct)) {
            $quantidade = (int) $request->quantidade;
            if($produto->quantidade < $quantidade)  $quantidade = $produto->quantidade;
            $request->user()->sacola->produtos()->attach($produto->id, ['quantidade' => $quantidade]);

        //SE ESTIVER NA SACOLA, É INCREMENTADA A QUANTIDADE
        } else {
            $quantidade = $bagProduct->pivot->quantidade + $request->quantidade;
            if($produto->quantidade < $quantidade) $quantidade = $produto->quantidade;
                $request->user()->sacola->produtos()->updateExistingPivot($produto->id, ['quantidade' => $quantidade]);
        }
        return redirect()->route('user.bag');
    }
    public function removeProductFromBag(Request $request, Produto $produto) {
        $request->user()->sacola->produtos()->detach($produto->id);
        return redirect()->route('user.bag');
    }
    public function updateProductBagQuantity(Request $request, Produto $produto) {
        //Recuperando a quantidade informada
        $quantidade = (int) $request->quantidade;

        //Recuperando a ação a ser tomada  
        $action = $request->action;

        //Verificando se o produto existe na sacola
        $produto = $request->user()->sacola->produtos()->where('sacolas_has_produtos.produtos_id', $produto->id)->first();
        if(isset($produto)) {
            //Se sim, então é verificado qual ação se deve tomar
            switch($action) {
                case 1: case 2: $quantidade_total = $produto->pivot->quantidade + $quantidade; break;
                case 3: $quantidade_total = $quantidade;
            }
            if($quantidade_total <= $produto->quantidade && $quantidade_total > 0) {
                //Atualizando a quantidade do produto na sacola
                $request->user()->sacola->produtos()->updateExistingPivot($produto->id, ['quantidade' => $quantidade_total]);
                //Recuperando o endereço de entrega principal
                $endereco = $request->user()->enderecos()->where('is_main', 1)->first();

                //Calculando o preço total para o produto
                $produto->precoTotal = $produto->preco * $quantidade_total;
                $produto->precoComDesconto = $produto->desconto * $quantidade_total;

                //Recuperando os produtos na sacola
                $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();
                $sedex['price'] = 0;

                //Calculando o valor do frete para o endereço selecionado
                if(isset($endereco)) $sedex = $this->calcShipping($endereco->cep, $produtos);

                //Calculando os valores totais da sacola
                $summary = $this->calcSummary($produtos, $sedex);
                $summary['frete']['sedex'] = $sedex;

                return response()->json([
                    'summary' => $summary,
                    'produto' => $produto
                ]);
            }
        }
    }
   
    public function calcSummary($produtos, $sedex) {
        //Calculando o preço total para cada produto na sacola
        $produtos = $produtos->map(function ($produto, $key) {
            $produto->precoTotal = $produto->pivot->quantidade  * $produto->preco;
            $produto->precoComDesconto = $produto->pivot->quantidade  * $produto->desconto;
            return $produto;
        });
        
        //Calculando o preço total da sacola
        $subTotal = $produtos->sum('precoComDesconto');
        $subTotalSemDesconto = $produtos->sum('precoTotal');
        $total = $sedex['price'] + $subTotal;

        return ['subTotal' => $subTotal, 'subTotalSemDesconto' => $subTotalSemDesconto, 'total' => $total];
    }
    public function calcShipping($cep, $produtos) {
        $correios = new Client;
        $correios->freight()
        ->origin('35574-061')
        ->destination($cep)
        ->services(Service::SEDEX, Service::PAC);

        foreach($produtos as $prod) $correios->freight()->item($prod->largura, $prod->altura, $prod->comprimento, $prod->peso, $prod->pivot->quantidade);
        $frete = $correios->freight()->calculate();
        $sedex = $frete[0];
        
        $td = Carbon::now()->locale('pt-br');
        $td->add('1 day');
        $sedex['date'] = $td->toDateString();
        return $sedex;
    }
}
