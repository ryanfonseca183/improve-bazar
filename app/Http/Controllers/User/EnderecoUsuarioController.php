<?php

namespace App\Http\Controllers\User;

use App\EnderecoUsuario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use FlyingLuscas\Correios\Client;
use FlyingLuscas\Correios\Service;

class EnderecoUsuarioController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [];
        if($request->has('cep')) {
            $cep = $request->input('cep');
            if(strlen($cep) == 9) {
                $correios = new Client;
                $data['cep'] = $cep;
                $data['redirect'] = $request->input('redirect');
                $address = $correios->zipcode()->find($cep);
                //Verifica se o CEP foi encontrado com sucesso
                if(isset($address['error'])) {
                    //Se não, então é retornado uma mensagem de erro
                    return view('user.address.user-adress-create', $data)->withErrors(['cep' => 'CEP não encontrado. Por favor, tente novamente!']);
                } else
                    $data['address'] = $address;
            }
        } 
        return view('user.address.user-adress-create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'cep' => "required|string|size:9",
            'estado' => "required|alpha|size:2",
            'cidade' => "required|string|max:255",
            'bairro' => "required|string|max:255",
            'rua' => "required|string|max:255",
            'numero' => "required|integer|max:65536",
            'complemento' => "nullable|string|max:255",
            'telefone' => "nullable|string|size:13",
            'redirect' => 'nullable|string',
        ]);

        $correios = new Client;
        $address = $correios->zipcode()->find($validated['cep']);
        //Verifica se o CEP foi encontrado com sucesso
        if(isset($address['error'])) 
            return redirect()->back()->withErrors(['cep' => 'CEP não encontrado. Por favor, tente novamente!'])->withInput($request->all());


        $endereco = new EnderecoUsuario;
        $endereco->users_id = $request->user()->id;
        $endereco->cep = $validated['cep'];
        $endereco->uf_sigla = $validated['estado'];
        $endereco->cidade = $validated['cidade'];
        $endereco->bairro = $validated['bairro'];
        $endereco->rua = $validated['rua'];
        $endereco->numero = $validated['numero'];
        $endereco->complemento = $validated['complemento'];
        $endereco->telefone = $validated['telefone'];

        $redirect = isset($validated['redirect']) ? $validated['redirect'] : null;
        //Verifica se o usuário possui endereços cadastrados
        if($request->user()->enderecos()->exists()) {
            if($redirect != null) {
                //Se sim, mas o usuário está cadastrando a partir da sacola, então esse endereço é definido como padrão
                $request->user()->enderecos()->where('is_main', 1)->update(['is_main' => 0]);
                $endereco->is_main = 1;
            } else {
                $endereco->is_main = 0;
            }
        } else {
            $endereco->is_main = 1;
        }
        $endereco->save();
        //Verifica se possui algum link de redirecionamento
        if(isset($redirect)) return redirect()->route($redirect);

        //Se não, é redirecionado ao perfil
        return redirect()->route('user.profile')->with(['address_active' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EnderecoUsuario $address)
    {
        $this->authorize('update', $address);
        $redirect = $request->input('redirect');
        return view('user.address.user-adress-edit', ['endereco' => $address, 'redirect' => $redirect]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnderecoUsuario $address)
    {
        $validated = $request->validate([
            'cep' => "required|string|size:9",
            'estado' => "required|alpha|size:2",
            'cidade' => "required|string|max:255",
            'bairro' => "required|string|max:255",
            'rua' => "required|string|max:255",
            'numero' => "required|integer|max:65536",
            'complemento' => "nullable|string|max:255",
            'telefone' => "nullable|string|size:13",
            'redirect' => 'nullable|string',
        ]);

        $correios = new Client;
        $endereco = $correios->zipcode()->find($validated['cep']);
        //Verifica se o CEP foi encontrado com sucesso
        if(isset($endereco['error'])) 
            return redirect()->back()->withErrors(['cep' => 'CEP não encontrado. Por favor, tente novamente!'])->withInput($request->all());

        $address->users_id = $request->user()->id;
        $address->cep = $validated['cep'];
        $address->uf_sigla = $validated['estado'];
        $address->cidade = $validated['cidade'];
        $address->bairro = $validated['bairro'];
        $address->rua = $validated['rua'];
        $address->numero = $validated['numero'];
        $address->complemento = $validated['complemento'];
        $address->telefone = $validated['telefone'];
        $address->save();

        $redirect = isset($validated['redirect']) ? $validated['redirect'] : null;
        //Verifica se possui algum link de redirecionamento
        if(isset($redirect)) return redirect()->route($redirect);

        //Se não, é redirecionado ao perfil
        return redirect()->route('user.profile')->with(['address_active' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EnderecoUsuario  $enderecoUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EnderecoUsuario $address)
    {
        $this->authorize('delete', $address);
        if($address->is_main == 1) {
            $next = $request->user()->enderecos()->where('is_main', 0)->first();
            if(isset($next)) {
                $next->is_main = 1;
                $next->save();
            } 
        }
        $address->delete();
        return redirect()->route('user.profile')->with(['address_active' => true]);;
    }
    public function setMain(Request $request, EnderecoUsuario $address) {
        //Recupera o endereço principal e define is_main pra 0
        $request->user()->enderecos()->where('is_main', 1)->update(['is_main' => 0]);

        //Define o atributo is_main do endereço selecionado para 1
        $address->is_main = 1;
        $address->save();

        return redirect()->route('user.profile')->with(['address_active' => true]);
    }
}
