<?php

namespace App\Http\Controllers;

use App\User;
use App\Categoria;
use Illuminate\Http\Request;
use App\Rules\Cpf;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('user.profile.user-profile');
    }
    public function edit()
    {
        return view('user.profile.user-profile-edit');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'cpf' => [new Cpf],
            'celular' => ['nullable', 'string', 'size:15'],
            'nascimento' => ['nullable', 'date'],
            'redirect' => ['nullable', 'string'],
        ]);
        
        //DEFINE AS POSSÍVEIS ALTERAÇÕES PARA OS ATRIBUTOS
        $user = $request->user();
        $user->name = $validated['name'];
        $strings = explode(" ", $validated['name']);
        $user->primeiro_nome = $strings[0];
        $user->cpf = $validated['cpf'];
        $user->celular = $validated['celular'];
        $user->nascimento = $validated['nascimento'];

        //VERIFICA SE ALGUM ATRIBUTO MUDOU E SALVA AS ALTERAÇÕES
        if($user->isDirty()) $user->save();
        

        //GERANDO REDIRECIONAMENTO PERSONALIZADO
        $redirect = isset($validated['redirect']) ? $validated['redirect'] : null;
        if($redirect) return redirect()->route($redirect);

        return redirect()->route('user.profile');
    }


    public function editPassword(Request $request) {
        return view('auth.passwords.password-edit');
    }
    public function changePassword(Request $request) {
        $validated = $request->validate([
            'password' => ['required', 'confirmed', 'min:8', 'string'],
        ]);

        $user = $request->user();
        $user->password = Hash::make($validated['password']);
        $user->save();
        return redirect()->route('user.profile');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
