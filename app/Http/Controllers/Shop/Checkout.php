<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use FlyingLuscas\Correios\Client;
use FlyingLuscas\Correios\Service;
use Carbon\Carbon;
use App\Pedido;
use App\EnderecoPedido;
use App\Rules\Cpf;


class Checkout extends Controller
{
    public function finish(Request $request) {
        $address = $request->user()->enderecos()->where('is_main', 1)->first();
        $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();
        $summary = $this->getOrderSummary($produtos, $address->cep);

        session(['summary' => $summary]);

        return view('shop.checkout.checkout-finish', ['summary' => $summary, 'address' => $address, 'quantidade' => $produtos->sum('pivot.quantidade')]);
    }
    public function order(Request $request) {
        $validated = $request->validate([
            'shipping-option' => ['required', 'string', 'in:sedex,pac'],
            'payment-option' => ['required', 'integer', 'exists:App\MetodoPagamento,id'],
            'name-owner' => ['required_if:payment-option,2', 'string', 'max:255'],
            'card-number' => ['required_if_:payment-option,2', 'string', 'size:19'],
            'cvv' => ['required_if_:payment-option,2', 'integer'],
            'expiration-date' => ['required_if_:payment-option,2', 'date_format:m/Y'],
            'num-parcels' => ['required_if_:payment-option,2', 'integer', 'between:1,12'],
        ]);
        $endereco = $request->user()->enderecos()->where('is_main', 1)->first();
        $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();
        $summary = session()->get('summary');

        // FRETE DO PRODUTO
        if($validated['shipping-option'] == 'sedex') $frete = $summary['frete']['sedex']; else $frete = $summary['frete']['pac'];
        $data_entrega = $frete['deadline'];
        $valor_frete = $frete['price'];

        // RESUMO DO PEDIDO
        $subTotal = $summary['subTotal'];
        $desconto = $summary['subTotalSemDesconto'] - $subTotal;
        $total = $subTotal +  $valor_frete;

        // GERANDO O PEDIDO
        $pedido = new Pedido();
        $pedido->users_id = $request->user()->id;
        $pedido->subTotal =  $subTotal;
        $pedido->desconto = $desconto;
        $pedido->valor_frete = $valor_frete;
        $pedido->data_entrega = $data_entrega;
        $pedido->total = $total;
        $pedido->status = "Em aprovação";
        $pedido->metodos_pagamento_id = $validated['payment-option'];
        $pedido->num_parcelas = isset($validated['num-parcels']) ? $validated['num-parcels'] : null;
        $pedido->save();

        // ENDEREÇO DE ENTREGA DO PEDIDO
        $endPedido = new EnderecoPedido();
        $endPedido->pedidos_id = $pedido->id;
        $endPedido->cep = $endereco->cep;
        $endPedido->uf_sigla = $endereco->uf_sigla;
        $endPedido->cidade = $endereco->cidade;
        $endPedido->bairro = $endereco->bairro;
        $endPedido->cidade = $endereco->cidade;
        $endPedido->rua =  $endereco->rua;
        $endPedido->numero =$endereco->numero;
        $endPedido->complemento =$endereco->complemento;
        $endPedido->telefone = $endereco->telefone;
        $endPedido->save();

        // ITENS DO PEDIDO
        foreach($produtos as $prod) {
            $quantidade = $prod->pivot->quantidade;
            $preco = $prod->desconto;
            $total = $preco * $quantidade;
            $pedido->produtos()->attach($prod->id, ['quantidade' => $quantidade, 'preco' => $preco, 'total' => $total]);

            //ATUALIZANDO A QUANTIDADE DO PRODUTO
            $prod->quantidade -= $prod->pivot->quantidade;

            // SE A QUANTIDADE EM ESTOQUE DO PRODUTO SIMPLES OU VARIAÇÃO FOR 0, ENTÃO O PRODUTO FICA INDISPONÍVEL
            if($prod->quantidade <= 0) $prod->is_avaliable = 0;
            $ids[] = $prod->id;
            $prod->save();

            
            // SE A QUANTIDADE EM ESTOQUE DE TODAS AS VARIAÇÕES DO PRODUTO CONFIGURÁVEL FOR 0, ENTÃO O PRODUTO FICA INDISPONÍVEL
            if($prod->parente()->exists()) {
                if($prod->parente->filhos()->sum('quantidade') == 0) {
                    $parente->is_avaliable = 0;
                    $parente->save();
                }
            }
        }
        // REMOVENDO OS PRODUTOS DA SACOLA APÓS A COMPRA
        $request->user()->sacola->produtos()->detach($ids);

        // DESTRUINDO AS VARIÁVEIS EM SESSÃO
        $request->session()->forget('summary');
        
        return redirect()->route('user.orders');
    }
    public function address(Request $request) {
        $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();

        $summary = $this->getOrderSummary($produtos);

        return view('shop.checkout.checkout-address', ['summary' => $summary]);
    }
    public function personal_info(Request $request) {
        $produtos = $request->user()->sacola->produtos()->where('is_avaliable', 1)->get();

        $summary = $this->getOrderSummary($produtos);

        return view('shop.checkout.checkout-personal-info', ['summary' => $summary]);
    }


    public function getOrderSummary($produtos, $cep = null) {
        //Recupera os produtos na sacola
        $frete['sedex']['price'] = 0;
        if($cep) $frete = $this->getShippingValues($produtos, $cep);
        
        //Calculando o preço total para cada produto na sacola
        $produtos = $produtos->map(function ($produto, $key) {
            $produto->precoComDesconto = $produto->pivot->quantidade * $produto->desconto;
            $produto->precoTotal = $produto->pivot->quantidade * $produto->preco;
            return $produto;
        });
        //Calculando o preço total da sacola
        $subTotal = $produtos->sum('precoComDesconto');
        $subTotalSemDesconto = $produtos->sum('precoTotal');
        $total = $frete['sedex']['price'] + $subTotal;

        return [
            'subTotalSemDesconto' => $subTotalSemDesconto,
            'subTotal' => $subTotal, 
            'frete' => $frete,  
            'total' => $total
        ];
       
    }
    public function getShippingValues($produtos, $cep) {
        $td = Carbon::now()->locale('pt-br');
        $td->add('1 day');
        $td = $td->toDateString();

        $correios = new Client;
        $correios->freight()
        ->origin('35574-061')
        ->destination($cep)
        ->services(Service::SEDEX, Service::PAC);

        foreach($produtos as $prod) $correios->freight()->item($prod->largura, $prod->altura, $prod->comprimento, $prod->peso, $prod->pivot->quantidade);
        $frete = $correios->freight()->calculate();
        $sedex = $frete[0];
        $pac = $frete[1];

        // DATA DE ENTREGA COM FINS DE SEMANA E FERIADO - SEDEX
        $sedexDeadline = file_get_contents("https://elekto.com.br/api/Calendars/br-BC/Add?initialDate=$td&days=". $sedex['deadline'] ."&type=work&finalAdjust=none");
        $sedexDeadline = substr($sedexDeadline, 1);
        $sedexDeadline = new Carbon(substr($sedexDeadline, 0, -1));
        $sedexDeadline = $sedexDeadline->day. " de " .$sedexDeadline->locale('pt-br')->monthName;
        

        // DATA DE ENTREGA COM FINS DE SEMANA E FERIADO -  PAC
        $pacDeadline = file_get_contents("https://elekto.com.br/api/Calendars/br-BC/Add?initialDate=$td&days=". $pac['deadline'] ."&type=work&finalAdjust=none");
        $pacDeadline = substr($pacDeadline, 1);
        $pacDeadline = new Carbon(substr($pacDeadline, 0, -1));
        $pacDeadline = $pacDeadline->day. " de " .$pacDeadline->locale('pt-br')->monthName;


        $sedex['deadline'] = $sedexDeadline;
        $pac['deadline'] = $pacDeadline;
        return ['sedex' => $sedex, 'pac' => $pac];
    }
    public function loadShippingValue(Request $request) {
        //Recupera os valores em sessão
        $summary = session()->get('summary');
        $frete = $summary['frete'];
        $subTotal = $summary['subTotal'];

        //Recupera a opção de frete selecionada
        $shippingOption = $request->input('shipping-option');

        //Recupera o preço do frete selecionado
        $shippingPrice = $shippingOption == "sedex" ? $frete['sedex']['price'] : $frete['pac']['price'];
        
        //Recalcula o total da sacola
        $total = $shippingPrice + $subTotal;

        $total = number_format($total, 2, ',', '.');
        $shippingPrice = number_format($shippingPrice, 2, ',', '.');

        return response()->json([
            'frete' => $shippingPrice,
            'total' => $total,
        ]);
    }   
}
