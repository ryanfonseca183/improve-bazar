<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Produto;
use App\Avaliacao;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $promocoes = clone $recomendados = clone $lancamentos = clone $produtos = Produto::select('produtos.id', 'produtos.nome', 'produtos.preco', 'promo_value', 'promo_starts_at', 'promo_ends_at', 'product_type')
        ->where('produtos.is_avaliable', 1)
        ->where('produtos.is_visible', 1)
        ->where('produtos.is_deleted', 0)
        ->whereNull('produtos.produtos_id')
        ->withCount('filhos')
        ->take(9);

        //Seleciona os produtos em promoção
        $promocoes = $promocoes->orderBy('promo_value', 'desc')
        ->whereDate('promo_starts_at', '<=', now()->toDateString())
        ->whereDate('promo_ends_at', '>=', now()->toDateString())
        ->get()
        ->chunk(3);
        
        $avaliacoes = Avaliacao::select('produtos_id', DB::raw('avg(valor) as rate'))->groupBy('produtos_id')->havingRaw('count(*) >= ?', [2])->orderBy('rate', 'desc')->take(9)->get();
        //Seleciona os produtos melhores avaliados na loja
        $recomendados = $recomendados->whereIn('produtos.id' , $avaliacoes->pluck('produtos_id'))
        ->get()
        ->chunk(3);

        //Seleciona os ultimos produtos cadastrados na loja
        $lancamentos = $lancamentos->orderBy('produtos.created_at', 'desc')->get()->chunk(3);

        return view('shop.shop-home', ['promocoes' => $promocoes, 'lancamentos' => $lancamentos, 'recomendados' => $recomendados]);
    }
}
