<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Produto;
use App\Categoria;

class ShowProduto extends Controller
{
    public function show(Produto $produto)
    {
        $semelhantes = Produto::where('subcategorias_id', $produto->subcategorias_id)
                                ->whereNull('produtos_id')
                                ->where('is_visible', 1)
                                ->where('is_deleted', 0)
                                ->where('id', "!=", $produto->id)
                                ->orderBy('nome');

        $strings = explode(' ', $produto->nome);
        $semelhantes = $semelhantes->where(function ($query) use ($strings) {
            foreach($strings as $key => $nome) {
                if(strlen($nome) > 2) {
                    $query->orWhere('nome', 'like', "%". $nome . "%");
                }
            }
        });
        $semelhantes = $semelhantes->take(9)->get()->chunk(3);
       return view('shop.shop-produto', ['produto' => $produto, 'semelhantes' => $semelhantes]);
    }
    public function unavaliable() {
        return view('shop.shop-produto-unavaliable');
    }
}
