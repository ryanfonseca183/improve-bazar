<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subcategoria;
use App\Produto;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;


class ShopController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if($request->hasAny(['nome', 'subcategorias_id'])){
            $filters = $request->query();
            $attributes = [];
            $order_attribute = "nome";
            $order_keyword = "asc";
            $page = array_key_exists('page', $filters) ? $filters['page'] : 1;
            $paginate = 10;
            unset($filters['page']);

            $subcategoria = Subcategoria::find($request->subcategorias_id);
            $shop = [
                'filters' => $filters,
                'subcategoria' => $subcategoria,
            ];

            $today = now()->toDateString();
            $produtos = Produto::select('produtos.id', 'produtos.nome',  DB::raw('CASE WHEN promo_starts_at <= "'.$today.'" and promo_ends_at >= "'. $today .'" THEN (((100 - promo_value) / 100) * preco) ELSE preco END AS desconto', [$today, $today]), 'promo_value', 'promo_starts_at', 'promo_ends_at', 'is_avaliable', 'produtos.produtos_id')
            ->where('produtos.is_visible', 1)
            ->where('produtos.is_deleted', 0)
            ->where('produtos.product_type', 1);
            
            // APLICANDO FILTROS DE PESQUISA
            foreach($filters as $key => $value) {
                switch($key) {
                    case 'nome': 
                        $strings = explode(' ', $value);
                        foreach($strings as $nome) $produtos->where('nome', 'like', "%". $nome . "%");
                    break;
                    case 'subcategorias_id': 
                        $produtos->where('produtos.subcategorias_id', $value);
                    break;
                    case 'preco': 
                        $range = explode("-", $value);
                        $priceMin = (int) $range[0] - 1;
                        $priceMax = (int) $range[1] + 1;
                        $produtos->whereBetween('preco', [$priceMin, $priceMax]);
                        $shop['priceRange'] = [
                            'min' => $range[0], 
                            'max' => $range[1]
                        ];
                    break;
                    case 'order_by': 
                        $separator = strpos($value, '_');
                        $order_attribute = substr($value, $separator + 1);
                        $order_keyword = substr($value, 0, $separator);
                    break;
                    default: 
                        $produtos->join('produtos_has_atributos as '. $key, 'produtos.id', $key.'.produtos_id');
                        if(strpos($value, 'label_') !== false) {
                            $value = substr($value, 6);
                            $produtos->where($key . '.label', $value);
                        } else {
                            $produtos->where($key . '.valor', $value);
                        }
                }
            }
            // CARREGANDO PRODUTOS
            if($subcategoria) {
                if($subcategoria->atributos()->where('is_configurable', 1)->exists()) {
                    // PRODUTOS CONFIGURÁVEIS
                    $produtos->addSelect(DB::raw('min(preco) as preco'))->whereNotNull('produtos.produtos_id')->groupBy('produtos.produtos_id');
                } else {
                    // PRODUTOS SIMPLES
                    $produtos->addSelect('preco')->whereNull('produtos.produtos_id');
                }
                $produtos->orderBy($order_attribute, $order_keyword);
            } else {
                // UNIÃO DE PRODUTOS SIMPLES E CONFIGURÁVEIS COM BASE NO NOME
                $simpleProducts = clone $produtos;
                $produtos->addSelect(DB::raw('min(preco) as preco'))->whereNotNull('produtos.produtos_id')->groupBy('produtos.produtos_id');
                $simpleProducts->addSelect('preco')->whereNull('produtos.produtos_id');
                $produtos = $produtos->union($simpleProducts)->orderBy($order_attribute, $order_keyword);;
            }
            $produtos = $produtos->get();

            //PAGINANDO OS PRODUTOS
            $pageItems = $produtos->slice($paginate * ($page - 1), $paginate);
            $produtos = new Paginator($pageItems, $produtos->count(), $paginate, $page, [
                'path' => $request->url(),
                'query' => $request->query(),
            ]);

            //Calculando o preço mínimo e máximo dos produtos
            $shop['priceMin'] = $produtos->min('preco');
            $shop['priceMax'] = $produtos->max('preco');
            
            session(['shop' => $shop]);
            return view('shop.shop-catalogo', ['produtos' => $produtos]);
        } else {
            return redirect()->route('shop.home');
        }
    }
}
