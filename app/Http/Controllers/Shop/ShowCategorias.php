<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Categoria;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ShowCategorias extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $letters = Categoria::select(DB::raw('SUBSTRING(nome, 1, 1) as nome'))->distinct()->pluck('nome');
        $categorias = Categoria::select('nome', 'id')->withCount('subcategorias')->get();
        $groups = [];
        foreach($letters as $letter) {
            foreach($categorias as $cat) {
                $nome = Str::lower($cat->nome);
                $letter = Str::lower($letter);
                if(Str::startsWith($nome, $letter)) {
                    if($cat->subcategorias_count != 0) {
                        $groups[$letter][] = $cat;
                    }
                }
            }
        }
        //Chama a view de listagem e cadastro das categorias passando os valores do modelo como argumento
        return view('shop.shop-categorias', ['categorias' => $categorias, 'groups' => $groups]); 
    }
}
