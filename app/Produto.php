<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Produto extends Model
{
    protected $appends = ['desconto'];

    public function atributos() {
        return $this->BelongsToMany('App\Atributo', 'produtos_has_atributos', 'produtos_id', 'atributos_id')->withPivot('valor', 'label');
    }
    public function fotosProduto() {
        return $this->hasMany('App\FotoProduto', 'produtos_id');
    }
    public function parente() {
        return $this->belongsTo('App\Produto', 'produtos_id');
    }
    public function irmaos() {
        return  Produto::select('*')->where('produtos_id', $this->produtos_id)->where('id', '!=', $this->id)->get();
    }
    public function filhos() {
        return $this->hasMany('App\Produto', 'produtos_id');
    }
    public function subcategoria() {
        return $this->belongsTo('App\Subcategoria', 'subcategorias_id');
    }
    public function sacolas() {
        return $this->belongsToMany('App\Sacola', 'sacolas_has_produtos', 'produtos_id', 'sacolas_id');
    }
    public function favoritos() {
        return $this->belongsToMany('App\Favorito', 'favoritos_has_produtos', 'produtos_id', 'favoritos_id');
    }
    public function pedidos() {
        return $this->belongsToMany('App\Pedido', 'itens_pedidos', 'produtos_id', 'pedidos_id');
    }
    public function avaliacoes() {
        return $this->hasMany('App\Avaliacao', 'produtos_id');
    }
    public function getPrecoAttribute($value) {
        if($this->product_type == 2) $value = $this->filhos()->min('preco');
        return $value;
    }
    public function getDescontoAttribute() {
        $today = now();
        $preco = $this->preco;
        //VERIFICA SE A PROMOÇÃO ESTÁ ATIVA
        if($today->greaterThanOrEqualTo($this->promo_starts_at) && $today->lessThanOrEqualTo($this->promo_ends_at)) {
            $preco = $preco * ((100 - $this->promo_value) / 100);
        }
        //RETORNA O PREÇO ATUALIZADO
        return $preco;
    }
    public function getPromoStartsMinAttribute() {
        $today = date('Y-m-d');
        $starts_min = $this->promo_starts_at != null ? $this->promo_starts_at : $today;
        $starts_min = new Carbon($starts_min);
        $starts_min = $starts_min->greaterThan($today) ? $today : $starts_min->toDateString();
        return $starts_min;
    }
    public function getPromoEndsMinAttribute() {
        $starts_min = $this->promo_starts_at != null ? $this->promo_starts_at : date('Y-m-d');
        $ends_min = new Carbon($starts_min);
        $ends_min->addDay();
        return $ends_min->toDateString();
    }
}
