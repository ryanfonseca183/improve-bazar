<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    public function valoresAtributos() {
        return $this->hasMany('App\ValorAtributo', 'atributos_id');
    }
    public function subcategoria() {
        return $this->belongsTo('App\Subcategoria', 'subcategorias_id');
    }
    public function tipoDeDados() {
        return $this->belongsTo('App\TipoAtributo', 'tipos_atributos_id');
    }
    public function produtos() {
        return $this->belongsToMany('App\Produto', 'produtos_has_atributos', 'atributos_id', 'produtos_id')->withPivot('valor', 'label');
    }
}
