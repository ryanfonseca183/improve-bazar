<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sacola extends Model
{
    public $timestamps = false;
    protected $table = "sacolas";

    public function produtos() {
        return $this->BelongsToMany('App\Produto', 'sacolas_has_produtos', 'sacolas_id', 'produtos_id')->withPivot('quantidade');
    }
}
