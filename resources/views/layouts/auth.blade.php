<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name')}} </title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    @if(Route::getCurrentRoute()->getName() != "register")
        <style>
            #header-auth  {
                position: relative;
                padding: 1.5rem 0;
            }
            #header-auth img {
                height: 100px;
                display:block;
                margin: auto;
            }
            #header-auth::before {
                content: "";
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 40vh;
                background-color: var(--amarelo-claro);
                z-index: -1;
            }
        </style>
    @endif

    @hasSection ('pl-custom-styles')
        @yield('pl-custom-styles')  
    @endif
</head>
<body id="page-top">
    @if(Route::getCurrentRoute()->getName() != "register")
        <header id="header-auth">
            <a href="{{ route('shop.home') }}">
                <img src="{{ asset('img/1 gra.png') }}" alt="Logo improve bazar" title="Tela inicial da loja">
            </a>
        </header>
    @endif
    <div class="container-md">
        @yield('content')
    </div>
    
   
    <!-- Bootstrap, jquery and pooper core JavaScript-->
    <script src="{{ asset('js/dependencies/bootstrap.js') }}"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/dependencies/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/shop.js') }}"></script>

    @hasSection ('pl-custom-scripts')
        @yield('pl-custom-scripts')
    @endif
</body>
</html>
