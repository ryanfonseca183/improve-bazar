@extends('.layouts.compressed-header')

@section('pl-custom-styles')
    <style>
        body {
            background: #f8f9fc !important;
        }
        #sidebar-container {
            margin-top: 3rem;
            margin-bottom: 1.5rem;
            background: var(--cinza-medio);
            border-radius: 0.25rem;
        }
        #sidebar {
            position:relative;
            padding: 1.5rem;
            width: 100%;
        }
        @media only screen and (min-width: 992px) {
            #sidebar-container {
                margin: 0;
                margin-right: 1rem;
            }
            #sidebar {
                padding-top: 3rem;
                margin-top: 0;
                margin-bottom: 0;
                width: 350px;
            }
            #sidebar-content.fixed {
                position:fixed;
                top: 3rem;
                width: calc(350px - 3rem);
            }
        }
        #main-content {
            padding: 2rem 2rem 3rem;
            border-radius: 0.25rem;
            box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
            background:white;
        }
    </style>
@endsection

@section('content')
    <div class="row" id="content">
         {{-- BARRA LATERAL --}}
         <div class="col-lg-auto" id="sidebar-container">
             <aside id="sidebar">
                <div id="sidebar-content">
                    <h2 class="h5">Resumo da compra</h2>
                    <x-order-summary :summary="$summary"></x-order-summary>
                </div>
             </aside>
        </div>
        <div class="col-lg pt-lg-5">
            <main>
                @yield('page-content')
            </main>
        </div>
    </div>
@endsection

@section('pl-custom-scripts')
    <script>
        //Define um tamanho minimo para a row content
        const topBarHeight = $("header").outerHeight();
        $("#content").css('min-height', 'calc(100vh - ' +topBarHeight+'px)');

        //Define um evento de scroll para a window, fixando o sidebar-content
        const sidebar = $("#sidebar");
        const sidebarContent = $("#sidebar-content");
        $(window).on('scroll', function(){
            if($(this).scrollTop() > topBarHeight) {
                sidebarContent.addClass('fixed')
            } else 
                sidebarContent.removeClass('fixed') 
        });
    </script>
@endsection

