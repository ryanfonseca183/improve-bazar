@extends('layouts.shop')

@section('title', 'Meu perfil')

@section('pl-custom-styles')
    <style>
        /* MENU LATERAL */
        #content {
            align-items:start;
        }
        #lateral-menu {
            padding: 1.5rem;
            background: var(--cinza-medio);
            border-radius: 0.35rem;
        }
        .logo {
            display:none;
            text-align: center;
        }
        .logo span {
            font-weight: 600;
            color: grey;
        }
        .logo img {
            display:block;
            margin: 0 auto;
            margin-bottom: 0.5rem;
            width: 50%;
        }
        .navbar-brand img {
            width: 50px;
            margin-right: 0.5rem;
            display: inline-block;
        }
        .navbar-nav .nav-item.active a {
            color:var(--laranja) !important;
        }
        @media only screen and (min-width: 768px) {
            #lateral-menu {
                padding: 3rem 1.5rem;
            }
        }
    </style>
@endsection

@section('content')
    <div class="container-md">
        <div class="row no-gutters" id="content">
            <div class="col-md-3" id="lateral-menu">
                <aside>
                    <div class="logo d-md-block">
                        <img src="{{ asset('img/user-icon.png') }}" alt="">
                        <span>{{Auth::user()->primeiro_nome}}</span>
                    </div>
                    <nav class="navbar navbar-expand-md navbar-light p-0">
                        <span class="navbar-brand d-md-none">
                            <img src="{{ asset('img/user-icon.png') }}" alt="">
                            <span>{{Auth::user()->primeiro_nome}}</span>
                        </span>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#userMenu" aria-controls="userMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>
                        @php
                            $route = Route::getCurrentRoute()->getName();
                            $ordersRoutes = [
                                'user.orders',
                                'user.orders.show',
                                'orders.produto.avaliacao.edit', 
                                'orders.produto.avaliacao.create',
                            ];
                        @endphp
                        <div class="collapse navbar-collapse mt-3" id="userMenu">
                            <ul class="navbar-nav mr-auto flex-column">
                                <li class="nav-item @if($route == 'user.profile') active @endif">
                                    <a class="nav-link" href="{{ route('user.profile') }}">Meus dados</a>
                                </li>
                                <li class="nav-item @if(in_array($route, $ordersRoutes)) active @endif">
                                    <a class="nav-link" href="{{ route('user.orders') }}">Compras</a>
                                </li>
                                <li class="nav-item @if($route == 'user.favorites') active @endif">
                                    <a class="nav-link" href="{{ route('user.favorites') }}">Favoritos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('user.bag') }}">Sacola</a>
                                </li>
                                <hr>
                                <li class="nav-item">
                                    <form action="{{ route('logout') }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn shadow-none nav-link">Sair</button>
                                    </form>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Deletar conta</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </aside>
            </div>
            @yield('profile-content')
        </div>
    </div>
@endsection