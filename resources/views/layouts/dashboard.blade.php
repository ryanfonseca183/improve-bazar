<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{' Painel de controle - ' . config('app.name')}} </title>

     <!-- Custom styles for this template-->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    @yield('pl-custom-styles')
</head>

<body id="page-top">
    <div id="pre-loader">
        <div class="spinner-border text-warning mb-4" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        <p class="mb-4">Estamos trabalhando na sua requisição, por favor aguarde...</p>
    </div>

    <div id="wrapper">
        <ul class="navbar-nav bg-gradient-orange sidebar sidebar-dark accordion" id="accordionSidebar">

            <a class="sidebar-brand" href="index.html">
                <div class="sidebar-brand-icon">
                    <i class="fas fa-chart-line"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Improve</div>
            </a>

            <hr class="sidebar-divider my-0">

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Painel de controle</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                Gerenciar
            </div>

            @php 
                $route = Route::getCurrentRoute()->getName();
                $routeCategorias = ['categoria.index'];
                $routeAtributos = [
                    'atributo.index',
                    'atributo.create',
                    'atributo.edit',
                    'atributo.show',
                ];
                $routeProdutos = [
                    'produto.index',
                    'produto.create',
                    'produto.show',
                ];
            @endphp
            <li class="nav-item @if(in_array($route, $routeCategorias)) active @endif">
                <a class="nav-link"  href="{{ route('categoria.index') }}">
                    <i class="fas fa-list"></i>
                    <span>Categorias</span>
                </a>
            </li>
            <li class="nav-item @if(in_array($route, $routeAtributos)) active @endif">
                <a class="nav-link"  href="{{ route('atributo.index') }}">
                    <i class="fas fa-tint"></i>
                    <span>Atributos</span>
                </a>
            </li>
            <li class="nav-item @if(in_array($route, $routeProdutos)) active @endif">
                <a class="nav-link" href="{{ route('produto.index') }}">
                    <i class="fas fa-mobile-alt"></i>
                    <span>Produtos</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                Configurações
            </div>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="fas fa-shipping-fast"></i>
                    <span>Frete</span>
                </a>
                <a class="nav-link" href="#">
                    <i class="fas fa-user-circle"></i>
                    <span>Contas</span>
                </a>
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Painel</span>
                </a>
            </li>
            <hr class="sidebar-divider d-none d-md-block">

            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <div id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                
                    <button id="sidebarToggleTop" class="btn btn-link text-warning d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a href="{{ route('shop.home') }}">
                        <img src="{{ asset('img/1 gra.png') }}" height="50">
                    </a>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
                            <i class="fas fa-user-circle"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Perfil
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Configurações
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Atividades
                            </a>
                            <div class="dropdown-divider"></div>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="dropdown-item">
                                    <i class="fas fa-sign-out-alt text-gray-400 mr-2"></i>Sair
                                </button>
                            </form>
                        </div>
                        </li>
                    </ul>
                    <!-- End of User information-->

                </nav>
                
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row no-gutters page-title py-4">
                        <div class="col-auto page-title-icon d-md-block">
                            @yield('icon')
                        </div>
                        <div class="col page-title-text text-md-left">
                            <h1 class="page-title-header">@yield('title')</h1>
                            <p class="page-title-desc">@yield('desc')</p>
                        </div>
                    </div>

                    @yield('content')
                    
                </div>
            </div>

            <footer class="sticky-footer">
                <div class="container">
                    <div class="copyright">
                        <span>Copyright &copy; Improve Bazar 2020</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    @hasSection ('modals')
        @yield('modals')    
    @endif


    <!-- Bootstrap, jquery and pooper core JavaScript-->
    <script src="{{ asset('js/dependencies/bootstrap.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/dependencies/jquery.easing.min.js') }}"></script>

 
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>

    @hasSection ('pl-plugins')
        @yield('pl-plugins')
    @endif

    @hasSection ('pl-custom-scripts')
        @yield('pl-custom-scripts')
    @endif
</body>
</html>