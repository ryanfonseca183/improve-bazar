@section('modals')

 <!--Modal de exclusão-->
 <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content py-4">
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-auto mb-3 modal-icon bg-white shadow-sm">
                        <i class="fas fa-exclamation-circle text-reddish"></i>
                    </div>
                    <div class="w-100"></div>
                    <div class="col-sm-10 mb-4 text-justify">
                        <h3 class="h4 mb-4 text-gray-800 text-center">Atenção</h3>
                        @yield('modal-text')
                    </div>
                    <div class="w-100"></div>
                    <div class="col">
                        <div class="row justify-content-center">
                            <div class="col-4">
                                <button type="button" class="btn btn-block btn-lg btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                            <div class="col-4">
                                <form method="POST" id="formDelete" action="@yield('route-delete')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-block btn-lg btn-danger">Excluir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection