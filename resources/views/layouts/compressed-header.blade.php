<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name')}} </title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/shop.css') }}">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <style>
        header {
            padding:0.5rem 1rem;
            position: relative;
        }
        header::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: var(--amarelo-claro);
            z-index: -1;
        }
        .logo {
            width: 100px;
            height: auto;
            display:inline-block;
        }
        @media only screen and (min-width: 578px) {
            .logo {
                width: 150px;
            }
        }
    </style>
    @hasSection ('pl-custom-styles')
        @yield('pl-custom-styles')  
    @endif
</head>
<body id="page-top">
    <header>
        <div class="container-sm py-2">
            <nav class="navbar navbar-expand-sm justify-content-between navbar-light p-0">
                <a class="navbar-brand" href="{{ route('shop.home') }}">
                    <img src="{{ asset('img/1 gra.png') }}" class="logo" alt="Página inicial da loja">    
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#user-menu" aria-controls="user-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse mt-3 mt-sm-0 justify-content-end" id="user-menu">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link px-lg-3 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="text-orange mr-1 fas fa-user-circle" title="Perfil do usuário"></i>
                                {{ Auth::user()->primeiro_nome}}
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('user.profile') }}">Perfil</a>
                                <a class="dropdown-item" href="{{ route('user.orders') }}">Compras</a>
                                <a class="dropdown-item" href="#">Endereços</a>
                                <div class="dropdown-divider"></div>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="dropdown-item">Sair</button>
                                </form>
                            </div>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link px-lg-3" href="{{ route('user.favorites') }}">
                            <i class="fas fa-heart text-orange mr-1" title="Favoritos"></i>
                            Favoritos
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link px-lg-3" href="{{ route('user.bag') }}">
                            <i class="text-orange mr-1 fas fa-shopping-bag" title="Sacola de compras"></i>
                            Sacola
                        </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <div class="container-sm mb-5">
        @yield('content')
    </div>

   
   
    <!-- Bootstrap, jquery and pooper core JavaScript-->
    <script src="{{ asset('js/dependencies/bootstrap.js') }}"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/dependencies/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dependencies/mask.js') }}"></script>


    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/shop.js') }}"></script>

    @hasSection ('pl-custom-scripts')
        @yield('pl-custom-scripts')
    @endif
</body>
</html>
