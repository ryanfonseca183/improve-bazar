<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@hasSection('title') @yield('title') - {{ config('app.name')}}  @else {{ config('app.name')}} @endif</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/shop.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    @hasSection('pl-custom-styles')
        @yield('pl-custom-styles')
    @endif
</head>
<body id="page-top">
    <header id="header">
        <div class="d-md-block" id="header-top">
            <div class="container-md">
                <div class="row">
                    <div class="col">
                        <div class="contact-info">
                            <ul class="nav">
                                <li class="nav-item"><a href="#" class="text-reset nav-link"><i class="fa fa-phone"></i> +55 (DDD) 90000-0000</a></li>
                                <li class="nav-item"><a href="#" class="text-reset nav-link"><i class="fa fa-envelope"></i> email@domínio.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="social-icons">
                            <ul class="nav">
                                <li class="nav-item"><a href="#" class="text-reset nav-link"><i class="fab fa-facebook"></i></a></li>
                                <li class="nav-item"><a href="#" class="text-reset nav-link"><i class="fab fa-twitter"></i> </a></li>
                                <li class="nav-item"><a href="#" class="text-reset nav-link"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->
        
        <div id="header-middle">
            <div class="container-md">
                <div class="row align-items-center py-4">
                    <div class="col-5 col-sm-4 mb-sm-0">
                        <a href="{{ route('shop.home') }}">
                            <img class="img-fluid" src="{{ asset("img/1 gra.png") }}" alt="Logo improve bazar" />
                        </a>
                    </div>
                    <div class="col">
                        <div class="header-middle-content float-lg-right">
                            <ul class="nav d-none d-sm-flex">
                                <li class="nav-item">
                                    @if(Auth::check())
                                        <li class="nav-item">
                                            <div class="btn-group mb-3 ">
                                                <a href="{{ route('user.profile') }}" class="btn btn-warning">
                                                    <i class="text-white mr-1 fas fa-user-circle" title="Perfil do usuário"></i>
                                                    <span>{{ Auth::user()->primeiro_nome }}</span>
                                                </a>
                                                <button type="button" class="btn btn-warning dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <div class="dropdown-menu shadow">
                                                    <a class="dropdown-item" href="{{ route('user.profile') }}">
                                                        <i class="fas fa-user-circle mr-2 text-gray-400" title="Perfil do usuário"></i>Perfil
                                                    </a>
                                                    <a class="dropdown-item" href="{{ route('user.orders') }}">
                                                        <i class="fas fa-file-invoice-dollar mr-2 text-gray-400"></i>Compras
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <i class="fas fa-map-marker-alt mr-2 text-gray-400"></i>Endereços
                                                    </a>
                                                    <div class="dropdown-divider"></div>
                                                    <form action="{{ route('logout') }}" method="POST">
                                                        @csrf
                                                        <button type="submit" class="dropdown-item">
                                                            <i class="fas fa-sign-out-alt mr-2 text-gray-400"></i>Sair
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('user.favorites') }}" class="text-reset nav-link px-2 px-sm-3">
                                                <i class="fas fa-heart text-orange mr-1" title="Favoritos"></i>
                                                <span class="d-none d-sm-inline">Favoritos</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('user.bag') }}" class="text-reset nav-link px-2 px-sm-3">
                                                <span class="bag-icon">
                                                    @php $quantidade = Auth::user()->sacola->produtos()->where('is_avaliable', 1)->sum('sacolas_has_produtos.quantidade') @endphp 
                                                    @if($quantidade != 0 && Route::getCurrentRoute()->getName() != "user.bag")
                                                        <span class="bag-quantity">
                                                            @if($quantidade != 0 && $quantidade < 100) 
                                                                {{$quantidade}}
                                                            @elseif($quantidade > 100) 
                                                                99+
                                                            @endif
                                                        </span>
                                                    @endif
                                                    <i class="text-orange mr-1 fas fa-shopping-bag" title="Sacola de compras"></i>
                                                </span>
                                                <span class="d-none d-sm-inline">Sacola</span>
                                            </a>
                                        </li>
                                        @if(Auth::user()->is_admin)
                                            <li class="nav-item">
                                                    <a href="{{ route('produto.index') }}" class="text-reset nav-link px-2 px-sm-3">
                                                    <i class="text-orange mr-1 fas fa-tachometer-alt" title="Painel de controle"></i>
                                                    <span class="d-none d-sm-inline">Painel</span>
                                                </a>
                                            </li>
                                        @endif
                                    @else 
                                        <li class="nav-item">
                                            <a href="{{ route('login') }}" class="text-reset nav-link px-2 px-sm-3">
                                                <i class="text-orange mr-1 fa fa-user" title="Entrar na conta"></i>
                                                <span class="d-none d-sm-inline">Entrar</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route("register") }}" class="text-reset nav-link px-2 px-sm-3">
                                                <i class="text-orange mr-1 fas fa-sign-in-alt" title="Cadastrar conta"></i>
                                                <span class="d-none d-sm-inline">Cadastrar</span>
                                            </a>
                                        </li>
                                    @endif
                                </li>
                            </ul>
                            <div class="search_box">
                                <form method="GET" action="{{ route('shop.catalogo') }}">
                                    <div class="input-group shadow-sm">
                                        <input type="text" name="nome" class="form-control border-0" required minlength="3" placeholder="Buscar produto...">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit" id="button-addon2">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    
        <div id="header-bottom">
            <div class="container-md">
                <nav class="navbar px-0 navbar-expand-lg navbar-dark">
                    <a class="navbar-brand" href="#">Bazar</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-nav" aria-controls="navbarText" aria-expanded="false" aria-label="Mostrar menu">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="menu-nav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('shop.home') }}">Inicio
                                </a>
                            </li>
                            @if(Auth::check())
                                <li class="nav-item dropdown d-sm-none">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{Auth::user()->primeiro_nome}}
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ route('user.profile') }}">Perfil</a>
                                        <a class="dropdown-item" href="#">Compras</a>
                                        <a class="dropdown-item" href="#">Endereços</a>
                                        <a class="dropdown-item" href="{{ route('user.bag') }}">Sacola</a>
                                        <a class="dropdown-item" href="#">Favoritos</a>
                                        <div class="dropdown-divider"></div>
                                        <form action="{{ route('logout') }}" method="POST">
                                            @csrf
                                            <button type="submit" class="dropdown-item">Sair</button>
                                        </form>
                                    <div>
                                </li>
                            @else 
                                <li class="nav-item d-sm-none">
                                    <a href="{{ route('login') }}" class="nav-link">Entrar</a>
                                </li>
                                <li class="nav-item d-sm-none">
                                    <a href="{{ route("register") }}" class="nav-link">Cadastrar</a>
                                </li>
                            @endif
                            <li class="nav-item d-lg-none">
                                <a class="nav-link" href="{{ route('shop.categorias') }}">Categorias</a>
                            </li>
                           
                            <li class="d-none d-lg-block nav-item dropdown-rel">
                                <a class="nav-link dropdown-toggler">Categorias
                                    <i class="fas fa-sort-down arrow-down"></i>
                                </a>
                                <div class="dropdown-container shadow">
                                    <div class="d-flex">
                                        <div class="dropdown-items">
                                            @php $categorias = App\Categoria::get(); @endphp 
                                            @foreach($categorias as $key => $categoria)
                                                @if($categoria->subcategorias()->exists())
                                                    @php $subcategorias[$key] = $categoria->subcategorias; @endphp
                                                    <a class="drop-item item-sub" href="#" data-order="{{ $key }}">
                                                        <span>{{$categoria->nome}}</span>
                                                        <i class="fas fa-chevron-right"></i>
                                                    </a>
                                                @endif
                                            @endforeach
                                        </div>
                                        @isset($subcategorias)
                                            <div class="subdropdown-items">
                                                @foreach($subcategorias as $key => $subs)
                                                    <div class="subdrop-item" data-order="{{ $key }}">
                                                        <h3 class="subdrop-title h5">{{$subs[0]->categoria->nome}}</h3>
                                                        <div class="subdrop-links form-row row-cols-2">
                                                            @foreach($subs as $sub)
                                                                <div class="col">
                                                                <a href="{{ route('shop.catalogo', ['subcategorias_id' => $sub->id]) }}">{{ $sub->nome }}</a>
                                                                </div>
                                                            @endforeach 
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endisset
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div id="app" class="pt-5">
        @yield('content')
    </div>

    <footer id="footer">
        <div class="footer-middle bg-light-gray">
            <div class="container-lg">
                <div class="form-row py-5">
                    <div class="d-none d-md-block col-md-4 col-lg-2">
                        <div class="footer-logo">
                            <img class="img-fluid" src="{{ asset('img/logo.png') }}"  alt="Improve bazar">
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="row">
                            <div class="col-5 offset-md-1 col-lg-auto">
                                <div class="single-widget">
                                    <h4 class="text-dark-yellow">Compra rápida</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link" href="#">Moda masculina</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Moda feminina</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Produtos unisex</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-5 offset-md-1 col-lg-auto">
                                <div class="single-widget">
                                    <h4 class="text-dark-yellow">Sobre nós</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link" href="#">Companhia</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Telefone</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Email</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="w-100 d-lg-none"></div>
                            <div class="col-5 offset-md-1 col-lg-auto">
                                <div class="single-widget">
                                    <h4 class="text-dark-yellow">Serviços</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link" href="#">Ajuda online</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Contate-nos</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">FAQ</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-5 offset-md-1 col-lg-auto">
                                <div class="single-widget">
                                    <h4 class="text-dark-yellow">Políticas</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a href="#" class="nav-link">Termos de uso</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Privacidade</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#">Devolução</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
    
        </div>
        <div class="footer-bottom bg-orange">
            <p class="text-center text-white p-3 m-0">Copyright © 2020 Improve Bazar. Todos os direitos reservados</p>
        </div>
    </footer>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    

    <!-- Bootstrap, jquery and pooper core JavaScript-->
    <script src="{{ asset('js/dependencies/bootstrap.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/dependencies/jquery.easing.min.js') }}"></script>
   
    <!--Custom scripts for this template-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/shop.js') }}"></script>

    @hasSection('pl-custom-scripts')
        @yield('pl-custom-scripts')
    @endif
</body>
</html>
