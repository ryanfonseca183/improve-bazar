@extends('layouts.shop')

@section('pl-custom-styles')
    <style>
        #highlights .carousel-item:nth-child(1) .highlight {
            background-image: url('{{ asset("img/kid1.png") }}'), url('{{ asset("img/shape.png") }}');
        }
        #highlights .carousel-item:nth-child(2) .highlight {
            background-image: url('{{ asset("img/kid2.png") }}'), url('{{ asset("img/shape.png") }}');
        }
        #highlights .carousel-item:nth-child(3) .highlight {
            background-image: url('{{ asset("img/kid3.png") }}'), url('{{ asset("img/shape.png") }}');
        }
    </style>
@endsection


@section('content')
    <main class="container-md">
        {{-- PROMOÇÕES EM DESTAQUE --}}
        <div id="highlights" class="carousel slide mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#highlights" data-slide-to="0" class="active"></li>
                <li data-target="#highlights" data-slide-to="1"></li>
                <li data-target="#highlights" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item py-5 active">
                    <div class="highlight">
                        <div class="col-sm-6">
                            <h1>IMPROVE</h1>
                            <h2>Item 1.</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <button type="button" class="btn btn-primary">Comprar agora</button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item py-5">
                    <div class="highlight">
                        <div class="col-sm-6">
                            <h1>IMPROVE</h1>
                            <h2>Item 2</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <button type="button" class="btn btn-primary">Comprar agora</button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item py-5">
                    <div class="highlight">
                        <div class="col-sm-6">
                            <h1>IMPROVE</h1>
                            <h2>Item 3</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <button type="button" class="btn btn-primary">Comprar agora</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- SESSÃO DE PRODUTOS NOVOS --}}
        @if(!$promocoes->isEmpty())
            <x-products-section :produtos="$promocoes" nome="PROMOÇÕES"></x-products-section>
        @endif
        @if(!$recomendados->isEmpty())
            <x-products-section :produtos="$recomendados" nome="RECOMENDADOS"></x-products-section>
        @endif
        @if(!$lancamentos->isEmpty())
            <x-products-section :produtos="$lancamentos" nome="LANÇAMENTOS"></x-products-section>
        @endif
    </div>
@endsection
