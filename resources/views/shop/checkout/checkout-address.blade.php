@extends('layouts.checkout')
@section('title', 'Finalizar a compra')

@section('page-content')
    <h1 class="h5">Endereço de entrega</h1>
    <form action="{{ route('address.store') }}" method="POST" class="needs-validation" id="address_form" novalidate>
        @csrf
        <input type="hidden" name="redirect" value="shop.checkout">
        <div class="card card-body" id="main-content">
            <div class="form-row" id="required-fields">
                <div class="form-group col-sm-6">
                    <label for="cep">CEP 
                        <a target="_blank" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="ml-2">Não sei meu CEP</a>
                    </label>
                    <input type="text" required maxlength="9" value="{{ old('cep')}}" class="form-control mb-2 @if($errors->has('cep')) is-invalid @endif" id="cep" name="cep">
                    <span class="invalid-feedback" id="info_cep">
                        <strong>
                            @error("cep")
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="w-100"></div>
                <div class="form-group col-sm">
                    <label for="uf">Estado</label>
                    <input id="uf" name="estado" value="{{ old('estado')}}" required maxlength="255" class="form-control @error('estado') is-invalid @enderror">
                    <span class="invalid-feedback" id="info_uf">
                        <strong>
                            @error('bairro')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="form-group col-sm">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" name="cidade" value="{{ old('cidade')}}" required maxlength="255" class="form-control @error('cidade') is-invalid @enderror">
                    <span class="invalid-feedback" id="info_cidade">
                        <strong>
                            @error('bairro')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="w-100"></div>
                <div class="form-group col-sm-6">
                    <label for="bairro">Bairro</label>
                    <input type="text" id="bairro" name="bairro" value="{{ old('bairro')}}" required maxlength="255" class="form-control @error('bairro') is-invalid @enderror">
                    <span class="invalid-feedback" id="info_bairro">
                        <strong>
                            @error('bairro')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
            
                <div class="w-100"></div>
                <div class="form-group col-8 col-md-6">
                    <label for="rua">Rua</label>
                    <input type="text" id="rua" required name="rua" value="{{ old('rua')}}" maxlength="255" class="form-control @error('rua') is-invalid @enderror">
                    <span class="invalid-feedback" id="info_rua">
                        <strong>
                            @error('rua')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
            
                <div class="form-group col-4 col-md-6">
                    <label for="numero">Número</label>
                    <input type="number" id="numero" name="numero" value="{{ old('numero') }}" required class="form-control @error('numero') is-invalid @enderror">
                    <span class="invalid-feedback" id="info_numero">
                        <strong>
                            @error('numero')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-sm-6">
                    <label for="complemento">Complemento <span class="small">(Opcional)</span></label>
                    <input type="text" id="complemento" name="complemento" value="{{ old('complemento') }}" maxlength="255" class="form-control @error('complemento') is-invalid @enderror" placeholder="Ex: No final da rua">
                    @error('complemento')
                        <span class="invalid-feedback">
                            <strong>
                                {{ $message }}
                            </strong>
                        </span>
                    @enderror
                </div>
                <div class="w-100"></div>
                <div class="form-group col-sm-6">
                    <label for="telefone">Telefone fixo <span class="small"> (Opcional)</span></label>
                    <input type="text" id="telefone" name="telefone" value="{{ old('telefone') }}" class="form-control @error('telefone') is-invalid @enderror">
                    @error('telefone')
                        <span class="invalid-feedback">
                            <strong>
                                {{ $message }}
                            </strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="float-right my-5">
            <a href=" {{ route('user.profile') }} " class="btn btn-secondary btn-lg mr-2">Cancelar</a>
            <button type="button" onclick="checkGroupValidity('required-fields', 'submit', 'address_form')" class="btn btn-primary btn-lg">Próximo</button>
        </div>
    </form>
@endsection


@section('pl-custom-scripts')
    @parent
    <script>
        $("#cep").mask('00000-000');
        $("#telefone").mask('(00)0000-0000');
        $("#cep").change(function(){
            consultarCEP($(this).val());
        })
        function consultarCEP(cep) {
            $.getJSON( "http://viacep.com.br/ws/"+ cep +"/json/", function( data ) {
                if(!('erro' in data)){
                    $("#uf").val(data.uf);
                    $("#cidade").val(data.localidade);
                    $("#bairro").val(data.bairro);
                    $("#rua").val(data.logradouro);
                    $("#complemento").val(data.complemento);
                    $("#cep").removeClass("is-invalid");
                } else {
                    $("#cep").addClass('is-invalid');
                    $("#info_cep strong").html('CEP não encontrado. Por favor, tente novamente');
                }
            }); 
        }
    </script>
@endsection