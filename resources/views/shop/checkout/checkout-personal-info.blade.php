@extends('layouts.checkout')
@section('title', 'Finalizar a compra')

@section('pl-custom-styles')
    @parent
    <style>
      #main-content {
        padding: 2rem 2rem 3rem;
        border-radius: 0.25rem;
        box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
        background:white;
      }
   </style>   
@endsection

@section('page-content')
    <h1 class="h5">Informações pessoais</h1>
    <form action="{{ route('user.profile.update') }}" method="POST" class="needs-validation" id="profile_form" novalidate>
        @csrf
        @method('PUT')
        <input type="hidden" name="redirect" value="shop.checkout">
        <div class="card card-body" id="main-content">
            <div class="form-row" id="required-fields">
                <div class="form-group col-sm-6">
                    <label for="name">Nome completo</label>
                    <input type="text" required value="{{ Auth::user()->name  }}" class="form-control mb-2 @error('name') is-invalid @enderror" id="name" name="name">
                    <span class="invalid-feedback" id="info_name">
                        <strong>
                            @error('name')
                                {{ $message }}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="form-group col-sm">
                    <label for="cpf">CPF</label>
                    <input type="text" required value="{{ Auth::user()->cpf }}" class="form-control @error('cpf') is-invalid @enderror" id="cpf" name="cpf" >
                    <span class="invalid-feedback" id="info_cpf">
                        <strong>
                            @error('cpf')
                                {{ $message }}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="w-100"></div>
                <div class="form-group col-sm">
                    <label for="celular">Celular</label>
                    <input type="text" required value="{{  Auth::user()->celular  }}" class="form-control @error('celular') is-invalid @enderror" id="celular" name="celular" >
                    <span class="invalid-feedback" id="info_celular">
                        <strong>
                            @error('celular')
                                {{ $message }}
                            @enderror
                        </strong>
                    </span>
                </div>
                <div class="form-group col-sm-6">
                    <label for="bairro">Nascimento</label>
                    <input type="date" required value="{{ Auth::user()->nascimento }}" class="form-control @error('bairro') is-invalid @enderror" id="nascimento" name="nascimento" >
                    <span class="invalid-feedback" id="info_nascimento">
                        <strong>
                            @error('nascimento')
                                {{$message}}
                            @enderror
                        </strong>
                    </span>
                </div>
                
            </div>
        </div>
        <div class="float-right my-5">
            <a href=" {{ route('user.profile') }}" class="btn btn-secondary btn-lg mr-2">Cancelar</a>
            <button type="button" onclick="checkFormValidity('profile_form')" class="btn btn-primary btn-lg">Próximo</button>
        </div>
    </form>
@endsection


@section('pl-custom-scripts')
    @parent
    <script>
        $("#cpf").mask('000.000.000-00');
        $("#celular").mask('(00) 00000-0000');
    </script>
@endsection