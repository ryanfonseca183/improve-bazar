@extends('layouts.checkout')
@section('title', 'Finalizar a compra')

@section('pl-custom-styles')
    @parent
    <style>
        .custom-controls-container {
            margin-bottom: 2rem;
        }
        .formStep:not(#formStep1) {
            display:none;
        }
        .custom-radio-label {
            display:flex;
            align-items:center;
            background:white;
            border-radius: 0.5rem;
            padding: 0 1rem;
            cursor:pointer;
            box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
        }
        .custom-radio-text {
            padding: 1rem;
            flex: 1;
        }
        .custom-radio-text p {
            font-size: 80%;
        }
        
        #payment_methods {
            box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
            overflow:hidden;
            border-radius: 0.25rem;
        }
        .payment-method-label {
            display:inline-flex;
            flex-direction:column;
            text-align:center;
            background: white;
            padding: 1rem;
            cursor:pointer;
            width: 100%;
            margin-bottom: 0;
        }
        .payment-method-label:hover {
            background: #f2f2f2;
        }
        .payment-control {
            position:absolute;
            opacity: 0;
        }
        .payment-control:checked + .payment-method-label {
            color: white;
            background-color: var(--laranja);
        }
        .payment-name {
            margin-bottom: 0;
            font-size: 1rem;
        }
        .payment-icon {
            font-size: 1.5rem;
            margin-bottom: 1rem;
        }
        @media only screen and (min-width: 768px) {
            .custom-radio-label {
                padding: 0 2rem;
            }
            .custom-radio-text {
                padding: 1.50rem 2rem;
            }
            .custom-radio-text p {
                font-size: 100%;
            }
            .payment-method-label {
                align-items:center;
                flex-direction: row;
            }
            .payment-icon {
                font-size: 2rem;
                margin-right: 1rem;
                margin-bottom: 0;
                text-align:unset;
            }
        }
   </style>   
@endsection

@php 
    $pac = $summary['frete']['pac'];
    $sedex = $summary['frete']['sedex'];
    $total = $summary['total'];
@endphp
@section('page-content')
    {{-- FORMULÁRIO DE PAGAMENTO --}}
    <form action="{{ route('shop.checkout.order') }}" method="POST" id="checkout_form" novalidate>
        @csrf
        {{-- ENDEREÇO DE ENTREGA --}}
        <h2 class="h5">Endereço de entrega</h2>
        <div class="card card-body border-0 mb-4 shadow-sm">
            <div class="row align-items-center">
                <div class="col-auto">
                    <i class="fas fa-map-marker-alt address-icon"></i>
                </div>
                <div class="col">
                <address class="mb-0">
                    {{$address->rua}}, {{$address->numero}}<br>
                    {{$address->cidade}}, {{$address->uf_sigla}} - {{$address->cep}}
                </address>
                </div>
                <div class="col-auto">
                <a href="{{ route('address.edit', $address->id) }}" class="p-3">Editar</a>
                </div>
            </div>
            
        </div>

        {{-- FRETE DO PRODUTO --}}
        <div id="formStep1" class="formStep">
            <h2 class="h5 mb-0">Opções de envio</h2>
            <p>Selecione a opção de envio</p>
            @php $pac = $summary['frete']['pac'] @endphp
            <div class="custom-controls-container">
                <label class="custom-radio-label">
                    <div class="custom-radio-control-container">
                        <input type="radio" class="custom-radio-control" value="pac" @if(old('shipping-option') == 'pac') checked @endif name="shipping-option">
                    </div>
                    <div class="custom-radio-text">
                        <h3 class="h6 mb-0">PAC</h3>
                    <p class="text-success mb-0"><strong>Chegará até o dia {{ $pac['deadline'] }}</strong></p>
                    </div>
                    <div class="custom-radio-value">
                        <strong>R$ {{$pac['price'] }}</strong>
                    </div>
                </label>
                <label class="custom-radio-label">
                    <div class="custom-radio-control-container">
                        <input type="radio" class="custom-radio-control" value="sedex" name="shipping-option" required @if(old('shipping-option') == null) checked @elseif(old('shipping-option') == 'sedex') checked @endif>
                    </div>
                    <div class="custom-radio-text">
                        <h3 class="h6 mb-0">SEDEX</h3>
                    <p class="text-success mb-0"><strong>Chegará até o dia {{ $sedex['deadline'] }}</strong></p>
                    </div>
                    <div class="custom-radio-value">
                        <strong>R$ {{$sedex['price'] }}</strong>
                    </div>
                </label>
                @error("shipping-option")
                    <span class="invalid-feedback d-block">
                        <strong>
                            Por favor, selecione um método de envio
                        </strong>
                    </span>
                @enderror
            </div>
        </div>
        
        {{-- MÉTODO DE PAGAMENTO --}}
        @php $metodosPagamento = App\MetodoPagamento::all() @endphp
        <div id="formStep2" class="formStep @if($errors->hasAny(['payment-option', 'name-owner', 'cpf-owner', 'card-number', 'cvv', 'expiration-date'])) d-block @endif">
            <h2 class="h5 mb-0">Método de pagamento</h2>
            <p>Selecione o método de pagamento</p>
            <div id="payment_methods" class="@if($errors->has('payment-option')) mb-2 @else mb-4 @endif">
                <div class="row no-gutters">
                    @foreach($metodosPagamento as $key => $mtd) 
                        <div class="payment-control-container col">
                        <input type="radio" class="payment-control" id="payment_{{ $key }}" value="{{ $mtd->id }}" name="payment-option" data-related="#{{$mtd->label}}" @if(old('payment-option') == $mtd->id) checked @endif required>
                            <label class="payment-method-label" for="payment_{{ $key }}">
                                <i class="{{$mtd->icone}} payment-icon"></i>
                                <h3 class="payment-name">{{$mtd->nome}}</h3>
                            </label>
                        </div>
                    @endforeach
                </div>
                
                {{-- CAMPOS PARA PAGAMENTO NO BOLETO --}}
                <div class="card card-body rounded-0 collapse" id="boleto">
                    <p>O boleto será gerado após a confirmação da compra</p>
                </div>

                {{-- CAMPOS PARA PAGAMENTO NO CARTÃO --}}
                <div class="card card-body rounded-0 collapse" id="cartao">
                    <div class="form-row">
                        <div class="form-group col-sm">
                            <label for="name-owner">Nome do titular</label>
                            <input type="text" class="form-control @if($errors->has('name-owner')) is-invalid @endif" id="name-owner" name="name-owner" required disabled value="{{ Auth::user()->name }}">
                            <span class="invalid-feedback">
                                <strong>
                                    @error('name-owner')
                                        {{$message}}
                                    @else 
                                        O nome do titular é obrigatório
                                    @endif
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-sm">
                            <label for="cpf-owner">CPF do titular</label>
                            <input type="text" class="form-control @if($errors->has('cpf-owner')) is-invalid @endif" id="cpf-owner" name="cpf-owner" required disabled value="{{ Auth::user()->cpf }}">
                            <span class="invalid-feedback">
                                <strong>
                                    @error('cpf-owner')
                                    {{$message}}
                                    @else 
                                        O cpf do titular é obrigatório
                                    @endif
                                </strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="card-number">Número do cartão</label>
                        <input type="text" class="form-control @if($errors->has('card-number')) is-invalid @endif" id="card-number" name="card-number" value="{{ old('card-number') }}" required placeholder="0000 0000 0000 0000" disabled>
                        <span class="invalid-feedback">
                            <strong>Por favor, informe um número de cartão válido</strong>
                        </span>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="expiration-date">Data de vencimento</label>
                            <input type="text" class="form-control @if($errors->has('expiration-date')) is-invalid @endif" id="expiration-date" name="expiration-date" value="{{ old('expiration-date')}}" required disabled>
                            <span class="invalid-feedback">
                                <strong>Por favor, informe um mês e ano válidos</strong>
                            </span>
                        </div>
                        <div class="form-group col">
                            <label for="cvv">CVV</label>
                            <input type="text" class="form-control" id="cvv" name="cvv" value="{{ old('cvv') }}" required disabled>
                        </div>
                    </div>
                </div>
            </div>
            @if($errors->has('payment-option'))
                <span class="invalid-feedback d-block">
                    <strong>Por favor, selecione um método de pagamento</strong>
                </span>
            @endif
        </div>

        {{-- NUMERO DE PARCELAS NO CARTÃO --}}
        <div id="formStep3" class="formStep">
            <h2 class="h5 mb-0">Número de parcelas</h2>
            <p>Selecione o número de parcelas</p>
            <div class="custom-controls-container">
                @for($i = 1; $i < 7; $i++)
                    <label class="custom-radio-label">
                        <div class="custom-radio-container">
                            <input type="radio" class="custom-radio-control" name="num-parcels" value="{{ $i }}" @if($i == 1) required checked @endif disabled>
                        </div>
                        <div class="custom-radio-text">
                            <h3 class="h6 mb-0">{{ $i }}x</h3>
                            <p class="text-success mb-0"><strong>Sem juros</strong></p>
                        </div>
                        <div class="custom-radio-value">
                            <strong>R$ {{ number_format($total / $i, 2, ',', '.') }}</strong>
                        </div>
                    </label>
                @endfor
            </div>
        </div>
        
        {{-- BOTÕES DE AÇÕES --}}
        <div class="form-row justify-content-md-end my-5">
            <div class="col col-md-3">
                <a href="{{ route('user.bag') }}" class="btn btn-secondary btn-lg btn-block mr-2">Cancelar</a>
            </div>
            <div class="col col-md-3">
                <button type="button" onclick="changeformStep()" class="btn btn-primary btn-block btn-lg">Próximo</button>
            </div>
        </div>
    </form>
@endsection


@section('pl-custom-scripts')
    @parent
   <script>
        $(function(){
            // DEFINE AS MÁSCARAS PARA OS CAMPOS DO CARTÃO DE CRÉDITO
            $("#cpf-owner").mask('000.000.000-00')
            $("#card-number").mask('0000-0000-0000-0000')
            $("#cvv").mask('000');
            $("#expiration-date").mask('00/0000', {placeholder: "__/____"});

            // CARREGA A ETAPA DO MÉTODO DE PAGAMENTO
            let payment = $(".payment-control:checked");
            if(payment.length != 0) {
                $("#formStep2").show(400);
                selectPaymentMethod(payment);
            }
        })
        $("[name='payment-option']").change(function(){
           selectPaymentMethod($(this));
        })
        $("[name='shipping-option']").change(function(){
           selectShippingOption($(this));
        })
        let formStep = $("#formStep1");
        function changeformStep() {
            //VALIDANDO A ETAPA
            formStep.addClass("was-validated");
            let inputs = $("input:visible:not([type=submit]):not([type=color]):not([type=button])", formStep);
            let flag = validate(inputs);
            //MUDANDO DE ETAPA
            if(flag) {
                if(formStep.prop('id') == "formStep2") {
                    let paymentOption = $("[name='payment-option']:checked").attr('data-related')
                    if(paymentOption == "#boleto") {
                        $("#checkout_form").submit();
                        return;
                    }
                }
                formStep = $(".formStep:not(:visible)").first();
                if(formStep.length != 0) {
                    formStep.show(400);
                    $([document.documentElement, document.body]).animate({
                        scrollTop: ($(formStep).offset().top - 100)
                    }, 1000);
                } else {
                    $("#checkout_form").submit();
                }
            }
        }
        function selectShippingOption(element) {
            $.ajax({
                url: "{{ route('shop.checkout.shipping') }}",
                method: "PUT",
                data: {
                    '_token': '{{ csrf_token() }}',
                    'shipping-option': element.val(),
                },
                beforeSend: function() {
                    $("[name='shipping-option']").prop('disabled', true);
                },
                success:function(response) {
                    $("#shipping-price").html("R$ " + response.frete);
                    $("#total").html("R$ " + response.total);
                },
                error:function(xhr) {
                    console.log(xhr)
                },
                complete: function() {
                    $("[name='shipping-option']").prop('disabled', false);
                }
            })
        }
        let collapse = null;
        function selectPaymentMethod(element) {
            //ESCONDE O MEIO DE PAGAMENTO ANTERIOR
            if(collapse) {
                collapse.hide(400);
                $("input", collapse).prop('disabled', true);
            }

            // MOSTRA O MEIO DE PAGAMENTO SELECIONADO
            collapse = $($(element).attr('data-related'));
            collapse.toggle(400);
            $("input", collapse).prop('disabled', false);

            // VERIFICA O MEIO DE PAGAMENTO SELECIONADO
            if(collapse.prop('id') != "cartao") {
                $("#formStep3").hide(400);
                $("#formStep3 input").prop('disabled', true);
                formStep = $("#formStep2");
            } else {
                $("#formStep3 input").prop('disabled', false);
            }
        }
   </script>
@endsection