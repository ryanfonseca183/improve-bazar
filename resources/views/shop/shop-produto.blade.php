@extends('layouts.shop')

@section('title', $produto->nome)

@section('pl-custom-styles')
    <style>
        /* FOTOS DO PRODUTO */
        #container-photos {
            border: 1px solid #e3e6f0 !important;
            border-radius: 0.35rem !important;
        }
        #preview-photos {
            border-bottom: 1px solid #e3e6f0 !important;
            position: relative;
        }
        #main-photo {
            display:flex;
            align-items:center;
            height: 300px;
        }
        #main-photo img {
            max-width: 100%;
            max-height: calc(300px - 2rem);
            display:block;
            margin: 0 auto;
        }
        .photo-wrapper {
            overflow: hidden;
            height: 100px;
        }
        .photo-wrapper.last {
            border-bottom: none !important;
        }
        .photo-wrapper:nth-child(1) {
            border-right: 1px solid #e3e6f0;
        }
        .photo-wrapper:nth-child(2) {
            border-right: 1px solid #e3e6f0;
        }
        .other-photos {
            display:block;
            margin: 0 auto;
            max-width: 100%;
            max-height: 100px;
            padding: 0.25rem;
            transition: transform 0.2s ease-in-out;
            cursor:zoom-in;
        }
        .other-photos:hover {
            transform: scale(1.2);
        }

        /* SLIDE PARA VER TODAS AS FOTOS */
        #preview-photos:hover #show-slide-photos {
            pointer-events: all;
            opacity: 1;
        }
        #show-slide-photos {
            position: absolute;
            top: 50%;
            right: 0;
            transform: translate(50%, -50%);
            display:block;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            padding: 0;
            opacity: 0;
            pointer-events: none;
            background-color: var(--laranja);
            color:white;
            transition: opacity 0.3s ease-in-out;
            font-size: 20px;
        }
        #slidePhotos .carousel-control-prev span, #slidePhotos .carousel-control-next span{
            background-color: var(--laranja);
            border-radius: 50%;
            width: 40px;
            height: 40px;
            background-size: 100% 40%;
        }
        #slidePhotos .carousel-indicators li {
            background-color: darkgray;
        }
        #slidePhotos .carousel-indicators li.active {
            background-color: var(--laranja);
        }
        .slide-photo {
            display:block;
            margin: 0 auto;
            max-width: 100%;
            max-height: 500px;
        }
        @media only screen and (min-width: 578px) {
            .other-photos {
                max-height: 100px;
            }
            #preview-photos {
                border-bottom: none !important;
                border-right: 1px solid #e3e6f0;
            }
            .photo-wrapper {
                border-bottom: 1px solid #e3e6f0;
                border-right: none !important;
            }
            #show-slide-photos {
                top: unset;
                right: unset;
                bottom: 0px;
                left: 50%;
                transform: translate(-50%, 50%);
            }
        }
        @media only screen and (min-width: 992px) {
            #main-photo {
                height: 500px;
            }
            #main-photo img {
                max-height: calc(500px - 2rem);
            }

        }
        /* FORMULÁRIO PARA ADICIONAR AO CARRINHO */
        .input-info {
            margin-bottom: 0.75rem;
        }
        #loadReviews {
            display:block;
            margin: 0 auto;
        }
        #input-group-quantity {
            background-color: #f8f9fc !important;
            border-radius: 0.35rem;
            display:inline-flex;
            width: auto;
            margin-bottom: 1rem;
        }
        #quantity {
            max-width: 50px;
            border:none;
            box-shadow:none !important;
            text-align:center;
            background:transparent !important;
            -moz-appearance: textfield;
        }
        #quantity::-webkit-outer-spin-button,
        #quantity::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        .btn-increment {
            padding: 0 0.25rem;
            box-shadow:none;
            font-size: 11px;
        }
        .btn-increment:hover {
            color: var(--laranja);
        }
        #quantity-avaliable {
            margin-left: 0.5rem !important;
            font-size: 80%;
            font-weight: 400;
        }
        .alert-quantity i {
            transform: rotate(180deg);
            font-size: 25pt;
            margin-right: 0.5rem;
        }
        #product_sku {
            display:inline-block;
            font-size:1rem;
            margin-bottom: 0;
        }
        #product_name {
            font-size: 1.25rem;
            margin-bottom: 0;
            font-weight: 700;
        }
        #product_price {
            margin-bottom: 1rem;
        }
    </style>
@endsection
@php
    $fotos_produto = $produto->fotosProduto;
    $foto_principal = $fotos_produto->where('is_main', 1)->first();
    $outras_fotos = $fotos_produto->where('is_main', 0);
    $ultima_foto = $outras_fotos->last();
    $avg = (float) $produto->avaliacoes()->avg('valor');
    $filho = $produto;
    if($produto->product_type == 2) {
        //BUSCA A CONFIGURAÇÃO MAIS BARATA DISPONÍVEL EM ESTOQUE
        $filho = $produto->filhos()
        ->orderBy('is_avaliable', 'desc')
        ->orderBy('preco', 'asc')
        ->first();
        
        //RECUPERA OS ATRIBUTOS CONFIGURAÇÕES
        $atributos = $filho->atributos()->where('is_configurable', 1)->get();
    }
    $preco = $filho->preco;
    $desconto = $filho->desconto;
    if($preco != $desconto) $hasPromo = 1; else $hasPromo = 0;
    $configUnique = [];
@endphp
@section('content')
    <main class="container-md">
        {{-- CLASSIFICAÇÃO E FAVORITOS --}}
        <div class="form-row flex-between mb-4">
            <div class="col-auto">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                      <li class="breadcrumb-item">{{$produto->subcategoria->categoria->nome}}</li>
                      <li class="breadcrumb-item active" aria-current="page">{{ $produto->subcategoria->nome}}</li>
                    </ol>
                </nav>
            </div>
            <div class="col-auto">
                @if(Auth::check() && Auth::user()->favoritos->produtos()->where('produtos.id', $produto->id)->exists())
                    <span class="btn-action">
                        <i class="fas fa-heart text-orange"></i>
                    </span>
                @else 
                    <form action="{{ route('favorites.produto.store', [$produto->id]) }}" method="POST">
                        @csrf
                        <button class="btn btn-action" type="submit">
                            <i class="fas fa-heart"></i>
                        </button>
                    </form>
                @endif
            </div>
        </div>
        {{-- INFORMAÇÕES DO PRODUTO --}}
        <div class="row">
            {{-- FOTOS DO PRODUTO --}}
            <div class="col-lg-8 mb-5">
                <div id="container-photos">
                    <div class="row no-gutters">
                        <div class="col-sm-2" id="preview-photos">
                            <div class="row no-gutters">
                                <div class="col-4 col-sm-12 photo-wrapper">
                                    <img src="{{ asset('storage/' . $foto_principal->caminho) }}" class="other-photos" />
                                </div>
                                @foreach($outras_fotos->take(4) as $key => $fto)
                                    <div class="col-4 col-sm-12 photo-wrapper @if($fto->id == $ultima_foto->id) last @endif @if($key > 2) d-none d-sm-block @endif">
                                        <img src="{{ asset('storage/'. $fto->caminho) }}" alt="{{ $produto->nome }}" class="other-photos ">
                                    </div>
                                @endforeach
                            </div>
                            @if($fotos_produto->count() >= 3)
                                <button type="button" class="btn @if($fotos_produto->count() <= 5) d-sm-none @endif" id="show-slide-photos" data-toggle="modal" data-target="#containerSlidePhotos">
                                    <i class="fas fa-chevron-down m-0 d-none d-sm-block"></i>
                                    <i class="fas fa-chevron-right m-0 d-sm-none"></i>
                                </button>
                            @endif
                        </div>
                        <div class="col-sm p-3" id="main-photo">
                            <img src="{{ asset('storage/' . $foto_principal->caminho) }}" />
                        </div>
                    </div>
                </div>
            </div>

            {{-- INFORMAÇÕES GERAIS DO PRODUTO --}}
            <div class="col-lg mb-5">
                <div class="mx-auto">
                    <div id="product_stock">
                        {{-- SKU --}}
                        <h2 id="product_sku"> {{ $filho->sku }} </h2>

                        {{-- STATUS DO PRODUTO --}}
                        <span id="product_status" class="badge badge-pill @if($filho->is_avaliable) badge-primary @else badge-danger @endif ml-2" >
                            @if($filho->is_avaliable) Em estoque @else Sem estoque @endif
                        </span>
                        <hr>
                    </div>
                    

                    {{-- LINK PARA O PAINEL --}}
                    @if(Auth::check() && Auth::user()->is_admin)
                        <a class="mb-2 d-inline-block" href="{{ route('produto.show', [$produto->id]) }}"><i class="text-orange mr-1 fas fa-tachometer-alt" title="Painel de controle"></i> Ver produto no painel</a>
                    @endif

                    {{-- NOME DO PRODUTO --}}
                    <h1 id="product_name">{{$filho->nome}}</h1>

                   
                    <div class="mb-3">
                        {{-- AVALIAÇÃO MÉDIA --}}
                        @if($produto->avaliacoes()->count() >= 3)
                            <div class="stars-outer stars-sm mt-1">
                                <div class="stars-inner" style="width: {{20 *  $avg}}%"></div>
                            </div>
                        @endif
                    </div>

                    {{-- PREÇO --}}
                    <div id="product_price">
                        <span class="old-price @if(!$hasPromo) d-none @endif" id="product_old_price">R$ {{ number_format($preco, 2, ',', '.') }} </span>
                        <div class="vflex-center">
                            <h2 class="current-price" id="product_current_price">R$ {{ number_format($desconto, 2, ',', '.') }}</h2>
                            <span class="badge badge-success @if(!$hasPromo) d-none @endif ml-2" id="product_descount">{{ $produto->promo_value }}% OFF </span>
                        </div>
                    </div>
                    
                    {{-- VARIAÇÕES DO PRODUTO --}}
                    @isset($atributos)
                        <form id="option-group-form">
                            <div class="option-group-container mb-2">
                                @foreach($atributos as $atr)
                                    @php 
                                        //CARREGA AS OPÇÕES DE CONFIGURAÇÃO DO PRODUTO
                                        $produtos = $atr->produtos()->select('produtos.id')->where('produtos.produtos_id', $produto->id)->get();
                                        $selected = $atr->pivot->valor;
                                        $array = collect([]);
                                        foreach($produtos as $prod){
                                            if(!$array->contains([$prod->pivot->valor, $prod->pivot->label])) {
                                                $array->push([$prod->pivot->valor, $prod->pivot->label]);
                                            }
                                        }
                                        //SE UM ATRIBUTO CONFIGURÁVEL FOR UNICO PARA TODAS AS VARIAÇÕES, É EXIBIDO JUNTAMENTE COM OS ATRIBUTOS SIMPLES
                                        if($array->count() == 1) $configUnique[] = [$atr->nome, $array[0][0], $array[0][1]];
                                    @endphp
                                    {{-- EXIBE AS OPÇÕES DE CONFIGURAÇÃO --}}
                                    @if($array->count() > 1)
                                        <div class="option-group ">
                                            <span class="option-group-title">{{$atr->nome}}</span>
                                            <div class="form-row">
                                                @foreach($array as $key => $val)
                                                    @if($atr->tipoDedados->frontend_type_label == "Cor")
                                                        <div class="col-auto radio-button-color">
                                                            <input 
                                                            type="radio"
                                                            class="radio-button-option" 
                                                            name="{{ $atr->label }}"
                                                            value="{{ $val[0] }}"
                                                            @if($val[0] == $selected) checked @endif
                                                            id="{{ $atr->label . "_" . $key }}"
                                                            required>
                                                            <label
                                                            for="{{ $atr->label . "_" .  $key }}" 
                                                            title="{{$val[1] }}" 
                                                            data-toggle="tooltip" 
                                                            data-placement="top"
                                                            class="radio-button-color-show" 
                                                            style="background-color:{{ $val[0] }}"></label>
                                                            <span class="sr-only">{{$val[1]}}</span>
                                                        </div>
                                                    @else 
                                                        <div class="col-auto radio-button-text">
                                                            <input
                                                            type="radio"
                                                            class="radio-button-option"
                                                            name="{{ $atr->label }}" 
                                                            @if($val[0] == $selected) checked @endif
                                                            value="{{ $val[0] }}" 
                                                            id="{{ $atr->label . "_" . $key }}"
                                                            required>
                                                            <label 
                                                            for="{{ $atr->label . "_" . $key }}" 
                                                            class="radio-button-text-show">
                                                                {{$val[0]}}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </form>
                    @endisset

                    {{-- ESTOQUE INDISPONÍVEL --}}
                    <div class="alert alert-danger alert-quantity @if($filho->is_avaliable == 1) d-none @endif">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <i class="fas fa-backspace"></i>
                            </div>
                            <div class="col">Estoque indisponível. Por favor, tente outras opções.</div>
                        </div>
                    </div>

                    {{-- QUANTIDADE --}}
                    <div class="form-row @if($filho->is_avaliable == 0) d-none @endif" id="quantity-container">
                        <label for="quantity" class="col-sm-auto col-form-label">Quantidade</label>
                        <div class="col-sm">
                            <form action="{{route('bag.produto.store')}}" method="POST" id="add_to_bag">
                                @csrf
                                <input type="hidden" name="produto_id" id="produto_id" value="{{$filho->id}}">
                                <div class="input-group" id="input-group-quantity">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-increment" id="minus">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                    <input type="number" name="quantidade" id="quantity" min="1" max="{{ $filho->quantidade }}" value="1" class="form-control">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-increment" id="more">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <span id="quantity-avaliable">({{ $filho->quantidade }} disponíveis)</span>
                            </form>
                        </div>
                        <div class="col-12">
                            <button type="button" onclick="validateProduct()" class="btn btn-lg btn-block btn-primary">Adicionar à sacola</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- DESCRIÇÃO E CARACTERÍSTICAS DO PRODUTO --}}
        <div class="row">
            {{-- DESCRIÇÃO DO PRODUTO --}}
            @isset($produto->descricao)
                <div class="col-lg-8 mb-4">
                    <h2 class="h5">Descrição</h2>
                    <pre class="product-description">{{$produto->descricao}}</pre>
                </div>
            @endisset

            {{-- CARACTERÍSTICAS DO PRODUTO --}}
            @if($filho->atributos()->where('is_configurable', 0)->exists())
                <div class="col-lg-8 mb-4">
                    <h2 class="h5">Características</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered ">
                            <tbody>
                                @foreach($filho->atributos()->where('is_configurable', 0)->get() as $atr)
                                    <tr>
                                        <td>{{$atr->nome}}</td>
                                        @if($atr->tipoDeDados->frontend_type_label == "Cor")
                                            <td>{{$atr->pivot->label}}</td>
                                        @else
                                            <td>{{$atr->pivot->valor}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                @foreach($configUnique as $values)
                                    <tr>
                                        <td>{{$values[0]}} </td>
                                        <td> {{ isset($values[2]) ? $values[2] : $values[1] }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td>Dimensões</td>
                                    <td> {{ $produto->comprimento }} x {{ $produto->altura }} x {{ $produto->largura }} cm</td>
                                </tr>
                                <tr>
                                    <td>Peso</td>
                                    <td> {{ $produto->peso }} kg</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif

            {{-- AVALIAÇÃO DO PRODUTO --}}
            <div class="col-lg-8 mb-4">
                <h2 class="h5">Opiniões sobre o produto</h2>
                @if($produto->avaliacoes()->count() >= 3)
                    {{-- MEDIA DAS AVALIAÇÕES --}}
                    <div class="row align-items-center mb-4">
                        <div class="col">
                            <div class="stars-outer stars-lg">
                                <div class="stars-inner" style="width: {{20 *  $avg}}%"></div>
                            </div><br>
                            <span>(Media entre {{ $produto->avaliacoes()->count() }} avaliações)</span>
                        </div>
                        <div class="col-sm-auto order-sm-first">
                            <h1 class="display-1 mb-0"> {{number_format($avg, 1, '.', '.')}}</h1>
                        </div>
                    </div>
                    <div id="reviews">

                    </div>
                    <button class="btn btn-outline-primary" id="loadReviews" type="button" onclick="loadReviews()">Ver mais</button>
                @else 
                    <div class="alert alert-warning">
                        Este produto não possui avaliações suficientes para serem exibidas
                    </div>
                @endif
            </div>
        </div>

        {{-- MODAL COM FOTOS DO PRODUTO --}}
        @if($fotos_produto->count() > 3)
            <div class="modal fade" id="containerSlidePhotos" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div id="slidePhotos" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach($fotos_produto as $key => $fto) 
                                        <li data-target="#slidePhotos" data-slide-to="{{ $key }}" @if($key == 0) class="active" @endif></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner mb-3">
                                    @foreach($fotos_produto as $key =>  $fto)
                                        <div class="carousel-item @if($key == 0) active @endif">
                                            <img src="{{ asset('storage/' . $fto->caminho) }}" class="slide-photo">
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#slidePhotos" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#slidePhotos" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- PRODUTOS SEMELHANTES --}}
        @if(!$semelhantes->isEmpty())
            <x-products-section :produtos="$semelhantes" nome="SEMELHANTES"></x-products-section>
        @endif
    </main>
@endsection


@section('pl-custom-scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            
        })
        jQuery.fn.visible = function() {
            return this.css('visibility', 'visible');
        };
        jQuery.fn.invisible = function() {
            return this.css('visibility', 'hidden');
        };
        var quantityContainer = $("#quantity-container");
        var quantityAlert = $(".alert-quantity");
        var quantityNumber = $("#quantity-avaliable");
        var quantityInput = $("#quantity");
        var appName = "{{ config('app.name') }}";
        var name = '{{ $produto->nome }}';
        var productId = $("#produto_id");
        var productStock = $("#product_stock");
        var productPrice = $("#product_price");
        var productSKU = $("#product_sku");
        var productStatus = $("#product_status");
        var productName = $("#product_name");
        var productOldPrice = $("#product_old_price");
        var productCurrentPrice = $("#product_current_price");
        var productDescount = $("#product_descount");
        var hasPromo = {{ $hasPromo }};

        $(".other-photos").click(function(){
            $("#main-photo img").prop('src', this.src)
        })
        $("#minus").click(function(){
            var value = Number( quantityInput.val() );
            if(value > 1) quantityInput.val(--value);
        })
        $("#more").click(function(){
            let value = Number( quantityInput.val());
            let max = Number( quantityInput.attr('max'));
            if(value < max) quantityInput.val(++value);
        })
        quantityInput.on('input', function(){
            let max = Number($(this).prop('max'));
            let val = Number($(this).val());
            if(val > max) $(this).val(max); else if(val < 1) $(this).val(1);
        })
        function validateProduct() {
            let id = Number( productId.val());
            let quantity = Number(quantityInput.val());
            let quantityMax = Number(quantityInput.attr('max'));
            if(id != 0 && quantity >= 1 && quantity <= quantityMax) {
                $("#add_to_bag").submit();
            }
        }
        @if($produto->filhos()->exists())
            $(function(){
                let data = $("#option-group-form").serialize();
                getVariation(data);
            });
            $(".radio-button-option").change(function(){
                let data = $("#option-group-form").serialize();
                getVariation(data);
            })
            function getVariation(formData) {
                $.ajax({
                    url: "{{ route('produto.getVariation', $produto->id) }}",
                    type: "GET",
                    data: formData,
                    success: function(response){
                        let produto = JSON.parse(response);
                        if(produto) {
                            productId.val(produto.id);
                            productSKU.text(produto.sku)
                            productName.text(produto.nome)
                            productStock.visible();
                            productPrice.show();
                            document.title = produto.nome + " - " + appName;
                            if(hasPromo) {
                                productOldPrice.html("R$ " + format(produto.preco));
                                productDescount.html(produto.promo_value + "% OFF")
                            } 
                            productCurrentPrice.html('R$ ' + format(produto.desconto));
                            if(produto.is_avaliable == 1) setAvaliable(produto.quantidade); else setUnavaliable();
                        } else {
                            setUnavaliable();
                            productStock.invisible();
                            productPrice.hide();
                            productName.html(name);
                        }
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    }
                });
            }
            function setAvaliable(quantidade) {
                productStatus.html('Em estoque')
                productStatus.addClass('badge-primary');
                productStatus.removeClass('badge-danger');

                quantityAlert.addClass('d-none');
                quantityContainer.removeClass('d-none');
                quantityNumber.removeClass('d-none');
                quantityNumber.html("(" + quantidade + " disponíveis)");
                quantityInput.attr('max', quantidade);
                quantityInput.val(1);
            }
            function setUnavaliable() {
                productStatus.html('Sem estoque')
                productStatus.addClass('badge-danger');
                productStatus.removeClass('badge-primary');

                quantityAlert.removeClass('d-none');
                quantityContainer.addClass('d-none');
                quantityNumber.addClass('d-none');
                productId.val(0);
            }
        @endif

        @if($produto->avaliacoes()->count() >= 3)
            $(function(){
                loadReviews();
            })
            let page = 1;
            let last_page;
            let reviews = $("#reviews");
            function loadReviews() {
                if(page > last_page) return;
                $.ajax({
                    url: "{{ route('shop.produtos.avaliacoes', $produto->id) }}",
                    type: "GET",
                    data: {
                        'page': page
                    },
                    success: function(response){
                        let avaliacoes = JSON.parse(response.avaliacoes);
                        let data = avaliacoes.data;
                        last_page = avaliacoes.last_page;
                        data.forEach(function(item){
                            let review = document.createElement('div');
                            review.classList.add('cardborder-0', 'shadow-sm',  'mb-4');
                            review.innerHTML = 
                                `<div class="card-body">
                                    <div class="stars-outer stars-sm mb-2">
                                        <div class="stars-inner" style="width: ${20 * item.valor}%"></div>
                                    </div>
                                    <h5 class="h6">${item.titulo}</h5>
                                    <p class="card-text">${item.mensagem}</p>
                                    <p class="card-text"><small class="text-muted">Postado em ${item.created_at}</small></p>
                                </div>`;
                            reviews.append(review);
                        })
                        page++;
                        if(page > last_page) $("#loadReviews").remove()
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText)
                    },
                })
            }
        @endif
    </script>
@endsection