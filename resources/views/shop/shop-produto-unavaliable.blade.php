@extends('layouts.shop')

@section('title', 'Produto indisponível')

@section('content')
    <main class="container-md px-3 px-md-4">
        <div class="row flex-column vflex-center">
            <div class="col-sm-7 mb-3">
                <img src="{{ asset('img/unavaliable.png') }}" alt="" id="empty-bag" class="img-fluid">
            </div>
            <div class="col-md text-center">
                <h2 class="display-5 mb-0">Produto indisponível</h2>
                <p class="mb-0">Desculpe-nos, o produto que voce busca não está disponível no momento. <br class="d-none d-sm-inline"> Ele pode ter sido deletado ou saido do catálogo temporariamente.</p>
            </div>
        </div>
    </main>
@endsection