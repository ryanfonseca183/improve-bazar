@extends('layouts.shop')

@php
    $shop = session()->get('shop');
    $subcategoria = $shop['subcategoria'];
    $filters = $shop['filters'];
    $priceMin = isset($shop['priceMin']) ? $shop['priceMin'] : 0;
    $priceMax = isset($shop['priceMax']) ? $shop['priceMax'] : 0;
    $priceRangeMin = isset($shop['priceRange']['min']) ? $shop['priceRange']['min'] : $priceMin;
    $priceRangeMax = isset($shop['priceRange']['max']) ? $shop['priceRange']['max'] : $priceMax;
@endphp

@section('title',  (isset($filters['nome']) ? $filters['nome'] : $shop['subcategoria']->nome) ." | Catálogo de produtos")

@section('pl-custom-styles')
    <style>
        #shop-items hr {
            margin: 2rem 0;
        }
        .filter {
            background: white;
            margin-bottom: 0.5rem;
        }
        .filter:last-child {
            margin-bottom: 0;
        }
        .btn-filter {
            padding: 0.75rem 1rem;
            width: 100%;
            display:flex;
            justify-content: space-between;
            align-items:center;
        }
        .filter-groups-container {
           border: 1px solid var(--cinza-medio);
           max-height: 200px;
           overflow: auto;
        }
       .filter-options-group {
            margin-bottom: 1rem;
        }
        .filter-options-list {
           list-style: none;
           padding:0.5rem 1rem;
           margin: 0px;
       }
        .filter-group-title {
            background-color: var(--cinza-medio);
            padding: 0.5rem;
            margin: 0px;
        }
       .filter-option {
           border: none !important;
           padding: 0 !important;
       }
       .filter-option a {
           color: darkgray
       }
       .filter-option a:hover {
           color: var(--laranja);
       }
       .filters-active {
           margin-bottom: 1rem;
       }
       .filter-active {
           background-color:white;
            padding:0.25rem 0.5rem;
            margin-right: 0.5rem;
            font-size: 10pt;
            margin-bottom: 0.5rem;
       }
        .filter-active-link:hover {
            color: var(--laranja);
        }
        .navbar-toggler {
            outline: none !important;
        }
        [for="amount"] {
            color: var(--laranja);
            font-weight: 700;
            margin-bottom: 1rem;
        }
        [name="order_by"] {
            background: transparent;
            padding: 0;
            border: none;
        }
    </style>    
@endsection


@section('content')
    <div class="container-md" id="shop-items">
        <h1 class="h5 mb-0">Catálogo de produtos</h1>
        <hr class="mt-2 bg-light-gray">
        @if(!$produtos->isEmpty())
            <div class="row align-items-start">
                {{-- MENU LATERAL--}}
                <div class="col-lg-3 mb-3">
                    <aside class="bg-light rounded p-3">
                        {{-- CLASSIFICAÇÃO DOS PRODUTOS --}}
                        @isset($subcategoria)
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        {{$subcategoria->categoria->nome}}
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        {{$subcategoria->nome}}
                                    </li>
                                </ol>
                            </nav>
                        @endisset

                        {{-- RESULTADO DA PESQUISA --}}
                        <h3 class="h4 mb-0 mt-5">{{isset($filters['nome']) ? $filters['nome'] : $shop['subcategoria']->nome}}</h3>
                        <p>{{$produtos->total()}} resultados</p>

                        {{-- FILTROS ATIVOS --}}
                        @if(count($filters) != 0)
                        <div class="row no-gutters filters-active">
                            @foreach($filters as $key => $filter)
                                @if($key != "subcategorias_id" &&  $key != "nome" && $key != 'order_by')
                                    <div class="col-auto ">
                                        <div class="filter-active">
                                            <span>
                                                @php 
                                                    if(strpos($filter, 'label_') !== false) {
                                                        $filter = substr($filter, 6);
                                                    }
                                                @endphp
                                                @if($key == "preco")
                                                    R$ {{ $priceRangeMin }} - {{ $priceRangeMax }}
                                                @else
                                                    {{ $filter }}
                                                @endif
                                            </span>
                                            @php 
                                                $array = $filters;
                                                unset($array[$key]);
                                            @endphp
                                            <a href="{{ route('shop.catalogo', $array) }}" class="link-reset filter-active-link">
                                                <i class="fas fa-backspace"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            {{-- REMOVER TODOS OS FILTROS --}}
                            @if(count($filters) > 3)
                                <div class="col-auto">
                                    <div class="filter-active">
                                    <span>Limpar todos</span>
                                        @php 
                                            $array = [];
                                            if(isset($filters['subcategorias_id'])) {
                                                $array['subcategorias_id'] = $filters['subcategorias_id'];
                                            }
                                            if(isset($filters['nome'])) {
                                                $array['nome'] = $filters['nome'];
                                            }
                                        @endphp
                                        <a href="{{ route('shop.catalogo', $array) }}" class="link-reset filter-active-link">
                                            <i class="fas fa-backspace"></i>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @endif

                        {{-- ORDENAMENTO --}}
                        @php $array = $filters; $selected = isset($filters['order_by']) ? $filters['order_by'] : null; @endphp
                        <div class="form-group">
                            <label class="d-block mb-1">Ordenar por</label>
                            @php $array['order_by'] = "asc_nome"; @endphp 
                            <a class="radio-button-text-show  @if($selected == "asc_nome" || $selected == null) active @endif" href="{{ route('shop.catalogo', $array) }}">
                                <i class="fas fa-sort-alpha-down"></i>
                            </a>
                            @php $array['order_by'] = "asc_desconto"; @endphp 
                            <a class="radio-button-text-show @if($selected == "asc_desconto") active @endif" href="{{ route('shop.catalogo', $array) }}">
                                <i class="fas fa-sort-numeric-up-alt"></i>
                            </a>
                            @php $array['order_by'] = "desc_desconto"; @endphp 
                            <a class="radio-button-text-show @if($selected == "desc_desconto") active @endif" href="{{ route('shop.catalogo', $array) }}">
                                <i class="fas fa-sort-numeric-down-alt"></i>
                            </a>
                        </div>

                        {{-- FILTROS DE PESQUISA --}}
                        <nav class="navbar navbar-expand-lg navbar-light p-0 mb-lg-5">
                            <span class="h5 d-lg-none mb-3">Filtros</span>

                            <button class="navbar-toggler mb-3" type="button" data-toggle="collapse" data-target="#filters">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse mb-3" id="filters">
                                <ul class="navbar-nav flex-column w-100">
                                    {{-- CLASSIFICAÇÃO DO PRODUTO --}}
                                    @if(!isset($shop['subcategoria']))
                                        <li class="nav-item filter">
                                            <button class="btn btn-filter" type="button" data-toggle="collapse" data-target="#categoria">
                                                <span>Categorias</span>
                                                <i class="fas fa-chevron-down text-orange"></i>
                                            </button>
                                            <div class="collapse p-3" id="categoria" data-parent="#filters">
                                                <div class="form-group">
                                                    <label for="">Pesquisar</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control search-input" data-list="list_categorias">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text bg-transparent">
                                                                <i class="fas fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="filter-groups-container"  id="list_categorias">
                                                    @php $categorias = App\Categoria::withCount('subcategorias')->get(); @endphp
                                                    @foreach($categorias as $cat)
                                                        @if($cat->subcategorias_count != 0)
                                                            <div class="filter-options-group">
                                                                <h3 class="h6 filter-group-title">
                                                                    {{ $cat->nome }}
                                                                </h3>
                                                                <ul class="filter-options-list">
                                                                    @foreach($cat->subcategorias as $subcat)
                                                                        <li class="filter-option">
                                                                            @if(isset($filters['nome']))
                                                                                <a href="{{ route('shop.catalogo', ['subcategorias_id' => $subcat->id, 'nome' => $filters['nome']]) }}">
                                                                                    {{$subcat->nome}}
                                                                                </a>
                                                                            @else 
                                                                                <a href="{{ route('shop.catalogo', ['subcategorias_id' => $subcat->id]) }}">
                                                                                    {{$subcat->nome}}
                                                                                </a>
                                                                            @endif
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>
                                    @endif

                                    {{-- PREÇO DO PRODUTO --}}
                                    <li class="nav-item filter">
                                        <button class="btn btn-filter" type="button" data-toggle="collapse" data-target="#price">
                                            <span>Preço</span>
                                            <i class="fas fa-chevron-down text-orange"></i>
                                        </button>
                                        <div class="collapse pb-3" id="price" data-parent="#filters">
                                            <form id="price-form" method="GET" action="{{ route('shop.catalogo') }}">
                                                @foreach($filters as $key =>  $filter)
                                                    @if($key != 'preco')
                                                        <input type="hidden" class="d-none" name="{{ $key }}" value="{{ $filter }}">
                                                    @endif
                                                @endforeach
                                                <div class="form-group px-2">
                                                    <label for="amount"></label>
                                                    <input type="hidden" name="preco" id="amount">
                                                    <div id="slider-range"></div>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    {{-- FILTROS ESPECÍFICOS --}}
                                    @isset($shop['subcategoria'])
                                        @if($subcategoria->atributos()->exists())
                                            @foreach($subcategoria->atributos as $atr)
                                                <x-search-filter :atr="$atr" :filters="$filters"></x-search-filter>
                                            @endforeach
                                        @endif
                                    @endisset
                                </ul>
                            </div>
                        </nav>
                    </aside>
                </div>
                {{-- CONTAINER PARA LISTAGEM DOS PRODUTOS --}}
                <div class="col">
                    <x-products-shop-section :produtos="$produtos" action="fav"></x-products-shop-section>
                </div>
            </div>
        @else 
            <div class="row shadow-sm p-3 pb-5 no-gutters justify-content-center">
                <div class="col-12">
                    <a href="{{ count($filters) > 1 ? url()->previous() : route("shop.catalogo") }}">
                        <i class="fas fa-long-arrow-alt-left mr-2"></i>
                        Voltar
                    </a>
                </div>
                <div class="col-md-8 col-lg-6">
                    <img src="{{ asset('img/notfound-shop.jpg') }}" class="img-fluid">
                    <h1 class="h2 text-orange">Desculpe!</h1>
                    <p>Nenhum produto foi encontrado. Tente buscar por um nome diferente ou aplicar outros filtros de pesquisa!</p>
                </div>
            </div>
        @endif
    </div>
@endsection 


@section('pl-custom-scripts')
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
    $('.search-input').on('input', function() {
        searchData(this, this.getAttribute('data-list'))
    });
    function searchData(input, list) {
        // Declare variables
        var filter, a, txtValue, subLists;
        filter = input.value.toUpperCase();
        ul = document.getElementById(list);
        li  = ul.getElementsByTagName('li');
        // Loop through all list items, and hide those who don't match the search query
        for (var i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            } else {
            li[i].style.display = "none";
            }
        }
    }
    $(".logic-filter").on('change', function(){
        if(!$(this).prop('checked')) {
            window.location.href = removeURLParameter(window.location.href, $(this).prop('name'));
        } else {
            window.location.href += `&${$(this).prop('name')}=${$(this).val()}`;
        }
    });
    $( "#slider-range" ).slider({
        animate: 'fast',
        range: true,
        disabled: @if($priceMin == $priceMax) true @else false @endisset,
        min: {{$priceMin}},
        max: {{$priceMax}},
        values: [{{ $priceRangeMin }}, {{ $priceRangeMax }} ],
        change: function( event, ui ) {
            $("#price-form").submit();
        },
        slide: function( event, ui ) {
            $( "#amount" ).val(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
            $( "[for='amount']" ).html("R$ " + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        },
    });
    $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 )+ "-" + $( "#slider-range" ).slider( "values", 1 ) );
    $( "[for='amount']" ).html("R$ " + $( "#slider-range" ).slider( "values", 0 )+ " - " + $( "#slider-range" ).slider( "values", 1 ) );
</script>

@endsection
