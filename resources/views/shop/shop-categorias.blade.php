@extends('layouts.shop')

@section('title', 'Categorias')

@section('pl-custom-styles')
    <style>
       .category-options {
           margin-left: 2.5rem;
       }
       .categories-group {
           margin-bottom: 2rem;
       }
       .categories-group-title {
           color: var(--laranja);
           font-size: 2.5rem;
       }
       .categories-group-title, .category-link {
            text-transform: uppercase;
       }
       .category-options-title {
            font-size: 1.25rem;
       }
    </style>
@endsection

@section('content')
    <main class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card custom-card">
                    <h1 class="h4 mb-0">Categorias de produtos</h1>
                    <p>Navegue pelas diversas categoria do nosso catálogo de produtos</p>
                    {{-- LINKS DE ATALHOS --}}
                    <div class="form-row align-items-center mb-5">
                        <div class="col-auto text-dark-yellow">
                            <h2 class="h5 mb-0">Atalhos
                                <i class="fas fa-angle-right ml-2"></i>
                            </h2>
                        </div>
                        <div class="col">
                            <ul class="nav mb-0">
                                @foreach($groups as $key => $group)
                                    <li class="nav-item">
                                        <a class="nav-link category-link" href="#group-{{ $key }}">{{ $key }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    {{-- CATEGORIAS E SUBCATEGORIAS --}}
                    <div class="categories-container">
                        @foreach($groups as $key => $group) 
                            <div class="categories-group" id="group-{{$key}}">
                                <h2 class="categories-group-title">{{ $key }}</h2>
                                <hr>
                                @foreach($group as $category)
                                    <div class="category-options">
                                        <h3 class="category-options-title">{{$category->nome}}</h3>
                                        <ul class="list-unstyled">
                                            @foreach($category->subcategorias as $subcat)
                                            <li>
                                                <a href="{{ route('shop.catalogo', ['subcategorias_id' => $subcat->id]) }}">{{$subcat->nome}}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection