@extends('layouts.shop')

@section('title', 'Sacola de compras')

@section('content')
    <main class="container-md px-3 px-md-4">
        <h1 class="h5">Meu carrinho</h1>
        <hr class="mb-5">
        <div class="row hflex-center">
            <div class="col-sm-8 col-md-5">
            <img src="{{ asset('img/empty-bag.png') }}" alt="" id="empty-bag" class="img-fluid mb-4 mb-md-0">
            </div>
            <div class="col-md text-center text-lg-left">
                <h2 class="display-5 mb-0">Sacola de compras vazia</h2>
                <p class="mb-0">Navegue pelo catálogo para conhecer as nossas ofertas.<br class="d-none d-sm-inline"> Basta pesquisar pelo nome do produto ou selecionar alguma categoria.</p>
            </div>
        </div>
    </main>
@endsection