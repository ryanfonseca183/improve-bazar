@extends('layouts.shop')

@section('title', 'Sacola de compras')

@section('pl-custom-styles')
    <style>
        .input-quantity-container {
            display:inline-flex;
            align-items:center;
        }
        .input-quantity {
            padding: 0.5rem;
            max-width: 50px;
            text-align: center;
            background: transparent;
            outline: none !important;
            margin: 0 0.5rem;
            border: 1px solid darkgray; 
            border-radius: 0.25rem;
        }
        .btn-plus:disabled, .btn-minus:disabled {
            cursor: context-menu;
        }
        .purchase-info {
            position: absolute;
            top: 0;
            left: 0;
            display:flex;
            align-items:center;
            justify-content:center;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.1);
            z-index: 10;
            border-radius: 0.25rem;
            opacity: 0;
            z-index:-1;
            transition: all 0.3s ease-out;
        }
        .purchase-info.active {
            opacity: 1;
            z-index: 1;
        }

        .address-container {
            max-height: 300px;
            overflow-y: auto;
            margin-bottom: 1.5rem;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }
        .address-icon {
            color: var(--laranja);
            font-size: 1.5rem;
            margin: 0 0.5rem;
        }
        .custom-checkbox-control {
            display:flex !important;
            padding:0 !important;
            overflow:hidden ;
        }
        .custom-checkbox-input, .custom-checkbox-label-text {
            align-self:center;
        }
        .custom-checkbox-input {
            order: 13;
            margin-right: 1rem;
        }
        .custom-checkbox-label {
            flex:1;
            margin: 0;
            cursor:pointer;
        }
        .custom-checkbox-label-text {
            margin-left: 1rem;
            padding: 0.5rem 0;
        }
        .custom-checkbox-label-icon {
            color:var(--laranja);
            transition: all 0.3s ease-out;
        }
        .custom-checkbox-icon {
            font-size: 20pt;
            width: 40px;
            height: 100%;
            display:flex;
            justify-content:center;
            align-items: center;
        }
        .custom-checkbox-input:checked + label .custom-checkbox-label-icon {
            background-color: var(--laranja);
            color:white;
        }
        @media only screen and (min-width: 576px) {
            .custom-checkbox-icon {
                width: 60px;
            }
        }
        @media only screen and (min-width: 768px) {
            .custom-checkbox-label-content {
                min-height:100px;
            }
        }
        .unavaliable {
            opacity: 0.5;
        }
    </style>
@endsection
@section('content')
    <div class="container-md px-md-4">
        <div class="row" id="content">
            <div class="col-lg-8">
                <main>
                    {{-- ENDEREÇO DE ENTREGA --}}
                    <h2 class="h5 @empty($endereco) mb-0 @endif">Endereço de entrega</h2>
                    @empty($endereco) <p class="mb-2">Voce verá o valor do frete antes de finalizar a compra</p> @endif
                    <div class="card card-body mb-4">
                        @isset($endereco)
                            <div class="form-row align-items-center">
                                <div class="col-auto">
                                    <i class="fas fa-map-marker-alt address-icon"></i>
                                </div>
                                <div class="col">
                                    <address class="mb-0" id="bag-address">
                                        {{$endereco->rua}}, {{$endereco->numero}}<br>
                                        {{$endereco->cidade}} - {{$endereco->uf_sigla}}, {{$endereco->cep}}
                                    </address>
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-action" data-toggle="modal" data-target="#addressModal">
                                        <i class="fas fa-chevron-down " ></i>
                                    </button>
                                </div>
                            </div>
                        @else 
                            <form action="{{ route('address.create') }}" method="GET" novalidate id="store_address">
                                <div class="form-group row">
                                    <label for="cep" class="col-lg-auto col-form-label">CEP</label>
                                    <div class="col-lg-5 mb-3 mb-lg-0">
                                        <input type="hidden" name="redirect" value="user.bag">
                                        <input type="text" class="form-control" required maxlength="9" id="cep" name="cep">
                                        <span class="invalid-feedback position-lg-absolute">
                                            <strong>Por favor, informe um CEP válido</strong>
                                        </span>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button" onclick="validateCEP()" class="btn btn-primary">Buscar</button>
                                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="ml-2" target="_blank">Não sei meu CEP</a>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>  
    
                    {{-- PRODUTOS NA SACOLA --}}
                    <h2 class="h5">Minha sacola</h2>
                    @foreach($produtos as $produto)
                        <div class="card card-body mb-4 @if(!$produto->is_avaliable) unavaliable @endif" data-product-id="{{ $produto->id }}">
                            <div class="row">
                                <div class="col-sm-auto">
                                    <div class="listable-product-image img-md">
                                        @php 
                                            if($produto->produtos_id != null)
                                                $image = $produto->parente->fotosProduto()->where('is_main', 1)->first();
                                            else 
                                                $image = $produto->fotosProduto()->where('is_main', 1)->first();
                                        @endphp
                                        <img class="img-fluid" src="{{ asset('storage/' . $image->caminho) }}" alt="">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-row align-items-center mb-2 mb-sm-0">
                                        {{-- NOME DO PRODUTO --}}
                                        <div class="col">
                                        <h3 class="h6 mb-0"><a href="{{ route('shop.produto', $produto->id) }}">{{$produto->nome}}</a></h3>
                                        </div>
    
                                        {{-- BOTÃO DE DELETE --}}
                                        <div class="col-auto">
                                            <form action="{{ route('bag.produto.delete', $produto->id) }}" method="post">
                                                @csrf 
                                                @method('DELETE')
                                                <button class="btn btn-action" type="submit" title="Tirar produto do carrinho">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    {{-- SKU --}}
                                    <div class="mb-2">
                                        <span class="small">SKU: {{ $produto->sku }}</span>
                                        @if($produto->is_avaliable) 
                                            <span class="small ml-2">{{ $produto->quantidade }} unidades disponíveis</span>
                                        @else 
                                            <span class="badge badge-danger ml-2">Indisponível</span>
                                        @endif
                                    </div>
    
                                    {{-- ATRIBUTOS CONFIGURÁVEIS --}}
                                    @php $caracteristicas = $produto->atributos()->where('is_configurable', 1)->get(); @endphp
                                    @if($caracteristicas->count() != 0)
                                        <table class="table table-transparent">
                                            <tbody>
                                                @foreach($caracteristicas as $atr)
                                                    <tr>
                                                        <td>
                                                            <span class="small">{{ $atr->nome }}</span>
                                                        </td>
                                                        @if($atr->tipoDeDados->frontend_type_label == "Cor")
                                                            <td class="small ml-2">{{ $atr->pivot->label }}</td>
                                                        @else 
                                                            <td class="small ml-2">{{ $atr->pivot->valor }}</td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                            @if($produto->is_avaliable)
                                <hr>
                                <div class="row no-gutters flex-between">
                                    {{-- QUANTIDADE DO PRODUTO --}}
                                    <div class="col-auto">
                                        <div class="input-quantity-container">
                                            <button type="button" class="btn btn-minus shadow-none p-0">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                            
                                            <input type="text" class="input-quantity" max="{{ $produto->quantidade }}" value="{{ $produto->pivot->quantidade }}" data-id="{{ $produto->id }}">
                                            <button tupe="button" class="btn btn-plus shadow-none p-0">
                                                <i class="fas fa-plus text-primary"></i>
                                            </button>
                                        </div>
                                    </div>
                                    {{-- PREÇO DO PRODUTO --}}
                                    <div class="col-auto">
                                        @php 
                                            $preco = $produto->precoTotal;
                                            $desconto = $produto->precoComDesconto;
                                        @endphp
                                        @if($preco != $desconto)
                                            <span class="old-price">{{ number_format($produto->precoTotal, 2, ',', '.') }}</span>
                                        @endif
                                        <strong class="bag-current-price">R$ {{ number_format($produto->precoComDesconto, 2, ',', '.')}}</strong>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </main>
            </div>
            <div class="col">   
                {{-- RESUMO DA COMPRA --}}
                <aside>
                    <h2 class="h5">Resumo da compra</h2>
                    <div class="bg-light rounded p-3 position-relative">
                        <div class="purchase-info">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <x-order-summary :summary="$summary"></x-order-summary>
                        @if(!$produtos->where('is_avaliable', 1)->isEmpty())
                            <a href="{{ route('shop.checkout') }}" class="btn btn-primary btn-lg btn-block ">Continuar</a>
                            <a href="{{ route('shop.home') }}" class="btn btn-outline-secondary btn-lg btn-block mb-4">Voltar a loja</a>
                        @else 
                            <a href="{{ route('shop.home') }}" class="btn btn-primary btn-lg btn-block mb-4">Voltar a loja</a>
                        @endif
                    </div>
                </aside>
            </div>
            {{-- MODAL ENDEREÇOS --}}
            @isset($endereco)
                <div class="modal fade" id="addressModal" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5 class="modal-title d-inline-block mb-1" id="addressModalLabel">Selecione um endereço de entrega</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <p class="mb-4">Voce poderá ver o valor do frete antes de finalizar a compra</p>
                                <div class="flex-between mb-2">
                                    <h6 class="mb-0">Endereços cadastrados</h6>
                                    <a href="{{ route('user.profile') }}">Gerenciar</a>
                                </div>
                                <div class="address-container">
                                    <ul class="list-group list-group-flush">
                                        @foreach(Auth::user()->enderecos as $key => $end)
                                        <li class="list-group-item custom-checkbox-control">
                                            <input type="radio" @if($end->is_main) checked @endif class="custom-checkbox-input" id="address_{{$key}}" name="address" value="{{ $end->id }}">
                                            <label for="address_{{$key}}" class="custom-checkbox-label">
                                                <div class="row no-gutters custom-checkbox-label-content">
                                                    <!--ICONE-->
                                                    <div class="col-auto custom-checkbox-label-icon">
                                                        <div class="custom-checkbox-icon">
                                                            <i class="fas fa-map-marker-alt"></i>
                                                        </div>
                                                    </div>
                                                    <!--TEXTO-->
                                                    <div class="col custom-checkbox-label-text">
                                                        <address class="m-1">
                                                            {{$end->rua}}, {{$end->numero}}<br>
                                                            {{$end->cidade}} {{$end->uf_sigla}}, {{ $end->cep }}<br>
                                                        </address>
                                                        <a href="{{ route("address.edit", ['address' => $end->id, 'redirect' => 'user.bag']) }}">Editar</a>
                                                    </div>
                                                </div>
                                            </label>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            
                                <div class="card card-body mb-4">
                                    <h6 class="mb-3">Outro endereço</h6>
                                    <form action="{{ route('address.create') }}" method="GET" novalidate id="store_address">
                                        <div class="form-group row">
                                            <label for="cep" class="col-lg-auto col-form-label">CEP</label>
                                            <div class="col-lg-5 mb-3 mb-lg-0">
                                                <input type="hidden" name="redirect" value="user.bag">
                                                <input type="text" class="form-control" required maxlength="9" id="cep" name="cep">
                                                <span class="invalid-feedback position-lg-absolute">
                                                    <strong>Por favor, informe um CEP válido</strong>
                                                </span>
                                            </div>
                                            <div class="col-auto">
                                                <button type="button" onclick="validateCEP()" class="btn btn-primary">Buscar</button>
                                                <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="ml-2" target="_blank">Não sei meu CEP</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endisset 
        </div>
    </div>
@endsection
@php $sedex = $summary['frete']['sedex']; @endphp
@section('pl-custom-scripts')
    <script type="text/javascript" src="{{ asset('js/dependencies/mask.js') }}"></script>
    <script>
        let days = date = null;
        @isset($sedex['date'])
            date = "{{ $sedex['date'] }}";
            days = "{{ $sedex['deadline'] }}";
        @endisset
        $(function(){
            //Incrementa uma quantidade
            $(".btn-plus").click(function(){
                let input = $(this).prev();
                input.val(function(i, val) {return ++val});
                updateBagQuantity(input, 1);
            })
            $(".input-quantity").each(function(){
                toggleButtons($(this));
            });
            //Drecrementa uma quantidade
            $(".btn-minus").click(function(){
                let input = $(this).next();
                input.val(function(i, val) {return --val});
                updateBagQuantity(input, 2);
            })
            //Define uma máscara para os campos
            $(".input-quantity").mask('#00000');

            //Define um evento de input, para verificar o valor máximo
            $(".input-quantity").on('input', function(){
                let val = Number($(this).val());
                let max = Number($(this).attr('max'));
                if(val > max) 
                    $(this).val(max) 
                else if(val < 1) 
                    $(this).val(1);
            })

            //Define um evento de change, para desabilitar os botões de incremento/decremento
            $(".input-quantity").change(function(){
                updateBagQuantity($(this), 3)
            })
            $("#cep").mask('00000-000');

            $("[name='address']").change(function(){
                updateBagAddress($(this).val());
            })
            if(date && days) getShippingDeadline(date, days);
        })
        function toggleButtons(input) {
            let val = Number(input.val());
            let max = Number(input.attr('max'));
            if(val == 1) 
                input.prev().prop('disabled', true)
            else 
                input.prev().prop('disabled', false);

            if(val >= max) 
                input.next().prop('disabled', true)
            else 
                input.next().prop('disabled', false);
        }
        function updateBagQuantity(input, action) {
            let quantidade;
            let id = input.attr('data-id');
            switch(action) {
                case 1: quantidade = 1
                break;
                case 2: quantidade = -1
                break;
                case 3: quantidade = input.val();
            }
            $.ajax({
                url: '/user/bag/produto/' + id + '/quantity',
                method: "PUT",
                data: {
                    'quantidade': quantidade,
                    'action': action,
                    '_token': '{{ csrf_token() }}',
                },
                beforeSend:function() {
                    input.siblings().prop('disabled', true);
                    $(".purchase-info").addClass("active");
                },
                success: function(response) {
                    loadSummary(response.summary);
                    //Atualizando os preços do produto
                    $("[data-product-id='"+response.produto.id+"'] .old-price").html(format(response.produto.precoTotal));
                    $("[data-product-id='"+response.produto.id+"'] .bag-current-price").html("R$ " + format(response.produto.precoComDesconto));
                },
                error:function(xhr) {
                    console.log(xhr.responseText)
                },
                complete: function() {
                    input.siblings().prop('disabled', false);
                    toggleButtons(input)
                    $(".purchase-info").removeClass("active");
                },
            })
        }
        function updateBagAddress(end_id) {
            $.ajax({
                url: '/user/bag/address/' + end_id + '',
                method: "PUT",
                data: {
                    '_token': '{{ csrf_token() }}',
                },
                beforeSend: function() {
                    $("[name='address']").prop('disabled', true);
                    $("#addressModal").modal('hide');
                    $(".purchase-info").addClass("active");
                },
                success:function(response) {
                    //Atualizando o endereço de entrega
                    let address = response.address;
                    let summary = response.summary;
                    $("#bag-address").html(`${address.rua},  ${address.numero} <br/> ${address.cidade} - ${address.uf_sigla}, ${address.cep}`);
                    
                    //Atualizando o resumo da compra
                    loadSummary(response.summary);
                    getShippingDeadline(summary.frete.sedex.date, summary.frete.sedex.deadline);
                },
                error:function(xhr) {
                    console.log(xhr)
                },
                complete: function() {
                    $(".purchase-info").removeClass("active");
                    $("[name='address']").prop('disabled', false);
                }
            })
        }
        function getShippingDeadline(date, days, calendar = "br-BC") {
            var url = "https://elekto.com.br/api/Calendars/" + calendar + "/Add?initialDate=" + date + "&days=" + days + "&type=work&finalAdjust=none";
            $.getJSON(url, function(r){
                    let date = new Date(r.substr(0, 10));
                    let dia = date.getUTCDate();
                    let mes = date.getUTCMonth() + 1;
                    let mesNome;
                    switch(mes) {
                        case 1: mesNome= "Janeiro"; break;
                        case 2: mesNome= "Fevereiro"; break;
                        case 3: mesNome= "Março"; break;
                        case 4: mesNome= "Abril"; break;
                        case 5: mesNome= "Maio"; break;
                        case 6: mesNome= "Junho"; break;
                        case 7: mesNome= "Julho"; break;
                        case 8: mesNome= "Agosto"; break;
                        case 9: mesNome= "Setembro"; break;
                        case 10: mesNome= "Outubro"; break;
                        case 11: mesNome= "Novembro"; break;
                        case 12: mesNome= "Dezembro"; break;
                    }
                    $("#shipping-message").html("Chegará até o dia " + dia + " de " + mesNome);
            })
            .fail(function(jqXhr, textStatus, errorThrown) {
                var err = textStatus + ', ' + errorThrown;
                console.log( "Request Failed: " + err);
            });
        }
        function loadSummary(summary) {
            let shippingPrice = summary.frete.sedex.price != null ? summary.frete.sedex.price : "--";
            let diferenca = summary.subTotalSemDesconto - summary.subTotal;
            $("#shipping-price").html('R$ ' + format(shippingPrice));
            $("#subtotal").html('R$ ' + format(summary.subTotal));
            $("#total").html('R$ ' + format(summary.total));
            if(diferenca > 0) {
                $("#diferenca").parent().show();
                $("#diferenca").html(format(diferenca));
            } else {
                $("#diferenca").parent().hide();
            }
        }
    </script>
@endsection