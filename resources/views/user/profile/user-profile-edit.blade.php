@extends('layouts.compressed-header')

@section('title', 'Cadastrar endereço')

@section('content')
    <main class="py-5">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                <form action="{{ route('user.profile.update') }}" method="POST" class="p-4 p-sm-5 rounded shadow-sm needs-validation" id="profile_form" novalidate>
                    @csrf
                    @method('PUT')
                    <h1 class="h4 mb-4">Atualizar perfil</h1>
                    <div class="form-row" id="required-fields">
                        <div class="form-group col-sm-6">
                            <label for="name">Nome completo</label>
                            <input type="text" required value="{{ Auth::user()->name  }}" class="form-control mb-2 @error('name') is-invalid @enderror" id="name" name="name">
                            <span class="invalid-feedback" id="info_name">
                                <strong>
                                    @error('name')
                                        {{ $message }}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-sm">
                            <label for="cpf">CPF</label>
                            <input type="text" required value="{{ Auth::user()->cpf }}" class="form-control @error('cpf') is-invalid @enderror" id="cpf" name="cpf" >
                            <span class="invalid-feedback" id="info_cpf">
                                <strong>
                                    @error('cpf')
                                        {{ $message }}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-sm">
                            <label for="celular">Celular</label>
                            <input type="text" value="{{  Auth::user()->celular  }}" class="form-control @error('celular') is-invalid @enderror" id="celular" name="celular" >
                            <span class="invalid-feedback" id="info_celular">
                                <strong>
                                    @error('celular')
                                        {{ $message }}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bairro">Nascimento</label>
                            <input type="date" value="{{ Auth::user()->nascimento }}" class="form-control @error('bairro') is-invalid @enderror" id="nascimento" name="nascimento" >
                            <span class="invalid-feedback" id="info_nascimento">
                                <strong>
                                    @error('nascimento')
                                        {{$message}}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-sm-6 mt-4">
                            <div class="form-row">
                                <div class="col">
                                    <a href=" {{ route('user.profile') }} " class="btn btn-outline-secondary btn-block">Cancelar</a>
                                </div>
                                <div class="col">
                                    <button type="button" onclick="checkFormValidity('profile_form')" class="btn btn-success btn-block">Atualizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection

@section('pl-custom-scripts')
    <script type="text/javascript" src="{{ asset('js/dependencies/mask.js') }}"></script>
    <script>
        $("#cpf").mask('000.000.000-00');
        $("#celular").mask('(00) 00000-0000');
    </script>
@endsection