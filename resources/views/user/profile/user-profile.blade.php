@extends('layouts.profile')

@section('pl-custom-styles')
    @parent
    <style>
        #content {
            align-items:stretch;
            background-color: var(--cinza-medio);
            border-radius: 0.35rem;
            box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
        }
        #myTabContainer {
            border-top: none;
        }
        #myTabContainer .card-header {
            background-color: var(--cinza-medio);
        }
        #myTab .nav-link {
            padding: 0.5rem;
        }
        #myTabContent .tab-pane {
            padding:3rem 1.5rem;
        }
        /*TAB ENDEREÇO */
        .address-not-found, .address-container {
            margin: 1.5rem 0;
        }
        .address {
            background:white;
            box-shadow: 0 0.125rem 0.25rem 0 rgba(58, 59, 69, 0.2) !important;
            margin-bottom: 1.5rem;
            border-radius: 0.35rem !important;
            position: relative
        }
        .address-icon {
            display:flex;
            justify-content: center;
            align-items: center;
            color: var(--laranja);
            background:  var(--cinza-medio);
            position: absolute;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            top: 0;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 1.25rem;
        }
        .main-address {
            background: var(--laranja);
            color: #f8f9fc;
        }
        
        .address address {
            padding-bottom: 1rem;
            padding-top: 1.5rem;
            margin-left: 1rem;
        }
        .address-not-found {
            display:flex;
            flex-direction: column;
            align-items: center;
            text-align:left;
        }.address-not-found img {
            margin-bottom: 0.5rem;
        }
        .address-not-found h3 {
            margin-bottom: 0.25rem !important;
            font-size: 1rem;
        }
        .address-not-found p {
            font-size: 80%;
            font-weight: 400;
            margin-bottom: 0;
        }
        @media only screen and (min-width: 578px) {
            .address-not-found {
                text-align: center !important;
            }
            .address {
                margin-bottom: 1rem;
            }
            .address address {
                padding-top: 1rem;
            }
            .address-icon {
                padding: 0 1rem !important;
                position:unset;
                transform:unset;
                width: unset;
                height: unset;
                border-radius: unset;
                font-size: 2rem;
                border-top-left-radius: 0.35rem;
                border-bottom-left-radius: 0.35rem;
            }
        }
        @media only screen and (min-width: 768px) {
            #myTab .nav-link {
                padding: 0.5rem 1rem;
            }
            #myTabContainer {
                border-top: 1px solid rgb(227, 230, 240);
            }
            #myTabContainer .card-header {
                background-color: #f8f9fc;
            }
            #myTabContent .tab-pane {
                padding: 3rem;
            }
        }
    </style>
@endsection

@section('title', 'Meu perfil')
@php $address_active = session('address_active') ?? session('address_active'); @endphp 
@section('profile-content')
    <div class="col card" id="myTabContainer">
        <div class="card-header tab-links">
            <ul class="nav nav-tabs card-header-tabs" id="myTab">
                <li class="nav-item" role="presentation">
                    <a class="nav-link @if(!$address_active) active @endif" id="home-tab" data-toggle="tab" href="#info">Sobre</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link @if($address_active) active @endif" id="contact-tab" data-toggle="tab" href="#address">Endereços</a>
                    </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#auth">Segurança</a>
                </li>
            </ul>
        </div>
        <div class="card-body p-0">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade @if(!$address_active) show active @endif" id="info">
                    <div class="row flex-between mb-4">
                        <div class="col-sm-auto mb-3 mb-sm-0">
                            <h2 class="h5 mb-0">Meus dados</h2>
                            <span class="small">Ultima atualização: {{ Auth::user()->updated_at }}</span>
                        </div>
                        <div class="col-sm-auto">
                            <a href="{{ route('user.profile.edit') }}">Editar
                                <i class="fas fa-long-arrow-alt-right ml-2"></i>
                            </a>
                        </div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label for="nome" class="col-lg-2 col-form-label">Nome</label>
                                <div class="col">
                                    <span>{{ Auth::user()->name }}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label class="col-lg-2 col-form-label">CPF</label>
                                <div class="col-lg">
                                    <span>{{ Auth::user()->cpf }}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label class="col-lg-2 col-form-label">Celular</label>
                                <div class="col-lg">
                                    <span>{{ Auth::user()->celular }}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label class="col-lg-2 col-form-label">Nascimento</label>
                                <div class="col-lg">
                                    <span>{{ Auth::user()->nascimento }}</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade @isset($address_active) show active @endif" id="address" role="tabpanel" aria-labelledby="profile-tab">
                    <h2 class="h5 mb-0">Endereços de entrega</h2>
                    {{-- CONTAINER DE ENDEREÇOS DE ENTREGA --}}
                    @if(!Auth::user()->enderecos->isEmpty())
                        <div class="address-container">
                            @foreach(Auth::user()->enderecos as $end)
                                <div class="row no-gutters address">
                                    {{-- ICONE ENDEREÇO --}}
                                    <div class="col-auto address-icon @if($end->is_main) main-address @endif">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </div>
                                    <div class="col">
                                        <div class="row no-gutters align-items-center">
                                            {{-- ENDEREÇOS DE ENTRGA --}}
                                            <div class="col">
                                                <address class="mb-0">
                                                    {{$end->rua}}, {{$end->numero}}.<br> 
                                                    {{$end->cidade}}, {{$end->uf_sigla}} - {{$end->cep}}<br>
                                                </address>
                                            </div>
                                            <div class="col-auto">
                                                <div class="btn-group px-3 dropleft">
                                                    {{-- TOGGLE MENU DE AÇÕES --}}
                                                    <button type="button" class="btn-reset no-arrow dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    {{-- MENU DE AÇÕES --}}
                                                    <div class="dropdown-menu">
                                                        {{-- DELETAR ENDEREÇO --}}
                                                        <form action="{{ route('address.destroy', $end->id) }}" method="POST">
                                                            @csrf 
                                                            @method('DELETE')
                                                            <button type="submit" class="dropdown-item">Deletar</button>
                                                        </form>

                                                        {{-- EDITAR ENDEREÇO --}}
                                                        <a href="{{ route('address.edit', $end->id) }}" class="dropdown-item">Editar</a>

                                                        {{-- DEFINIR COMO PRINCIPAL --}}
                                                        @if(!$end->is_main)
                                                            <form action="{{ route('address.setMain', $end->id) }}" method="POST">
                                                                @csrf 
                                                                @method('PUT')
                                                                <button type="submit" class="dropdown-item">Definir como padrão</button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <hr>
                        <div class="address-not-found">
                            <div class="row no-gutters justify-content-center">
                                <div class="col-sm-4">
                                    <img src="{{ asset('img/address.jpg')}}" class="img-fluid" alt="Endereços de entrega não encontrados">
                                </div>
                                <div class="w-100"></div>
                                <div class="col-sm-8 col-lg-6">
                                    <h3>Nenhum endereço cadastrado</h3>
                                    <p>Por favor, cadastre ao menos uma localização para que a gente possa carregar essas informações durante a compra.</p>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('address.create') }}" class="btn btn-primary">Novo endereço</a>
                    </div>
                </div>
                <div class="tab-pane fade " id="auth">
                    <h2 class="h5 mb-4">Dados de autenticação</h2>
                    <ul class="list-group mb-4">
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label for="email" class="col-lg-2 col-form-label">Email</label>
                                <div class="col">
                                    <div class="row no-gutters flex-nowrap">
                                        <div class="col">
                                            <span>{{ Auth::user()->email }}</span>
                                            <span class="ml-2">
                                                @if(Auth::user()->email_verified_at != null)
                                                    <i class="fas fa-check-circle text-success"  data-toggle="tooltip" data-placement="top" title="Email verificado em: {{ Auth::user()->email_verified_at }}" style="font-size:14pt;"></i>
                                                @else
                                                    <i class="fas fa-exclamation-circle text-warning" data-toggle="tooltip" data-placement="top" title="Email não verificado" style="font-size:14pt;"></i>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-long-arrow-alt-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <label class="col-lg-2 col-form-label">Senha</label>
                                <div class="col-lg">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <i class="fas fa-ellipsis-h"></i>
                                        </div>
                                        <div class="col-auto">
                                            <a href="{{ route('password.edit') }}">
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection