@extends('layouts.profile')

@section('title', 'Compras')

@php if($produto->parente) $prod = $produto->parente; else $prod = $produto; @endphp
@section('profile-content')
    <div class="col px-md-4">
        <main >
            <div class="profile-page-title">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent p-0">
                        <li class="breadcrumb-item"><a href="{{ route('user.orders') }}">Compras</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user.orders.show', $pedido->id) }}">Compra n° {{ $pedido->id }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span class="small">{{ $produto->nome }}</span></li>
                    </ol>
                </nav>
            </div>
            <div class="card custom-card">
                <div class="form-row">
                    <div class="col-sm-auto mb-4">
                        <div class="listable-product-image img-md">
                            {{-- IMAGEM DO PRODUTO --}}
                            @php $image = $prod->fotosProduto()->where('is_main', 1)->first(); @endphp
                            <img src="{{ asset('storage/' . $image->caminho) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col">
                        <form method="POST" action="{{ route('orders.produto.avaliacao.store', [$pedido->id, $produto->id]) }}" id="form_aval" novalidate>
                            @csrf
                            <h1 class="h4 mb-4">{{ $produto->nome }}</h1>
                            {{-- N° ESTRELAS --}}
                            <div class="form-group">
                                <span>Avaliação</span>
                                <div class="star-rating-controls">
                                    <div class="star-rating-group">
                                        <input type="radio" class="star-rating-control" required name="valor" value="1" id="one-star">
                                        <label for="one-star" class="star-rating-label">
                                            <i class="fas fa-star"></i>
                                        </label>
                                    </div>
                                    <div class="star-rating-group">
                                        <input type="radio" class="star-rating-control" name="valor" value="2" id="two-star">
                                        <label for="two-star" class="star-rating-label">
                                            <i class="fas fa-star"></i>
                                        </label>
                                    </div>
                                    <div class="star-rating-group">
                                        <input type="radio" class="star-rating-control" name="valor" value="3" id="tree-star">
                                        <label for="tree-star" class="star-rating-label">
                                            <i class="fas fa-star"></i>
                                        </label>
                                    </div>
                                    <div class="star-rating-group">
                                        <input type="radio" class="star-rating-control" name="valor" value="4" id="four-star">
                                        <label for="four-star" class="star-rating-label">
                                            <i class="fas fa-star"></i>
                                        </label>
                                    </div>
                                    <div class="star-rating-group">
                                        <input type="radio" class="star-rating-control" name="valor" value="5" id="five-star">
                                        <label for="five-star" class="star-rating-label">
                                            <i class="fas fa-star"></i>
                                        </label>
                                    </div>
                                </div>
                                <span class="invalid-feedback d-block" id="info_one-star">
                                    <strong>@error('valor'){{ $message }}@enderror</strong>
                                </span>
                            </div>
                            {{-- TITULO DA AVALIAÇÃO --}}
                            <div class="form-group">
                                <label for="title">Titulo</label>
                                <input type="text" name="titulo" class="form-control" id="title" required maxlength="60" value="{{ old('titulo') }}" placeholder="Ex: Excelente produto">
                                <span class="invalid-feedback @error('titulo') d-block @enderror" id="info_title">
                                    <strong>@error('titulo'){{ $message }}@enderror</strong>
                                </span>
                            </div>

                            {{-- MENSAGEM DA AVALIAÇÃO --}}
                            <div class="form-group">
                                <label for="message">Mensagem</label>
                                <textarea name="mensagem" id="message" class="form-control" rows="7" required maxlength="255" placeholder="Diga-nos o que achou do produto">{{ old('mensagem') }}</textarea>
                                <span class="invalid-feedback @error('mensagem') d-block @enderror" id="info_message">
                                    <strong>@error('mensagem'){{ $message }}@enderror</strong>
                                </span>
                            </div>
                            {{-- BOTÕES DE AÇÃO --}}
                            <div class="float-right">
                                <a href="{{ route('user.orders.show', [$pedido->id]) }}" class="btn btn-outline-secondary btn-lg mr-2">Voltar</a>
                                <button type="button" onclick="checkFormValidity('form_aval')" class="btn btn-primary btn-lg">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('pl-custom-scripts')
    <script> let val = {{ old('valor') != null ? old('valor') : 0 }}; </script>
@endsection