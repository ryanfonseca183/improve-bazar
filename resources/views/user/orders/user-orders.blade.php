@extends('layouts.profile')

@section('title', 'Compras')

@section('profile-content')
    <div class="col px-md-4">
        <main>
            <h1 class="profile-page-title h5">Compras</h1>
            @if(!$orders->isEmpty())
                @foreach($orders as $pedido)
                    <div class="card custom-card">
                        <div class="mb-4">
                            <h2 class="order-number">Compra n° {{ $pedido->id }}</h2>
                            <strong class="order-deadline">Chegará até o dia {{ $pedido->data_entrega }}</strong>
                        </div>
                        {{-- STATUS DA COMPRA --}}
                        <x-order-status></x-order-status>

                        {{-- ITENS DA COMPRA --}}
                        <h2 class="h6">Itens</h2>
                        @if($pedido->produtos()->count() > 1)
                            @php $produtos = $pedido->produtos; @endphp 
                            <x-order-products-list :pedido="$pedido" :produtos="$produtos"></x-order-products-list>
                        @else 
                            @php $produto = $pedido->produtos()->first(); @endphp
                            <x-order-product :pedido="$pedido" :produto="$produto"></x-order-product>
                        @endif
                        <a href="{{ route('user.orders.show', ['order' => $pedido->id]) }}"class="btn btn-primary order-details">Detalhes do pedido</a>
                    </div>
                @endforeach
                <div class="hflex-center">
                    {{$orders->links()}}
                </div>
            @else 
                <div class="card custom-card">
                    <div class="row vflex-column-center mt-5">
                        <div class="col-6 col-sm-4">
                            <img src="{{ asset('img/orders-not-found.png') }}" class="img-fluid mb-4" alt="">
                        </div>
                        <div class="w-100"></div>
                        <div class="col-sm-8 col-md-12 col-lg-8 text-center">
                            <h2 class="h6">Voce ainda não fez compras na loja</h2>
                            <p class="mb-0">Não sabe o que comprar? Navegue pelo nosso catálogo de produtos para conhecer todas as nossas incríveis ofertas.</p>
                        </div>
                    </div>
                </div>
            @endif
        </main>
    </div>
@endsection

@section('pl-custom-scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection