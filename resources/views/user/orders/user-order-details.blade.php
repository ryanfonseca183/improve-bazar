@extends('layouts.profile')

@section('title', 'Compras')

@section('profile-content')
    <div class="col px-md-4">
        <main >
            <div class="row no-gutters flex-between profile-page-title">
                <div class="col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('user.orders') }}">Compras</a></li>
                            <li class="breadcrumb-item active">Compra n° {{ $pedido->id }}</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-auto">
                    <strong class="text-success">Chegará até o dia {{$pedido->data_entrega}}</strong>
                </div>
            </div>
            <div class="card custom-card">
                {{-- STATUS DO PEDIDO --}}
                <h2 class="h6 text-center mb-3">Status do pedido</h2>
                <x-order-status></x-order-status>

                {{-- RESUMO DA COMPRA E MÉTODO DE PAGAMENTO --}}
                <div class="row mb-4">
                    <div class="col-sm-6 col-md-12 col-lg-6 mb-4 mb-sm-0 mb-md-4 mb-lg-0">
                        <h2 class="h6">Resumo da compra</h2>
                        <div class="card order-card full">
                            @php 
                                $summary['subTotal'] =  $pedido->subTotal;
                                $summary['subTotalSemDesconto'] = $pedido->subTotal + $pedido->desconto;
                                $summary['frete']['sedex']['price'] = $pedido->valor_frete;
                                $summary['total'] = $pedido->total;
                            @endphp
                            <x-order-summary :summary="$summary"></x-order-summary>
                        </div>
                    </div>
                    <div class="col-sm-6  col-md-12 col-lg-6">
                        @php $payment = $pedido->metodoPagamento; @endphp 
                        <h2 class="h6">Método de pagamento</h2>
                        <div class="card order-card full">
                            <div class="form-row align-items-center">
                                <div class="col-sm-auto">
                                    <div class="order-card-icon">
                                        <i class="{{ $payment->icone }}"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <h3 class="order-payment-name">{{ $payment->nome }}</h3>
                                    <p class="order-payment-desc">
                                        @if($payment->label == "cartao")
                                            {{$pedido->num_parcelas }}x de R$ {{ number_format(($pedido->total / $pedido->num_parcelas), 2, ',', '.') }} sem juros
                                        @else
                                            A vista
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                {{-- ENDEREÇO DE ENTREGA --}}
                <h2 class="h6">Endereço de entrega</h2>
                <div class="card order-card mb-4">
                    <div class="row">
                        <div class="col-sm-auto">
                            <div class="order-card-icon ">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                        </div>
                        <div class="col">
                            @php $end = $pedido->endereco; @endphp
                            <address class="mb-0">
                                {{$end->rua}}, {{ $end->numero }}<br>
                                {{$end->bairro}}<br>
                                {{$end->cidade}} {{$end->uf_sigla}}, {{$end->cep}}<br>
                            </address>
                        </div>
                    </div>
                </div>

                {{-- ITENS DA COMPRA --}}
                <h2 class="h6">Itens da compra</h2>
                @if($pedido->produtos()->count() > 1)
                    @php $produtos = $pedido->produtos; @endphp 
                    <x-order-products-list :pedido="$pedido" :produtos="$produtos"></x-order-products-list>
                @else 
                    @php $produto = $pedido->produtos()->first(); @endphp
                    <x-order-product :pedido="$pedido" :produto="$produto"></x-order-product>
                @endif
            </div>
        </main>
    </div>
@endsection