@extends('layouts.profile')

@section('title', 'Favoritos')

@section('profile-content')
    <div class="col px-md-4">
        <main>
            <h1 class="profile-page-title h5">Favoritos</h1>
            @if(!$produtos->isEmpty())
                <x-products-shop-section :produtos="$produtos" action="desfav"></x-products-shop-section>
            @else
                <div class="card custom-card">
                    <div class="row vflex-column-center mt-5">
                        <div class="col-6 col-sm-3">
                            <img src="{{ asset('img/favorites.png') }}" class="img-fluid mb-4" alt="">
                        </div>
                        <div class="w-100"></div>
                        <div class="col-sm-8 col-md-12 col-lg-8 text-center">
                            <h2 class="h6">Nenhum produto favoritado</h2>
                            <p class="mb-0">Ao encontrar o produto que te interessa, salve nos favoritos<br class="d-none d-lg-inline"> se não puder finalizar a compra no momento.</p>
                        </div>
                    </div>
                </div>
            @endif
        </main>
    </div>
@endsection