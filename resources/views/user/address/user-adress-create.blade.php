@extends('layouts.compressed-header')

@section('title', 'Cadastrar endereço')
@section('content')
    <main class="py-5">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                <form action="{{ route('address.store') }}" method="POST" class="p-4 p-sm-5 rounded shadow-sm needs-validation" id="address_form" novalidate>
                    @csrf
                    <h1 class="h4 mb-4">Novo endereço</h1>
                    @isset($redirect) <input type="hidden" name="redirect" value="{{ $redirect }}">@endisset
                    <div class="form-row" id="required-fields">
                        <div class="form-group col-sm-6">
                            <label for="cep">CEP 
                                <a target="_blank" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="ml-2">Não sei meu CEP</a>
                            </label>
                            <div class="cep-container">
                                <input type="text" required maxlength="9" value="{{ isset($cep) ? $cep : old('cep')}}" class="form-control mb-2 @if($errors->has('cep')) is-invalid @endif" id="cep" name="cep">
                                <span class="invalid-feedback" id="info_cep">
                                    <strong>
                                        @error("cep")
                                            {{$message}}
                                        @enderror
                                    </strong>
                                </span>
                                <div class="spinner-border text-primary" style="display:none;" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-sm">
                            <label for="uf">Estado</label>
                            <input id="uf" name="estado" required value="{{ isset($address) ? $address['uf'] : old('estado')}}" class="form-control @error('estado') is-invalid @enderror">
                            <span class="invalid-feedback" id="info_uf">
                                <strong>
                                    @error('estado')
                                    {{ $message }}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-sm">
                            <label for="cidade">Cidade</label>
                            <input id="cidade" name="cidade" required value="{{ isset($address) ? $address['city'] : old('cidade')}}" class="form-control @error('cidade') is-invalid @enderror">
                            <span class="invalid-feedback" id="info_cidade">
                                <strong>
                                    @error('cidade')
                                    {{ $message }}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-sm-6">
                            <label for="bairro">Bairro</label>
                            <input type="text" id="bairro" name="bairro" value="{{ isset($address) ? $address['district'] : old('bairro')}}" required maxlength="255" class="form-control @error('bairro') is-invalid @enderror">
                            <span class="invalid-feedback" id="info_bairro">
                                <strong>
                                    @error('bairro')
                                        {{$message}}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        
                        <div class="w-100"></div>
                        <div class="form-group col-8 col-md-6">
                            <label for="rua">Rua</label>
                            <input type="text" id="rua" required name="rua" value="{{ isset($address) ? $address['street'] : old('rua')}}" maxlength="255" class="form-control @error('rua') is-invalid @enderror">
                            <span class="invalid-feedback" id="info_rua">
                                <strong>
                                    @error('rua')
                                        {{$message}}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                        
                        <div class="form-group col-4 col-md-6">
                            <label for="numero">Número</label>
                            <input type="number" id="numero" name="numero" value="{{ old('numero') }}" required class="form-control @error('numero') is-invalid @enderror">
                            <span class="invalid-feedback" id="info_numero">
                                <strong>
                                    @error('numero')
                                        {{$message}}
                                    @enderror
                                </strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label for="complemento">Complemento <span class="small">(Opcional)</span></label>
                            <input type="text" id="complemento" name="complemento" value="{{ old('complemento')}}" maxlength="255" class="form-control @error('complemento') is-invalid @enderror" placeholder="Ex: Perto da igreja">
                            @error('complemento')
                                <span class="invalid-feedback">
                                    <strong>
                                        {{ $message }}
                                    </strong>
                                </span>
                            @enderror
                        </div>
                        <div class="w-100"></div>
                        <div class="form-group col-sm-6">
                            <label for="telefone">Telefone fixo <span class="small"> (Opcional)</span></label>
                            <input type="text" id="telefone" name="telefone" value="{{ old('telefone') }}" class="form-control @error('telefone') is-invalid @enderror">
                            @error('telefone')
                                <span class="invalid-feedback">
                                    <strong>
                                        {{ $message }}
                                    </strong>
                                </span>
                            @enderror
                        </div>
                        <div class="w-100"></div>
                        <div class="col-sm-6 mt-4">
                            <div class="form-row">
                                <div class="col">
                                    <a href=" {{ route('user.profile') }} " class="btn btn-outline-secondary btn-block">Cancelar</a>
                                </div>
                                <div class="col">
                                    <button type="button" onclick="checkGroupValidity('required-fields', 'submit', 'address_form')" class="btn btn-success btn-block">Cadastrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection

@section('pl-custom-scripts')
    <script type="text/javascript" src="{{ asset('js/dependencies/mask.js') }}"></script>
    <script>
        $("#telefone").mask('(00)0000-0000');
        let address_uf = $("#uf");
        let address_city = $("#cidade");
        let address_bairro = $("#bairro");
        let address_street = $("#rua");
        let address_cep = $("#cep");
        let address_cep_feedback = $("#info_cep strong");
        let address_complement = $("#complemento");
        let addressControls = [address_uf, address_city, address_bairro, address_street,  address_complement, address_cep];
        const spinner = $(".cep-container .spinner-border");

        address_cep.mask('00000-000');
        address_cep.change(function(){
            consultarCEP($(this).val());
        })
    </script>
@endsection