<div class="product-wrapper"> 
    <div class="product-content">
        <div class="product-info">
            @php 
                $image = $produto->fotosProduto()->where('is_main', 1)->first();
                $preco = $produto->preco;
                $desconto = $produto->desconto;
                if($preco != $desconto) $hasPromo = true; else $hasPromo = false;
            @endphp 
            <div class="product-image">
                <img src="{{asset('storage/' .$image->caminho)}}" alt="{{ $produto->nome }}">
            </div>
            <div class="product-price">
                <div class="price-info">
                    @if($produto->filhos_count != 0) A partir de @endif
                    @if($hasPromo)<span class="old-price">R$ {{ number_format($preco, 2, ',', '.') }}</span> @endif
                </div>
                <h2 class="current-price">R$ {{ number_format($desconto, 2, ',', '.') }} 
                    @if($hasPromo) <span class="badge badge-success">{{ $produto->promo_value }}% OFF</span> @endif
                </h2>
            </div>
            <div class="product-name">
                <h2 class="h6">{{ $produto->nome }}</h2>
            </div>
            <a href="{{ route('shop.produto', [$produto->id]) }}" class="btn add-to-bag"></i>Ver mais</a>
        </div>
        <div class="product-overlay">
            <div class="overlay-content">
                <div class="product-price">
                    <div class="price-info">
                        @if($produto->filhos_count != 0) A partir de @endif
                        @if($hasPromo)<span class="old-price">R$ {{ number_format($preco, 2, ',', '.') }}</span> @endif
                    </div>
                    <h2 class="current-price">R$ {{ number_format($desconto, 2, ',', '.') }} 
                        @if($hasPromo) <span class="badge badge-success">{{ $produto->promo_value }}% OFF</span> @endif
                    </h2>
                </div>
                <div class="product-name">
                    <h2 class="h6">{{ $produto->nome }}</h2>
                </div>
                <a href="{{ route('shop.produto', [$produto->id]) }}" class="btn btn-primary"></i>Ver mais</a>
            </div>
        </div>
    </div>
</div>