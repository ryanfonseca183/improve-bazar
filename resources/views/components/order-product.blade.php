
                                
@php if($produto->parente()->exists()) $auxiliar = $produto->parente; else $auxiliar = $produto; @endphp
<div class="card order-card">
    <div class="row align-items-center">
        <div class="col-sm-auto order-col">
            <div class="listable-product-image img-sm thumb">
                @php $image = $auxiliar->fotosProduto()->where('is_main', 1)->first(); @endphp
                <img src="{{ asset('storage/' . $image->caminho) }}" alt="" class="img-fluid">
            </div>
        </div>
        @if($auxiliar->is_visible && !$auxiliar->is_deleted)
            <div class="col order-col">
                <a href="{{ route('shop.produto', [$produto->id]) }}">{{$produto->nome}}</a><br>
                <span>{{ $produto->pivot->quantidade }}x unidade(s) - R$ {{$produto->pivot->total}}</span>
            </div>
            <div class="col-sm-auto order-col">
                @php $avaliacao = Auth::user()->avaliacoes()->where('avaliacoes.produtos_id', $auxiliar->id)->first(); @endphp
                @isset($avaliacao)
                    <div class="row order-aval">
                        <div class="col-auto">
                            <span>Sua nota</span>
                        </div>
                        <div class="col-auto">
                            <h3 class="h5 mb-0">
                                {{ $avaliacao->valor }}
                                <i class="fas fa-star order-star-icon"></i>
                            </h3>
                        </div>
                        <div class="col-auto mt-sm-2">
                            <a href="{{ route("orders.produto.avaliacao.edit", [$pedido->id, $produto->id, $avaliacao->id]) }}">Editar</a>
                        </div>
                    </div>
                @else
                    <a href="{{ route('orders.produto.avaliacao.create', [$pedido->id, $produto->id]) }}" class="btn btn-action">
                        <span class="mr-1">Avaliar</span>
                        <i class="fas fa-star"></i>
                    </a>
                @endif
            </div>
        @else 
            <div class="col">
                <h3 class="h6 mb-0">{{$produto->nome}}</h3>
                <span class="d-inline-block mb-4">{{ $produto->pivot->quantidade }}x unidade(s) - R$ {{$produto->pivot->total}}</span>
                <div class="alert alert-warning mb-0">
                    <i class="fas fa-exclamation-circle mr-2"></i>
                    Produto deletado ou temporariamente indisponível
                </div>
            </div>
        @endif
    </div>
</div>