
<div class="products-shop-container">

    @foreach($produtos as $produto)
        @php 
            $preco = $produto->preco;
            $desconto = $produto->desconto;
            if($produto->parente()->exists()) $produto = $produto->parente;
            $avaliacoes_count = $produto->avaliacoes()->count();
            $image = $produto->fotosProduto()->where('is_main', 1)->first();
            if($preco != $desconto) $hasPromo = true; else $hasPromo = false;
        @endphp
        <div class="product-shop-wrapper">
            <div class="row vflex-center">
                {{-- IMAGEM DO PRODUTO --}}
                <div class="col-sm-auto my-3">
                    <a href="{{ route('shop.produto', [$produto->id]) }}">
                        <div class="listable-product-image img-sm">
                            <img  src="{{ asset("storage/".$image->caminho) }}" class="img-fluid" alt="{{ $produto->nome }}">
                        </div>
                    </a>
                </div>
        
                {{-- INFORMAÇÕES DO PRODUTO --}}
                <div class="col mb-3 mb-sm-0">
                    {{-- NOME DO PRODUTO --}}
                    <a href="{{ route('shop.produto', $produto->id) }}">
                        <h2 class="h5">{{ $produto->nome }}</h2>
                    </a>
        
                    {{-- AVALIAÇÕES --}}
                    @if($avaliacoes_count > 3)
                        <div class="stars-outer">
                            <div class="stars-inner" style="width:{{20 * (float) $produto->avaliacoes()->avg('valor') }}%"></div>
                        </div>
                        <span class="small">({{ $avaliacoes_count }} avaliacoes)</span>
                    @endif
        
                    {{-- PREÇO DO PRODUTO --}}
                    <div id="product_price">
                        @if($produto->is_avaliable)
                            <span class="old-price @if(!$hasPromo) d-none @endif" id="product_old_price">R$ {{ number_format($preco, 2, ',', '.') }} </span>
                            <div class="vflex-center">
                                <h2 class="current-price" id="product_current_price">R$ {{ number_format($desconto, 2, ',', '.') }}</h2>
                                <span class="badge badge-success @if(!$hasPromo) d-none @endif ml-2" id="product_descount">{{ $produto->promo_value }}% OFF </span>
                            </div>
                        @else 
                            <span class="badge badge-danger">Sem estoque</span>
                        @endif
                    </div>
                </div>

                <div class="col-auto col-action">
                    @if($action == "fav")
                        {{-- ADICIONAR AOS FAVORITOS --}}
                        @if(Auth::check() && Auth::user()->favoritos->produtos()->where('produtos.id', $produto->id)->exists())
                            <span class="btn-action">
                                <i class="fas fa-heart text-orange"></i>
                            </span>
                        @else 
                            <form action="{{ route("favorites.produto.store", [$produto->id]) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-action">
                                    <i class="fas fa-heart"></i>
                                </button>
                            </form>
                        @endif
                    @else
                        {{-- TIRAR DOS FAVORITOS --}}
                        <form action="{{ route('favorites.produto.delete', [$produto->id]) }}" method="POST">
                            @method('DELETE')
                            @csrf 
                            <button type="submit" class="btn btn-action" title="Tirar dos favoritos">
                                <i class="fas fa-trash-alt "></i>
                            </button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="hflex-center">
    {{$produtos->links()}}
</div>