<div class="col-md-6  form-group">
    <label for="{{ $atr->label }}">{{ $atr->nome }}</label>
    @if($atr->is_definable == 0)
        <select 
            id="{{ $atr->label }}" 
            class="custom-select valor" 
            data-atr-id="{{ $atr->id }}">
                @foreach($atr->valoresAtributos as $val)
                    <option value="{{ $val->valor }}">{{ $val->valor }}</option>
                @endforeach
        </select>
    @else 
        <input 
        type="text" 
        required 
        maxlength="255" 
        class="form-control valor" 
        id="{{ $atr->label }}"
        data-name="{{ $atr->nome }}"
        data-atr-id="{{ $atr->id }}" 
        list="list_{{ $atr->label }}" />

        <span class="invalid-feedback" id="info_{{$atr->label}}">
            <strong></strong>
        </span>
        {{-- Valores predefinidos --}}
        @if($atr->valoresAtributos()->exists())
            <datalist id="list_{{ $atr->label }}">
                @foreach($atr->valoresAtributos as $val)
                    <option value="{{ $val->valor }}">
                @endforeach
            </datalist>
        @endif
    @endif
</div>