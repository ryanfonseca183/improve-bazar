<ul class="list-group list-min-selectable border-0">
    <li 
    class="list-group-item px-4" 
    onclick="setAtribute('{{ $atr->id}}', 'Sim', '{{ $atr->nome }}', this)">
        <span>É {{$atr->nome}}</span>
        <i class="fas fa-long-arrow-alt-right"></i>
    </li>
    <li 
    class="list-group-item px-4" 
    onclick="setAtribute('{{ $atr->id}}', 'Não', null, this)">
        <span>Não é {{$atr->nome}}</span>
        <i class="fas fa-long-arrow-alt-right"></i>
    </li>
</ul>