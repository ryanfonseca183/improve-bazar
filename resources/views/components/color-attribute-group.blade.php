{{-- INPUT DO TIPO COR --}}
<div class="attribute-group @if(!$atr->valoresAtributos()->exists()) py-5 @endif" id="group_{{$atr->label}}">
    <label class="attribute-label" for="{{$atr->label}}">{{ $atr->nome }}</label>
    @if($atr->is_definable == 1)
        <div class="form-row">
            <div class="col">
                <input 
                type="color" 
                class="form-control" 
                id="{{$atr->label}}" 
                data-atr-id="{{ $atr->id }}" />
            </div>
            <div class="col">
                <input 
                type="text" 
                class="form-control"
                placeholder="Nome da cor"
                name="{{ $atr->label }}" 
                id="label_{{$atr->label}}"
                data-name="nome"
                required 
                maxlength="255"/>
                <span class="invalid-feedback" id="info_label_{{$atr->label}}">
                    <strong></strong>
                </span>
            </div>
            <div class="col-auto">
                <button 
                type="button" 
                class="btn btn-primary" onclick="setAtributeFromInput('{{$atr->label}}')">
                    <i class="fas fa-long-arrow-alt-right"></i>
                </button>
            </div>
        </div>
    @endif
</div>
{{-- VALORES PREDEFINIDOS DO TIPO COR --}}
@if($atr->valoresAtributos()->exists())
    <ul class="list-group list-min-selectable border-0" id="list_{{ $atr->label }}">
        @foreach($atr->valoresAtributos as $val)
            <li 
            class="list-group-item px-4" 
            data-parent="#list_{{ $atr->label }}" 
            onclick="setAtribute('{{ $atr->id}}', '{{ $val->valor }}', '{{$val->label}}', this)">
                <div class="input-group-transparent">
                    <i class="input-show-color" style="background-color:{{ $val->valor }}"></i>
                    <span class="ml-2 pl-2">{{ $val->label }}</span>
                </div>
                <i class="fas fa-long-arrow-alt-right"></i>
            </li>
        @endforeach 
    </ul>
@endif