<div class="table-responsive order-itens">
    <table class="table">
        <thead>
            <tr><th class="sr-only">Nome</th></tr>
        </thead>
        <tbody>
            @foreach($produtos as $prod) 
                @php if($prod->parente()->exists()) $auxiliar = $prod->parente; else $auxiliar = $prod; @endphp
                <tr>
                    <td>
                        @if($auxiliar->is_visible && !$auxiliar->is_deleted)
                            <div class="flex-between">
                                <span>{{$prod->pivot->quantidade }}x <a href="{{ route("shop.produto", $prod->id) }}"> {{ $prod->nome }}</a></span>
                                <div>
                                    @php $avaliacao = Auth::user()->avaliacoes()->where('avaliacoes.produtos_id', $auxiliar->id)->first(); @endphp
                                    @isset($avaliacao)
                                        <h3 class="h5 mb-0 d-inline-block mr-2">
                                            {{ $avaliacao->valor }}
                                            <i class="fas fa-star order-star-icon"></i>
                                        </h3>
                                        <a href="{{ route("orders.produto.avaliacao.edit", [$pedido->id, $prod->id, $avaliacao->id]) }}" class="btn btn-action">Editar</a>
                                    @else 
                                        <a href="{{ route("orders.produto.avaliacao.create", [$pedido->id, $prod->id]) }}" class="btn btn-action">Avaliar</a>
                                    @endif
                                </div>
                            </div>
                        @else 
                            {{$prod->pivot->quantidade }}x {{ $prod->nome }}
                            <i class="fas fa-exclamation-circle ml-2 text-warning" data-toggle="tooltip" data-placement="right" title="Produto indisponível"></i>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>