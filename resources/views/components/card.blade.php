@php 
    if($cardRole == "select") {
        $cardPad = "p-0";
        $bodyPad = "p-0";
    } else{
        $cardPad = null;
        $bodyPad = "py-5";
    }
@endphp

<div class="card  shadow-sm {{ $cardPad }}">
    <div class="card-header bg-orange text-white">
        <div class="row align-items-center">
            <div class="col">
                <h2 class="h5 mb-0 p-2">{{ $cardTitle }}</h2>
            </div>
            @if(isset($card_header))
                <div class="col-auto">
                    {{$card_header}}
                </div>
            @endif
        </div>
    </div>
    @if(isset($card_body))
        <div class="card-body {{ $bodyPad }}">
            {{ $card_body }}
        </div>
    @endif
    @if(isset($card_footer))
        <div class="card-footer">
            {{ $card_footer }}
        </div>
    @endif
</div>