@php $type = $atr->tipoDeDados->frontend_type_label;  @endphp
@if($type == "Lógico") 
    <li class="nav-item filter">    
        <button class="btn btn-filter">
            <label class="mb-0" for="{{ $atr->label }}">{{ $atr->nome }}</label>
            <input type="checkbox" @if(isset($filters[$atr->label])) checked @endif id="{{ $atr->label }}" name="{{ $atr->label }}" value="label_{{ $atr->nome }}" class="form-check logic-filter">
        </button>
    </li>
@else 
    @php 
        $valAtributos = [];
        foreach($atr->produtos()->where('is_deleted', 0)->where('is_avaliable', 1)->get() as $prod) {
            if($type == "Caractere") $val = $prod->pivot->valor; else $val = $prod->pivot->label;
            if(!in_array($val, $valAtributos) && $val != "Não") $valAtributos[] = $val;
        }
        sort($valAtributos);
    @endphp
    @if(count($valAtributos) != 0)
        <li class="nav-item filter">
            @if($type != "Lógico")
                {{-- BOTÃO DE EXPANDIR FILTRO --}}
                <button class="btn btn-filter" type="button" data-toggle="collapse" data-target="#{{ $atr->label }}">
                    <span>{{ $atr->nome }}</span>
                    <i class="fas fa-chevron-down text-orange"></i>
                </button>
                {{-- FILTRO DE PESQUISA --}}
                <div class="collapse p-3" id="{{ $atr->label }}" data-parent="#filters">
                    {{-- BARRA DE PESQUISA --}}
                    <div class="form-group">
                        <label>Pesquisar
                            <div class="input-group mt-2">
                                <input type="text" class="form-control search-input" data-list="list_{{ $atr->label }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fas fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </label>
                    </div>
                    {{-- LISTAS DE OPÇÕES --}}
                    <div class="filter-groups-container">
                        <div class="filter-options-group">
                            <ul class="filter-options-list" id="list_{{ $atr->label }}">
                                @foreach($valAtributos as $value)
                                    {{--  MONTANDO A URL --}}
                                    @php
                                        $label = $value;
                                        if($type == "Cor") $value = 'label_' . $value;
                                        $array = $filters;
                                        $array[$atr->label] = $value;
                                    @endphp 
                                    {{-- EXIBINDO A OPÇÃO --}}
                                    <li class="filter-option">
                                        <a href="{{ route('shop.catalogo', $array) }}">
                                            {{ $label }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
        </li>
    @endif
@endif
