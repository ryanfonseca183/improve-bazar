@php 
    $diferenca = $summary['subTotalSemDesconto'] - $summary['subTotal']; 
    $sedex = $summary['frete']['sedex'];
@endphp
<ul class="list-group list-group-flush summary-info">
    <li class="list-group-item">
        <div class="flex-between">
            <span>Subtotal</span>
            <strong id="subtotal">R$ {{ number_format($summary['subTotal'], 2, ',', '.') }} </strong>
        </div>
        @if($diferenca != 0)
            <div class="alert alert-success small mb-0 mt-2">
                Economia de R$ <span id="diferenca">{{ number_format($diferenca, 2, ',', '.') }}</span> em descontos
            </div>
        @endif
    </li>
    <li class="list-group-item">
        <div class="flex-between">
            <h3 class="h6 mb-0">Frete</h3>
            <strong id="shipping-price">{{ isset($sedex['price']) ? "R$ " . number_format($sedex['price'], 2, ',', '.') : "--" }}</strong>
        </div>
        <div class="text-success"><strong id="shipping-message"></strong></div>
    </li>
    <li class="list-group-item">
        <div class="flex-between">
            <h3 class="h6 mb-0">Total</h6>
            <strong id="total">R$ {{number_format($summary['total'], 2, ',', '.')}}</strong>
        </div>
    </li>
</ul>