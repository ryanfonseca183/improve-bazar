 {{-- INPUT TIPO TEXT--}}
 <div class="attribute-group @if(!$atr->valoresAtributos()->exists()) py-5 @endif" id="group_{{ $atr->label }}">
    <label class="attribute-label" for="{{$atr->label}}">{{$atr->nome}}</label>
    {{-- VERIFICA SE É POSSÍVEL DEFINIR UM VALOR PARA O ATRIBUTO --}}
    @if($atr->is_definable == 1)
        <div class="form-row">
            <div class="col">
                <input 
                type="text"
                id="{{$atr->label}}"
                class="form-control @if(!$atr->valoresAtributos()->exists()) form-control-lg @endif"
                required
                maxlength="255"
                data-name="{{ $atr->nome }}"
                data-atr-id="{{ $atr->id }}" />
                <span class="invalid-feedback" id="info_{{$atr->label}}">
                    <strong></strong>
                </span>
            </div>
            @if($atr->valoresAtributos()->exists())
                <div class="col-auto">
                    <button onclick="setAtributeFromInput('{{$atr->label}}')" class="btn btn-primary">
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </button>
                </div>
            @else 
                <div class="col-lg-12 text-right mt-3">
                    <button onclick="setAtributeFromInput('{{$atr->label}}')" class="btn btn-primary">
                        Próximo
                    </button>
                </div>
            @endif
        </div>
    @endif
</div>
{{-- VALORES PREDEFINIDOS DO TIPO CARACTERE --}}
@if($atr->valoresAtributos()->exists())
    <ul class="list-group list-min-selectable border-0" id="list_{{ $atr->label }}">
        @foreach($atr->valoresAtributos as $val)
            <li 
            class="list-group-item px-4" 
            data-parent="#list_{{ $atr->label }}" onclick="setAtribute('{{ $atr->id}}', '{{ $val->valor }}', null, this)">
                <span>{{ $val->valor }}</span>
                <i class="fas fa-long-arrow-alt-right"></i>
            </li>
        @endforeach 
    </ul>
@endif
