<div class="col-12 form-group">
    @if($atr->is_definable == 0)
        <div class="input-group">
            {{-- Cor --}}
            <input 
            type="color" 
            id="{{$atr->label}}" 
            class="d-none valor"
            data-atr-id="{{ $atr->id }}" 
            value="{{$atr->valoresAtributos[0]->valor}}"/>
            {{-- Nome da cor --}}
            <input 
            type="text" 
            class="d-none label" 
            id="label_{{ $atr->label}}" 
            value="{{$atr->valoresAtributos[0]->label}}"/>
        </div>
        

        <label for="btn_{{ $atr->label }}">{{ $atr->nome }}</label>
        {{-- Botão para mostrar os valores predefinidos --}}
        <button 
            class="form-control datalist-toggle" 
            type="button" 
            data-toggle="collapse" 
            data-target="#index_{{$atr->label}}" 
            id="btn_{{$atr->label}}">
                <span>{{ $atr->valoresAtributos[0]->label}}</span>
                <i class="fas fa-sort-down"></i>
        </button>
        {{-- Valores predefinidos --}}
        <div class="collapse custom-datalist" id="index_{{$atr->label}}">
            <ul class="list-group bg-white">
                @foreach($atr->valoresAtributos as $val)
                    <li 
                    class="datalist-option" onclick="loadDataListOption(this)" 
                    data-option-value="{{$atr->label}}/{{$val->valor}}" 
                    data-option-name="label_{{ $atr->label }}/{{ $val->label }}">
                        <div class="input-group-transparent">
                            <i class="input-show-color" style="background-color:{{ $val->valor }}"></i>
                            <span class="ml-2 pl-2">{{ $val->label }}</span>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    @else
        <div class="form-row">
            <div class="col">
                <label for="{{$atr->label}}">{{ $atr->nome}}</label>
                <input 
                type="color" 
                id="{{$atr->label}}" 
                class="form-control valor" 
                data-atr-id="{{ $atr->id }}"/>
            </div>
            <div class="col">
                <label for="label_{{$atr->label}}">Nome da cor</label>
                <div class="input-group">
                    <input 
                    type="text" 
                    class="form-control label" 
                    data-name="nome" 
                    id="label_{{ $atr->label}}" 
                    required 
                    maxlength="255" />
                    <div class="input-group-append">
                        <button class="input-group-text" type="button" data-toggle="collapse" data-target="#collapse_{{ $atr->label }}">
                            <i class="fas fa-chevron-down"></i>
                        </button>
                    </div>
                </div>
                {{-- VALORES DOS ATRIBUTOS CONFIGURÁVEIS --}}
                <div class="collapse custom-datalist" id="collapse_{{$atr->label}}">
                    <ul class="list-group bg-white">
                        @foreach($atr->valoresAtributos as $val)
                            <li 
                            class="datalist-option" onclick="loadDataListOption(this)" 
                            data-option-value="{{$atr->label}}/{{$val->valor}}" 
                            data-option-name="label_{{ $atr->label }}/{{ $val->label }}">
                                <div class="input-group-transparent">
                                    <i class="input-show-color" style="background-color:{{ $val->valor }}"></i>
                                    <span class="ml-2 pl-2">{{ $val->label }}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
</div>