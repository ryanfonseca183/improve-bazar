 {{-- Categorias --}}
 <div class="carousel-item @if(!isset($selected['subcategoria'])) active @endif">
    <div class="card">
        <div class="card-header bg-orange text-white">
            <h2 class="h5 mb-0 p-2">Selecione uma categoria</h2>
        </div>
        <div class="card-body p-0">
            <ul class="list-group list-min-selectable border-0" id="catList">
                @foreach($categorias as $cat)
                    @if($cat->subcategorias_count != 0)
                        <li 
                        class="list-group-item 
                        @if(isset($selected['categoria']) && $cat->id == $selected['categoria']->id)    
                            active 
                        @endif" 
                        data-parent="catList" 
                        onclick="loadLiValue(this, getSubcategorias)"
                        data-option-value="{{ $cat->id }}" 
                        data-submit="ajax">
                            <span>{{ $cat->nome }}</span>
                            <i class="fas fa-long-arrow-alt-right"></i>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>

 {{-- Subcategorias --}}
 <div class="carousel-item">
    <div class="card">
        <div class="card-header bg-orange text-white">
            <h2 class="h5 mb-0 p-2">Selecione uma subcategoria</h2>
        </div>
        <div class="card-body p-0">
            <form method="GET" action="{{ route('produto.create') }}" id="getAtributes">
                <!--Listagem das subcategorias-->
                <ul class="list-group list-min-selectable border-0" id="subcatList">
                    @if(isset($selected['categoria']))
                        @foreach($selected['categoria']->subcategorias as $subcat)
                            <li 
                            class="list-group-item option 
                            @if(isset($selected['subcategoria']) && $subcat->id == $selected['subcategoria']->id) 
                                active 
                            @endif" 
                            data-parent="subcatList" 
                            data-submit="{{ $action }}" 
                            data-option-value="subcatInput/{{ $subcat->id }}">
                                <span>{{ $subcat->nome }}</span>
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </li>
                        @endforeach 
                    @endif
                </ul>
                <input type="hidden" name="subcategoria" id="subcatInput">
            </form>
        </div>
    </div>
</div>
