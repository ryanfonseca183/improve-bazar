<section class="content-section">
    <div class="section-header">
        <div class="section-header-text">
            <h2 class="h4 mb-0 section-title">{{$nome}}</h2>
        </div>
    </div>
    <div class="section-body">
        <div id="{{$nome}}" class="carousel carousel-fade slide mb-5" data-interval="false">
            @if($produtos->count() > 1)
                <ol class="carousel-indicators">
                    @foreach($produtos->keys() as $key)
                        <li data-target="#{{$nome}}" data-slide-to="{{$key}}" @if($key == 0) class="active" @endif></li>
                    @endforeach
                </ol>
                <a class="control-prev d-none d-md-flex" href="#{{$nome}}" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="control-next d-none d-md-flex" href="#{{$nome}}" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                    <span class="sr-only">Next</span>
                </a>
            @endif
            <div class="carousel-inner">
                @foreach($produtos as $key => $group)
                    <div class="carousel-item @if($key == 0) active @endif">
                        <div class="row">
                            @foreach($group as $produto)
                                <div class="col-sm-6 col-lg-4">
                                    <x-product-wrapper :produto="$produto"></x-product-wrapper>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>