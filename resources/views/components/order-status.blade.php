<div class="steps mb-4">
    <div class="step active">
        <div class="indicator-container">
            <span class="step-indicator">
                <i class="fas fa-dollar-sign icon-default"></i>
                <i class="fas fa-check icon-active"></i>
            </span>
        </div>
        <span class="step-info ">Processando <br> pagamento</span>
    </div>
    <div class="step ">
        <div class="indicator-container">
            <span class="step-indicator">
                <i class="fas fa-shipping-fast icon-default"></i>
                <i class="fas fa-check icon-active"></i>
            </span>
        </div>
        <span class="step-info ">Pedido a <br>caminho</span>
    </div>
    <div class="step ">
        <div class="indicator-container">
            <span class="step-indicator">
                <i class="fas fa-thumbs-up icon-default"></i>
                <i class="fas fa-check icon-active"></i>
            </span>
        </div>
        <span class="step-info ">Pedido <br>entregue</span>
    </div>
</div>