@extends('layouts.dashboard')
@extends('layouts.modal-delete')

@section('title', 'Atributos')

@section('icon')
    <i class="fas fa-tint"></i>
@endsection

@section('desc', 'Cadastre, atualize ou remova os atributos das subcategorias de produtos ofertados na loja.')

@section('route-delete', route('atributo.destroy', [$atributo->id]))

@section('modal-text')
    @if($atributo->is_configurable == 1) 
        <h3 class="h6">Atributo configurável</h3>
        <p >Ao deleta-lo, todos os produ&shy;tos configu&shy;ráveis com base nesse atributo também serão dele&shy;tados. </p>
        <p >Atualmente, <strong>{{$atributo->produtos->count()}} produto(s)</strong> utilizam esse atributo.</p>

    @else
        <h3 class="h6">Atributo simples</h3>
        <p>Ao deleta-lo, podem surgir produtos identicos na loja. Esse processo não é reversível.</p>
        <p >Atualmente, <strong>{{$atributo->produtos->count()}} produto(s)</strong> utilizam esse atributo.</p>
    @endif
@endsection

@section('pl-custom-styles')
    <style>
        #info_values_carousel {
            min-height: 300px;
        }
        .carousel {
            padding-bottom: 1rem;
        }
        .carousel-fade .carousel-item {
            transition-property: opacity;
            transition-duration: 1s;
        }
        .carousel-indicators {
            margin-bottom: 0px;
            bottom: -1rem;
        }
        .carousel:not(#slidePhotos) .carousel-indicators li {
            background-color: var(--cinza-escuro);
            width: 15px;
            height: 15px;
            border-radius: 50%;
        }
        .carousel:not(#slidePhotos) .carousel-indicators li.active {
            background-color: var(--laranja);
        }
    </style>
@endsection

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 bg-transparent">
        <li class="breadcrumb-item"><a href="{{ route('atributo.index') }}">Atributos</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$atributo->nome}}</li>
        </ol>
    </nav>

    <div class="row justify-content-center mt-5">
       
        <!--Informações da lista-->
        <div class="col-lg-8 mb-5">
            <div class="card content-card">
                <div class="flex-between mb-4">
                    <h2 class="h5">Informações do atributo</h2>
                    <div>
                        <a href="{{ route('atributo.edit', [$atributo->id]) }}" class="btn btn-link">Atualizar</a>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalDelete">
                            Deletar
                        </button>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    
                    <li class="list-group-item px-0 py-4">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <h4 class="h6 m-0">Nome:</h4>
                            </div>
                            <div class="col">
                                {{$atributo->nome}}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 py-4">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <h4 class="h6 m-0">Subcategoria:</h4>
                            </div>
                            <div class="col">
                                {{$atributo->subcategoria->nome }}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 py-4">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <h4 class="h6 m-0">Tipo de dados:</h4>
                            </div>
                            <div class="col">
                                {{$atributo->tipoDeDados->frontend_type_label}}
                            </div>
                        </div>
                    </li>
                    @if($atributo->tipoDeDados->frontend_type_label != "Lógico")
                        <li class="list-group-item px-0 py-4">
                            <div class="row align-items-center">
                                <div class="col-sm-3">
                                    <h4 class="h6 m-0 ">Configurável</h4>
                                </div>
                                <div class="col">
                                    <span>{{ $atributo->is_configurable == 1 ? "Sim"  : "Não"}}</span>
                                </div>
                            </div>
                        </li>
                    @endif
                    <li class="list-group-item px-0 py-4">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <h4 class="h6 m-0">Visível</h4>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input 
                                    type="checkbox" 
                                    class="custom-control-input config" 
                                    id="is_visible" 
                                    @if($atributo->is_visible == 1) checked @endif>
                                    <label class="custom-control-label" for="is_visible"></label>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0 py-4">
                        <div class="row align-items-center">
                            <div class="col-sm-3">
                                <h4 class="h6 m-0">Obrigatório</h4>
                            </div>
                            <div class="col">
                                <div class="custom-control custom-switch">
                                    <input 
                                    type="checkbox" 
                                    class="custom-control-input config" 
                                    id="is_required" 
                                    @if($atributo->is_required == 1) checked @endif>
                                    <label class="custom-control-label" for="is_required"></label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        @if($atributo->tipoDeDados->frontend_type_label != 'Lógico')
            <div class="col-lg-8 mb-5 rounded">
                {{-- Valores predefinidos --}}
                <div class="card shadow-sm">
                    <div class="card-body px-4 pb-5 pt-4">
                        <h2 class="h5 mb-4">
                            Valores predefinidos
                            <button class="btn p-1" data-toggle="modal" data-target="#info_attributes_values">
                                <i class='fas fa-question-circle text-info'></i>
                            </button>
                        </h2>
                        
                        <h3 class="h6">{{$atributo->nome}}  </h3>
                        <!--Ajuda-->
                        <ul class="list-unstyled">
                            @if($atributo->tipoDedados->frontend_type_label == "Caractere")
                                <li>Para cadastrar um valor, digite-o no campo abaixo e clique em adicionar <i class="fas fa-plus text-primary ml-2"></i>.</li>
                                <li>Para remover um valor, clique, segure e arraste até a lixeira.</li>
                            @elseif($atributo->tipoDedados->frontend_type_label == "Cor")
                                <li>Para cadastrar uma cor, clique no campo abaixo, selecione a cor e clique em adicionar <i class="fas fa-plus text-primary ml-2"></i>.</li>
                                <li>Para remover uma cor, clique no botão de deletar <i class='fas fa-times ml-2 text-danger'></i></li>
                            @endif
                        </ul>
                        
                        
                        <div class="form-group" id="valGroup">
                            <!--Input para cadastro de novo valor-->
                            @if($atributo->tipoDeDados->frontend_type_label == "Cor")
                                <label for="valor">Cor</label>
                                <div class="input-group">
                                    <input 
                                    type="color" 
                                    id="valor" 
                                    name="valor" 
                                    class="form-control"/>
                                    <input 
                                    type="text" 
                                    id="label" 
                                    name="label"
                                    data-name="nome" 
                                    class="form-control" 
                                    required
                                    placeholder="Nome da cor"
                                    maxlength="60"/>
                                    <div class="input-group-append">
                                        <button
                                        type="button" 
                                        class="btn btn-primary" 
                                        onclick="checkVal({{$atributo->id}})">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <span class="invalid-feedback d-block" id="info_label">
                                    <strong></strong>
                                </span>
                            @else 
                                <div class="input-group">
                                    <input 
                                    type="{{ $atributo->tipoDeDados->frontend_type }}"
                                    data-name="{{ $atributo->nome }}"
                                    name="valor"
                                    id="valor"
                                    class="form-control"
                                    required
                                    maxlength="100">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="checkVal({{ $atributo->id }})">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <span class="invalid-feedback d-block" id="info_valor">
                                    <strong></strong>
                                </span>
                            @endif
                        </div>

                        <!--Lista dos valores cadastrados-->
                        <div class="form-group">
                            @if($atributo->tipoDeDados->frontend_type_label == "Caractere")
                                <ul class="list-group list-selectable mb-3" id="list-val">
                                    @if(!$atributo->valoresAtributos->isEmpty())
                                        @foreach($atributo->valoresAtributos as $val)
                                            <li 
                                            class="list-group-item list-value" 
                                            data-val-id="{{ $val->id }}" 
                                            data-value="{{ $val->valor }}">
                                                {{ $val->valor }}
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                                <!--Exclusão dos valores-->
                                <div class="form-group">
                                    <div class="row no-gutters py-3 justify-content-center" id="trash-val">
                                        <div class="col-auto">
                                            <i class="fas fa-trash-restore-alt" ></i>
                                        </div>
                                    </div>
                                </div>
                            @elseif($atributo->tipoDeDados->frontend_type_label == "Cor")
                                <div class="row" id="list-val">
                                    @if(!$atributo->valoresAtributos->isEmpty())
                                        @foreach($atributo->valoresAtributos as $val)
                                        <div 
                                        class="col-auto form-group list-value" 
                                        data-value="{{ $val->valor }}" 
                                        data-val-id="{{ $val->id }}">
                                            <div class="input-group-color">
                                                <i class="input-show-color" style="background-color:{{ $val->valor }};"></i>
                                                <span class="ml-2 pl-2">{{$val->label}}</span>
                                                <button 
                                                class='btn-color text-gray-600 bg-transparent' 
                                                type='button' 
                                                onclick='destroyVal({{$val->id}})'>
                                                    <i class='fas fa-times'></i>
                                                </button>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                            @endif
                        </div>

                        <!--É definível-->
                        <div class="form-group">
                            <div class="custom-control custom-switch mb-2">
                                <input 
                                type="checkbox" 
                                class="custom-control-input config" 
                                id="is_definable"
                                @if($atributo->is_definable == 1) checked @endif>
                                <label class="custom-control-label" for="is_definable">Permitir o cadastro de novos valores</label>
                            </div>
                            <p class="text-muted small">Desabilitando essa opção, no momento de cadastro dos produtos dessa subcategoria voce poderá apenas selecionar os valores da lista</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="info_attributes_values" tabindex="-1">
                <div class="modal-dialog modal-lg ">
                    <div class="modal-content p-4">
                        <div class="modal-body">
                            <div class="flex-between mb-5">
                                <h1 class="h3 ">Valores</h1>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="info_values_carousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#info_values_carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#info_values_carousel" data-slide-to="1"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-7">
                                                <img class="img-thumbnail d-block mx-auto" src="{{ asset('img/img2.png') }}" alt="">
                                            </div>
                                            <div class="col">
                                                <h2 class="h5 mb-2">Cadastro rápido e fácil</h2>
                                                <p>Os valores predefinidos serão listados no momento de cadastro dos produtos, permitindo que voce selecione-os.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-7">
                                                <img class="img-thumbnail d-block mx-auto" src="{{ asset('img/img.png') }}" alt="">
                                            </div>
                                            <div class="col">
                                                <h2 class="h5 mb-2">Forçar seleção</h2>
                                                <p>Com isso, não será possível definir um outro valor para o atributo no momento de cadastro dos produtos.</p>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
       
    </div>
@endsection


@section('pl-plugins')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

@section('pl-custom-scripts')
    

    @if($atributo->tipoDeDados->frontend_type_label == "Caractere")
        <script>
            $(document).ready(function () { 
                //Carrega a lista drag and drop
                $("#list-val" ).sortable({
                    revert: true,
                })
                //Carrega a lixeira
                $('#trash-val').droppable({
                    drop: function(event, ui) {
                        let id = $(ui.draggable).attr('data-val-id');
                        if(id != null) {
                            $(ui.draggable).remove();
                            destroyVal(id);
                        }
                    },
                })  
            })
        </script>
    @endif

    <script>
         $(document).ready(function () { 
            checkList()
        })
        //Adiciona evento de change aos inputs checkbox
        $('.config').change(function(){
            setConfig("{{ $atributo->id }}", this.id, this.checked)
        })

        //Valida o valor recebido e chama o método para armazenar no banco
        function checkVal(atr){
            let valor = document.getElementById('valor');
            let label = document.getElementById('label');
            let inputs = [];
            inputs.push(valor);
            if(label) inputs.push(label);
            let flag = validate(inputs);
            $("#valGroup").addClass("was-validated");
            if(flag) {
                //Verifica se o valor já foi cadastrado
                let listValues = document.querySelectorAll("#list-val .list-value");
                for(let i =0; i < listValues.length; i++) {
                    if(listValues[i].getAttribute('data-value') == valor.value) {
                        if(label != null) {
                            $("#info_label strong").html("Esse valor já foi cadastrado");
                        } else {
                            $("#info_valor strong").html("Esse valor já foi cadastrado");
                        }
                        flag = false;
                        break;
                    };
                }
                if(flag) {
                    let valLabel = null;
                    if(label != null) {
                        valLabel = label.value;
                    }
                    storeVal(atr, valor, valLabel);
                    $("#valGroup").removeClass("was-validated");
                }
            } 
        }
        var listVal = document.getElementById('list-val')
        //Verifica se a lista possui valores suficientes para forçar a seleção
        function checkList() {
            if($('#list-val .list-value').length == 0) {
                setConfig("{{ $atributo->id }}", 'is_definable', true);
                $("#is_definable").prop('checked', true);
                $('#is_definable').prop('disabled', true);
            } else {
                $('#is_definable').prop('disabled', false);
            }
        }
        //Armazena o novo valor
        function storeVal(atr, input, label) {
            $.ajax({
                url: '/dashboard/atributo/'+atr+'/valores',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    valor: input.value,
                    label: label,
                },
                dataType: "text",
                
                success: function(result, status, request) {
                    let res = JSON.parse(result);
                    newElement(input, res)
                },
                error: function(request, status, code) {
                    console.log(request, status, code)
                },
            });
        }
        //Gera um novo elemento para exibir o valor
        function newElement(input, result) {
            let element;
            switch(input.type) {
                case 'text':
                    let li = document.createElement('li');
                    li.classList.add('list-group-item', 'list-value');
                    li.innerHTML = result.valor;
                    li.setAttribute('data-val-id', result.id);
                    li.setAttribute('data-value', result.valor);
                    element =  li;
                break;
                case 'color':
                    let col = document.createElement('div');
                    col.classList.add('col-auto', 'form-group', 'list-value');
                    col.setAttribute('data-val-id', result.id);
                    col.setAttribute('data-value', result.valor);
                    col.innerHTML = 
                        '<div class="input-group-color">'+
                            '<i class="input-show-color" style="background-color:'+result.valor +';"></i>'+
                            '<span class="ml-2">'+ result.label +'</span>'+
                            '<button class="btn-color text-gray-600"  type="button"'+ 'onclick="destroyVal('+result.id+')">'+
                                "<i class='fas fa-times'></i>" +
                            '</button>'+
                        '</div>';

                    element = col;
                break;
            }
            if(listVal.firstElementChild != null) {
                listVal.insertBefore(element, listVal.firstElementChild);
            } else {
                listVal.appendChild(element);
            }
            checkList();
            input.value = "";
            $("#label").val("");
        }
        //Deleta um valor
        function destroyVal(id) { 
            $.ajax(
                {
                    url: '/dashboard/valores/'+id,
                    type: 'delete', // replaced from put
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (response) {
                        //Remove o elemento do DOM
                        $('[data-val-id="' + id + '"]').remove();
                        checkList()
                    },
                    error: function(xhr, status, erro) {
                        console.log(xhr, status, erro);
                    },
                });
        }
        //Habilita ou desabilita as configurações dos atributos
        function setConfig(idAtr, config, checked) {
            $.ajax({
                url: "/dashboard/atributo/"+ idAtr +"/config",
                method: "PUT",
                data: {
                    _token: "{{ csrf_token() }}",
                    config_name: config,
                    is_checked: checked,
                },
                error: function(xhr, status, error) {
                    console.log(xhr, status, error)
                },
            });
        }
        
    </script>
@endsection
