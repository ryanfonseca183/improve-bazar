@extends('layouts.dashboard')
@extends('layouts.modal-delete')

@section('title', 'Atributos')

@section('pl-custom-styles')
    <!-- Custom styles for this page -->
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style>
        
        .down {
            transform: rotateX(180deg);
        }
        .shown + tr {
            background: #f8f9fc !important;
        }
        .shown + tr .list-group-item {
            background: transparent;
        }
        .shown + tr label {
            font-weight: 600;
        }
    </style>
@endsection

@section('icon')
    <i class="fas fa-tint"></i>
@endsection

@section('desc', 'Cadastre, atualize ou remova os atributos das subcategorias de produtos ofertados na loja.')

@section('route-delete', route('atributo.destroy', 0))

@section('modal-text', 'Voce tem certeza que deseja deletar esse atributo? O processo não é reversível.')

@section('content')
    <div class="row my-4 justify-content-center my-5">

        <div class="col-lg-10 mb-5">
            <div class="card content-card">
                <div class="flex-between mb-5">
                    <h2 class="h5 mb-0">Atributos cadastrados 
                        <button type="button" class="btn p-1 ml-2" data-toggle="modal" data-target="#info_attributes">
                            <i class="fas fa-question-circle text-info"></i>
                        </button>
                    </h2>
                    <a href="{{ route('atributo.create') }}" class="btn btn-primary float-right"><i class="fas fa-plus mr-2"></i>Novo</a>
                </div>
                <div class="table-responsive">
                    <table class="table nowrap" id="atrDataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Nome</th>
                                <th>Subcategoria</th>
                                <th>Tipo</th>
                                <th class="text-center">Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal fade" id="info_attributes" tabindex="-1">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content p-5">
                    <div class="modal-body">
                        <div class="flex-between mb-5">
                            <h1 class="h3 ">Atributos</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row mb-4">
                            <div class="col-2 text-center">
                                <i class="fas fa-tint display-4 mt-3 text-orange"></i>
                            </div>
                            <div class="col">
                                <h2 class="h5 mb-2">Como características</h2>
                                <p>Os atributos nos permitem fornecer informações adicionais aos produtos de uma mesma subcategoria na loja.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2 text-center">
                                <i class="fas fa-sliders-h display-4 mt-3 text-orange"></i>    
                            </div>
                            <div class="col">
                                <h2 class="h5 mb-2">Como filtros de pesquisa</h2>
                                <p>Através destes elementos, é possível filtrar por determinados valores ao navegar pelo catálogo de produtos.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pl-plugins')
    <!--Plugins-->
    <script src="{{ asset('js/dependencies/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dependencies/dataTables.bootstrap4.min.js') }}"></script>
@endsection

@section('pl-custom-scripts')
    <script src="{{ asset('js/custom/dashboard/index.js') }}"></script>
    

    <script>
        function actionsButtons(id) {
            return `<div class="btn-group dropleft">
                        <button type="button" class="btn btn-action dropdown-toggle no-arrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                        <div class="dropdown-menu shadow-sm">
                            <button type="button" class="dropdown-item"  data-toggle="modal" data-target="#modalDelete" data-id="${id}">Deletar</button>
                            <a  class="dropdown-item" href="/dashboard/atributo/${id}/edit">Editar</a>
                            <a  class="dropdown-item" href="/dashboard/atributo/${id}">Ver atributo</a>
                        </div>
                    </div>`;
        }
        function rowFormat(d)  {
            return `<ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row"> 
                                <label class="text-bold h6 col-3">Visível</label>
                                <div class="col"><span> ${d.is_visible == 1 ? "<i class='fas fa-check-circle mr-2 text-success'></i> Sim" : "<i class='fas fa-times-circle mr-2 text-danger'></i>Não"} </span></div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row"> 
                                <label class="text-bold h6 col-3">Obrigatório</label>
                                <div class="col"><span> ${d.is_required == 1 ? "<i class='fas fa-check-circle mr-2 text-success'></i> Sim" : "<i class='fas fa-times-circle mr-2 text-danger'></i>Não"} </span></div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row"> 
                                <label class="text-bold h6 col-3">Configurável</label>
                                <div class="col"><span> ${d.is_configurable == 1 ? "<i class='fas fa-check-circle mr-2 text-success'></i> Sim" : "<i class='fas fa-times-circle mr-2 text-danger'></i>Não"} </span></div>
                            </div>
                        </li>
                    </ul>`;
        }
        $(document).ready(function() { 
            //Carrega a datatables
            var table = $('#atrDataTable').DataTable({
                order: [1, 'asc'],
                deferRender: true,
                createdRow: function( row, data, dataIndex ) {
                    $('td:last-child', row).html(actionsButtons(data['id']));
                },
                ajax: {
                    
                    "url": "{{ route('getAttributes') }}",
                    "dataSrc": "atributos"
                },
                columns: [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '<i class="fas fa-chevron-up down"></i>'
                    },
                    { "data": "nome" },
                    { "data": "subcategoria" },
                    { "data": "tipo"},
                    {
                        "className": 'text-center',
                        "orderable":      false,
                        "data":           null,
                    },
                ],
                initComplete: function () {
                    this.api().columns([2,3]).every( function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
        
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );
        
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                },
            });
            $('#atrDataTable tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
        
                if ( row.child.isShown() ) {

                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    $(this).find('.fa-chevron-up').addClass('down')
                }
                else {
                    // Open this row
                    row.child( rowFormat(row.data()) ).show();
                    tr.addClass('shown');
                    $(this).find('.fa-chevron-up').removeClass('down')
                }
            } );
        });
        
    </script>
@endsection
