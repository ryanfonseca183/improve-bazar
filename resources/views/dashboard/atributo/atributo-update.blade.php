@extends('layouts.dashboard')

@section('title', 'Atributos')

@section('icon')
    <i class="fas fa-tint"></i>
@endsection

@section('desc', 'Cadastre, atualize ou remova os atributo das subcategorias de produtos ofertados na loja.')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 bg-transparent">
        <li class="breadcrumb-item"><a href="{{ route('atributo.index') }}">Atributos</a></li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('atributo.show', [$atributo->id]) }}">{{ $atributo->nome }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Atualizar</li>
        </ol>
    </nav>
    
    <div class="row justify-content-center my-5">
        <!--Formulário de atualização do atributo-->
        <div class="col-lg-10 col-xl-9">
            <div class="card content-card">
                <h1 class="h5 mb-4">
                    Editar atributo
                </h1>
                <form method="POST" action="{{ route('atributo.update', $atributo->id) }}" id="updateAtr">
                    @csrf
                    @method('PUT')
                    <div id="carousel" class="carousel" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            @if($errors->any())
                                <div class="alert alert-danger mt-3" role="alert">
                                    Alguns dados são inválidos. Por favor, preencha novamente!
                                </div>
                            @enderror
                            @php $subcategoria = $atributo->subcategoria; $categoria = $subcategoria->categoria; @endphp
                            <x-choose-categorie :categorias="$categorias" :selected="['categoria' => $categoria, 'subcategoria' => $subcategoria]"></x-choose-categorie>
        
                            <!--Nome e tipo de dados-->
                            <div class="carousel-item active">
                                <x-card card-title="Dados do atributo">
                                    <x-slot name="card_body">
                                        
                                            <input type="hidden" name="subcategoria" id="subcatInput" value="{{ $atributo->subcategorias_id }}">
        
                                            <div class="form-row mb-3">
                                                <!--Nome do atributo-->
                                                <div class="form-group col-md-6" id="nameGroup">
                                                    <label for="nome">Nome</label>
                                                    <input 
                                                    type="text" 
                                                    class="form-control @error('nome') is-invalid @enderror mb-2" 
                                                    name="nome" 
                                                    id="nome" 
                                                    required
                                                    maxlength="60" 
                                                    value="{{ $atributo->nome }}" />
                                                    <span class="invalid-feedback" id="info_nome">
                                                        <strong>
                                                            @error('nome')
                                                                {{$message}}
                                                            @enderror
                                                        </strong>
                                                    </span>                                            
                                                </div>
        
                                                <!--Tipo de dados-->
                                                <div class="form-group col-md-6">
                                                    <!--Ajuda-->
                                                    <div class="form-row">
                                                        <div class="col-auto">
                                                            <label for="tipoAtr">
                                                                Tipo de dados  
                                                            </label>
                                                        </div>
                                                        <div class="col-auto">
                                                            <a tabindex="0" class="pop-button" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="O tipo do atributo determina os valores aceitos no momento de cadastro dos produtos." title="Tipo de dados">
                                                                <i class="fas fa-question-circle text-info"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    
                                                    <select class="form-control" name="tipoAtr" id="tipoAtr">
                                                        @foreach($tiposAtributo as $tipo)
                                                            @if($tipo->id == $atributo->tipos_atributos_id)
                                                                <option selected value="{{ $tipo->id }}">{{ $tipo->frontend_type_label }}</option>
                                                            @else 
                                                                <option value="{{ $tipo->id }}">{{ $tipo->frontend_type_label }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <!--É visível-->
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch mb-1">
                                                            <input 
                                                            type="checkbox" 
                                                            class="custom-control-input" 
                                                            name="is_visible" 
                                                            id="is_visible"
                                                            @if($atributo->is_visible == 1) checked @endif />
                                                            <label class="custom-control-label" for="is_visible">Visível</label>
                                                        </div>
                                                        <p class="text-gray-600 small">O atributo será exibido no momento de cadastro dos produtos.</p>
                                                    </div>
                                                    <!--É obrigatório-->
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch mb-1">
                                                            <input 
                                                            type="checkbox" 
                                                            class="custom-control-input" 
                                                            name="is_required" 
                                                            id="is_required"
                                                            @if($atributo->is_required == 1) checked @endif />
                                                            <label class="custom-control-label" for="is_required">Obrigatório</label>
                                                        </div>
                                                        <p class="text-gray-600 small">Será necessário selecionar ou definir um valor para o atributo no cadastro dos produtos.</p>
                                                    </div>
                                                    <!--É configurável-->
                                                    <div class="form-group" id="atrIsConfig">
                                                        <div class="custom-control custom-switch mb-1">
                                                            <input 
                                                            id="is_configurable"
                                                            type="checkbox" 
                                                            class="custom-control-input" 
                                                            @if($atributo->is_configurable == 1) checked @endif
                                                            disabled />
                                                            <label class="custom-control-label" for="is_configurable">Configurável</label>
                                                        </div>
                                                        <p class="text-gray-600 small">Voce poderá selecionar ou definir mais de um valor para o atributo, criando variações do produto.</p>
                                                        <div class="alert alert-warning mb-0">
                                                            <i class="fas fa-exclamation-circle mr-2 text-warning" style="font-size: 13pt;"></i> Não é possível alterar a natureza do atributo
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </x-slot>
                                    <x-slot name="card_footer">
                                        <button type="button" onclick="checkGroupValidity('nameGroup', 'submit', 'updateAtr')" class="btn btn-success float-right" >Salvar</button>
                                    </x-slot>
                                </x-card>
                            </div>
                        </div>
        
                        <!--Controles-->
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('pl-custom-scripts')
    <!-- Page level custom scripts -->
    <script src="{{ asset('js/custom/dashboard/create.js') }}"></script>

    <script>
         //Verifica se o tipo do atributo é lógico e esconde o campo "Multiplos valores"
         function showConfigurable(val) {
            if(val == "3") {
                $('#atrIsConfig').addClass('d-none');
            } else {
                $('#atrIsConfig').removeClass('d-none');
            }
        }
        $('#tipoAtr').change(function(){
            showConfigurable(this.value);
        })
        function getSubcategorias(id) {
            $.ajax({
                url: '{{ route("categoria.getSubcategorias") }}',
                method: 'GET',
                datatype: "JSON",
                data: {
                    categoria: id,
                },
                beforeSend: function() {
                    $('#carousel').addClass('carousel-sliding');
                },
                success: function(response) {
                    var subcategorias = response.subcategorias;
                    $('#list-subcat .list-group-item').remove();

                    //Carrega as subcategorias
                    for(var i = 0; i < subcategorias.length; i++) {
                        let option = 
                        '<li '+
                            'class="list-group-item" '+
                            'onclick="loadLiValue(this, null);" '+
                            'data-submit="next" '+
                            'data-parent="list-subcat" '+ 
                            'data-option-value="subcatInput/'+ subcategorias[i].id +'">'+
                                '<span>'+ subcategorias[i].nome +'</span>'+
                                '<i class="fas fa-long-arrow-alt-right"></i>'+
                        '</li>';
                        $('#list-subcat').append(option);
                    }
                    $('#carousel').carousel('next');
                },
                error: function(xhr, status, code) {
                    console.log(xhr);
                },
                complete: function() {
                    $('#carousel').removeClass('carousel-sliding');
                }
            })
        }
    </script>
@endsection
