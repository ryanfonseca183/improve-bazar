    @extends('layouts.dashboard')

    @section('title', 'Atributos')

    @section('icon')
        <i class="fas fa-tint"></i>
    @endsection

    @section('desc', 'Cadastre, atualize ou remova os atributos das subcategorias de produtos ofertados na loja.')

    @section('content')
    
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb px-0 bg-transparent">
            <li class="breadcrumb-item"><a href="{{ route('atributo.index') }}">Atributos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Cadastrar</li>
            </ol>
        </nav>
        
        <div class="row justify-content-center my-5">
            @if(isset($categorias))
                <!--Cadastro dos atributos-->
                <div class="col-lg-10 col-xl-9">
                    <div class="card content-card">
                        <h1 class="h5 mb-4">
                            Novo atributo
                        </h1>
                        <form method="POST" action="{{ route('atributo.store') }}" id="cadAtr">
                            @csrf
                            <div id="carousel" class="carousel" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    @if($errors->any())
                                        <div class="alert alert-danger">
                                            Alguns campos são inválidos. Por favor, tente novamente!
                                        </div>
                                    @endif
                                    <x-choose-categorie :categorias="$categorias"></x-choose-categorie>
                                    <!--Nome e tipo de dados-->
                                    <div class="carousel-item">
                                        <x-card card-title="Dados do atributo">
                                            <x-slot name="card_body">
                                                <!--Campos-->
                                                <div class="form-row">
                                                    <!--Nome do atributo-->
                                                    <div class="form-group col-md-6" id="nameGroup">
                                                        <label for="nome">Nome</label>
                                                        <input 
                                                        type="text" 
                                                        class="form-control @error('nome') is-invalid @enderror mb-2" 
                                                        name="nome" 
                                                        id="nome" 
                                                        required 
                                                        maxlength="60" />
                                                        <span class="invalid-feedback" id="info_nome">
                                                            <strong>
                                                                @error('nome')
                                                                    {{$message}}
                                                                @enderror
                                                            </strong>
                                                        </span>
                                                    </div>

                                                    <!--Tipo de dados-->
                                                    <div class="form-group col-md-6">
                                                        <div class="form-row">
                                                            <div class="col-auto">
                                                                <label for="tipoAtr">
                                                                    Tipo de dados  
                                                                </label>
                                                            </div>
                                                            <div class="col-auto">
                                                                <a tabindex="0" class="pop-button" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="O tipo do atributo determina os valores aceitos no momento de cadastro dos produtos." title="Tipo de dados">
                                                                    <i class="fas fa-question-circle text-info"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    
                                                        <select class="form-control" name="tipoAtr" id="tipoAtr">
                                                            @foreach($tiposAtributos as $tipo)
                                                                <option value="{{ $tipo->id }}">{{ $tipo->frontend_type_label }}</option> 
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col">
                                                        <!--É visível-->
                                                        <div class="form-group">
                                                            <div class="custom-control custom-switch mb-1">
                                                                <input 
                                                                type="checkbox" 
                                                                class="custom-control-input" 
                                                                name="is_visible" 
                                                                id="is_visible"
                                                                checked/>
                                                                <label class="custom-control-label" for="is_visible">Visível</label>
                                                            </div>
                                                            <p class="text-gray-600 small">O atributo será exibido no momento de cadastro dos produtos.</p>
                                                        </div>
                                                        <!--É obrigatório-->
                                                        <div class="form-group">
                                                            <div class="custom-control custom-switch mb-1">
                                                                <input 
                                                                type="checkbox" 
                                                                class="custom-control-input" 
                                                                name="is_required" 
                                                                id="is_required"
                                                                checked/>
                                                                <label class="custom-control-label" for="is_required">Obrigatório</label>
                                                            </div>
                                                            <p class="text-gray-600 small">Será necessário selecionar ou definir um valor para o atributo no cadastro dos produtos.</p>
                                                        </div>
                                                        <!--É configurável-->
                                                        <div class="form-group d-none" id="atrIsConfig">
                                                            <div class="custom-control custom-switch mb-1">
                                                                <input 
                                                                type="checkbox" 
                                                                class="custom-control-input" 
                                                                name="is_configurable" 
                                                                id="is_configurable"/>
                                                                <label class="custom-control-label" for="is_configurable">Configurável</label>
                                                            </div>
                                                            <p class="text-gray-600 small">Voce poderá selecionar ou definir mais de um valor para o atributo, criando variações do produto.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </x-slot>
                                            <x-slot name="card_footer">
                                                <button type="button" onclick="checkGroupValidity('nameGroup', 'submit', 'cadAtr')" class="btn btn-success float-right" >Finalizar</button>
                                            </x-slot>
                                        </x-card>
                                    </div>
                                </div>
        
                                <!--Controles-->
                                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    @endsection
    
    @section('pl-custom-scripts')
        <!-- Page level custom scripts -->
        <script src="{{ asset('js/custom/dashboard/create.js') }}"></script>
        
        <script>
            $(document).ready(function () { 
                showConfigurable($('#tipoAtr').val());
            })

            //Verifica se o tipo do atributo é lógico e esconde o campo "Configurável"
            function showConfigurable(val) {
                if(val == "3") {
                    $('#atrIsConfig').addClass('d-none');
                    $('#atrIsConfig').prop('checked', false)
                } else {
                    $('#atrIsConfig').removeClass('d-none');
                }
            }
            $('#tipoAtr').change(function(){
                showConfigurable(this.value);
            })            
            function getSubcategorias(id) {
                $.ajax({
                    url: '{{ route("categoria.getSubcategorias")}}',
                    method: 'GET',
                    datatype: "JSON",
                    data: {
                        categoria: id,
                    },
                    beforeSend: function() {
                        $('#carousel').addClass('carousel-sliding');
                    },
                    success: function(response) {
                        var subcategorias = response.subcategorias;
                        $('#subcatList .list-group-item').remove();

                        //Carrega as subcategorias
                        for(var i = 0; i < subcategorias.length; i++) {
                            let option = `
                                <li 
                                onclick="loadLiValue(this, null)"
                                class="list-group-item"
                                data-submit="next"
                                data-parent="subcatList"
                                data-option-value="subcatInput/${subcategorias[i].id}">
                                    <span>${subcategorias[i].nome}</span>
                                    <i class="fas fa-long-arrow-alt-right"></i>
                                </li>
                            `;
                            $('#subcatList').append(option);
                        }
                        $('#carousel').carousel('next');
                    },
                    error: function(xhr, status, code) {
                        console.log(xhr);
                    },
                    complete: function() {
                        $('#carousel').removeClass('carousel-sliding');
                    }
                })
            }
        </script>
    @endsection
