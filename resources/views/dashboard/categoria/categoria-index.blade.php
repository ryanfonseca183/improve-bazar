@extends('layouts.dashboard')

@section('title', 'Categorias')

@section('icon')
    <i class="fas fa-list"></i>
@endsection

@section('pl-custom-styles')
<style>
ul, #tree-view {
  list-style-type: none !important;
}
#tree-view {
    margin: 0;
    padding: 0;
}
#root-list {
  display:none;
}
#root-list > li:last-child::after {
    content: "";
    position: absolute;
    top: 1.40rem;
    left: 0;
    transform: translate(-100%);
    width: 40px;
    height: 100%;
    background:white;
}
.tree-grid {
    position: relative;
}
.tree-grid.nested {
    display:none;
}

/* TRAÇO VERTICAL  */
.tree-grid::before {
    content:"";
    position: absolute;
    top: -0.5rem;
    left: 20px;
    width: 2px;
    height: calc(100% - 24px + 0.5rem);
    background-color: #f2f2f2;
}
.tree-grid-item{
    margin-bottom: 0.5rem;
    border-radius: 0.25rem;
    border: 1px solid #f2f2f2;
    display:flex;
    align-items:center;
    padding: 0.5rem 1rem;
}
.toggle-actions-menu {
    opacity: 0;
    padding: 0 0.5rem;
}

.tree-grid-item:hover .toggle-actions-menu, .toggle-actions-menu:focus {
    opacity: 1;
}

.tree-grid.nested .tree-grid-item:last-child {
    margin-bottom: 2rem;
}
.tree-grid-item.toggle {
    position: relative;
    padding-left: 30px;
}
.tree-grid-indicator {
    cursor: pointer;
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    width: 30px;
    height: 30px;
    text-align: center;
    line-height: 30px;
}
.grid-down .tree-grid-indicator {
    color: var(--laranja);
  transform: translateY(-50%) rotate(90deg);
}
.tree-grid-text {
    flex: 1;
}

/*  TRAÇO HORIZONTAL */
.tree-grid li {
    position: relative;
}
.tree-grid li::before {
    content: "";
    position: absolute;
    top: 20px;
    left: 0;
    transform: translate(-100%, -50%);
    width: 20px;
    height: 2px;
    background-color: #f2f2f2;
}



#new-category {
    background: #f8f9fc !important;
}
.new-category-input {
    background: transparent;
    border: 0;
    flex: 1;
    outline: none !important;
    margin-right: 1rem;
}
.new-category-button {
    background: transparent;
    border: none;
}
#info_category_name {
    position: absolute;
    bottom: 0;
    transform: translateY(130%);
}
.was-validated .new-category-input:invalid, .new-category-input.is-invalid {
    border-bottom: 2px solid #e74a3b !important;
}
.toast {
    position: fixed;
    top: 10px;
    left: 50%;
    transform: translateX(-50%);
    z-index: 100;
}
.default-item {
    display:none;
}
.default-item {
    padding-top: 0.6rem;
    padding-bottom: 0.6rem;
}
.default-item:only-child {
    display:block;
}
</style>
@endsection

@section('desc', 'Cadastre, atualize ou remova as categorias dos produtos ofertados na loja.')
@section('content')
    {{-- NOTIFICAÇÃO DE SUCESSO --}}
    <div class="toast toast-success" data-delay="2000">
        <div class="toast-header bg-success text-light">
            <strong class="mr-auto">Improve Bazar</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body"></div>
    </div>

    {{-- NOTIFICAÇÃO DE ERRO --}}
    <div class="toast toast-error" data-delay="2000">
        <div class="toast-header bg-danger text-light">
            <strong class="mr-auto">Improve Bazar</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body"></div>
    </div>

    <div class="row my-4 justify-content-center my-5">
        
        <div class="col-lg-10 mb-5">
            
            {{-- LISTAGEM DE CATEGORIAS --}}
            <div class="card content-card">
                <h2 class="h5 mb-4">Categorias cadastradas</h2>
                <div class="flex-between mb-3" data-list="root-list">
                    <button type="button" onclick="newCategory(this)" class="btn btn-link"><i class="fas fa-plus mr-2"></i>Nova categoria</button>
                    <div class="btn-group" role="group">
                        <button type="button" onclick="expandAll()" class="btn btn-outline-primary">Expandir</button>
                        <button type="button" onclick="collapseAll()" class="btn btn-outline-primary">Esconder</button>
                    </div>
                </div>
                <ul id="tree-view">
                    <li>
                        <span class="tree-grid-item toggle" id="root-toggle">
                            <i class="fas fa-caret-right tree-grid-indicator"></i>
                            <span class="tree-grid-text">Categorias</span>
                        </span>
                        <ul class="tree-grid" id="root-list">
                            @foreach ($categorias as $key => $cat)
                                <li data-id="{{ $cat->id }}">
                                    <span class="tree-grid-item toggle">
                                        <i class="fas fa-caret-right tree-grid-indicator"></i>
                                        <span class="tree-grid-text">{{ $cat->nome }}</span>
                                        <div class="dropdown dropleft">
                                            <button class="btn toggle-actions-menu" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>
                                            <div class="dropdown-menu shadow-sm" data-list="list_{{ $cat->id }}" data-nome="{{ $cat->nome }}" data-id="{{ $cat->id }}">
                                                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalUpdate">Editar</button>
                                                <button type="button" data-toggle="modal" data-target="#modalDelete" class="dropdown-item">Excluir</button>
                                                <button class="dropdown-item" onclick="newCategory(this)" type="button">Adicionar subcategoria</button>
                                            </div>
                                        </div>
                                    </span>
                                    <ul class="tree-grid nested" id="list_{{ $cat->id }}">
                                        <li class="tree-grid-item default-item">
                                            <span class="tree-grid-text">Nenhuma subcategoria cadastrada</span>
                                        </li>
                                        @foreach($cat->subcategorias as $subcat)
                                            <li class="tree-grid-item" data-id="{{ $subcat->id }}">
                                                <span class="tree-grid-text">{{ $subcat->nome }}</span>
                                                <div class="dropdown dropleft">
                                                    <button class="btn toggle-actions-menu" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>
                                                    <div class="dropdown-menu shadow-sm" data-nome="{{ $subcat->nome }}" data-id="{{ $subcat->id }}">
                                                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalUpdate">Editar</button>
                                                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalDelete">Excluir</button>
                                                        <a href="{{ route('produto.create', ['subcategoria' => $subcat->id]) }}" class="dropdown-item">Adicionar produto</a>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            {{-- MODAL DE ATUALIZAÇÃO--}}
            <div class="modal" tabindex="-1" role="dialog" id="modalUpdate">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body p-4">
                            <h3 class="h4 mb-4">Atualizar <span id="update_category_nome"></span></h3>
                            <div class="form-group">
                                <label for="nome">Novo nome</label>
                                <input type="hidden" id="update_category_id">
                                <input 
                                type="text" 
                                class="form-control" 
                                required 
                                maxlength="60"
                                data-info="nome"
                                id="update_nome">
                                <span class="invalid-feedback" id="info_update_nome">
                                    <strong>
                                       Digite um nome válido
                                    </strong>
                                </span>
                            </div>
                            <p class="m-0 text-gray-600" style="font-weight:600;">Lembre-se:</p>
                            <ul class="list-unstyled text-muted small mb-4">
                                <li>O nome deve ter no máximo 60 caracteres</li>
                                <li>Deve conter apenas letras e espaços.</li>
                                <li>Deve ser único</li>
                            </ul>
                            <button type="button" class="btn btn-lg btn-success">Atualizar</button>
                            <button type="button" class="btn btn-lg btn-outline-secondary ml-md-2" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- MODAL DE DELETE--}}
            <div class="modal fade" tabindex="-1" role="dialog" id="modalDelete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content py-4">
                        <div class="modal-body">
                            <div class="row justify-content-center">
                                <div class="col-auto mb-3 modal-icon bg-white shadow-sm">
                                    <i class="fas fa-exclamation-circle text-reddish"></i>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-sm-10 mb-4 text-justify">
                                    <h3 class="h4 mb-4 text-gray-800 text-center">Atenção</h3>
                                    <p class="modal-text"></p>
                                </div>
                                <div class="w-100"></div>
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <div class="col-4">
                                            <button type="button" class="btn btn-block btn-lg btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                        <div class="col-4">
                                            <input type="hidden" id="delete_category_id">
                                            <button type="button" class="btn btn-block btn-lg btn-danger">Excluir</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pl-custom-scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function(){
            $('.toast').toast()
            $("#root-list").toggle(400);
            $("#root-toggle").toggleClass("grid-down");
            makeToggable($(".tree-grid-indicator"));
            makeSortable($(".tree-grid.nested"));
        })
        function expandAll() {
            $(".tree-grid").toggle(true);
            $(".tree-grid-item.toggle").addClass('grid-down');
        }
        function collapseAll() {
            $(".tree-grid.nested").toggle(false);
            $(".tree-grid-item.toggle:not(#root-toggle)").removeClass('grid-down');
        }
        var dropdownMenu = "";
        $('#modalDelete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            dropdownMenu = button.parent(); 

            var dataList = dropdownMenu.attr('data-list');
            
            btnDelete = $(this).find('.btn-danger');
            //REMOVE OS EVENTOS DE CLICK
            btnDelete.off('click');

            //ATRIBUI UM NOVO EVENTO DEPENDENDO DO RECURSO
            if(dataList != undefined) {
                btnDelete.click(deleteCategory);
                $(this).find('.modal-text').html("Ao deletar essa categoria, todas as subcate&shy;gorias e seus respec&shy;tivos atribu&shy;tos e produtos também serão dele&shy;tados.")
            } else {
                btnDelete.click(deleteSubcategory);
                $(this).find('.modal-text').html("Ao deletar essa subcategoria, todos os atribu&shy;tos e produtos relacionados também serão dele&shy;tados.")
            }
            var id = dropdownMenu.attr('data-id');
            $(this).find('#delete_category_id').val(id);
        })
        $('#modalUpdate').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            dropdownMenu = button.parent(); 
            var nome = dropdownMenu.attr('data-nome');
            var id = dropdownMenu.attr('data-id');

            var dataList = dropdownMenu.attr('data-list');
            var btnEdit =  $(this).find('.btn-success');
            //REMOVE EVENTOS DE CLICK
            btnEdit.off('click');

            //ATRIBUI UM NOVO EVENTO DEPENDENDO DO RECURSO
            if(dataList != undefined)
                btnEdit.click(updateCategory);
            else
                btnEdit.click(updateSubcategory);

            $(this).find('#update_category_nome').html(nome);
            $(this).find('#update_category_id').val(id);
        })
        var lastParent = "";
        //CRIA UM INPUT PARA CADASTRAR A CATEGORIA
        function newCategory(invoker) {
            let dropdownMenu = invoker.parentNode;
            let parentId = dropdownMenu.getAttribute('data-list');
            parent = $("#" + parentId);
            //EXPANDE A GRADE SELECIONADA
            if(parent.css('display') == "none") {
                parent.toggle('400');
                parent.prev().toggleClass('grid-down');
            }
            //VERIFICA SE A GRADE ATUAL É IGUAL A GRADE ANTERIOR
            if(lastParent != "" && parentId == lastParent) {
                $("#category_name").focus();
                return false;
            }
            lastParent = parentId;
            
            //CRIA O ELEMENTO NA NOVA GRADE
            if($("#new-category").length != 0) $("#new-category").remove();
            var newCategory = document.createElement('li');
            newCategory.classList.add('tree-grid-item')
            newCategory.id = "new-category";
            newCategory.innerHTML = 
                `<input type="text" class="new-category-input" required maxlength="60" id="category_name">
                <span class="invalid-feedback" id="info_category_name"><strong>Digite um nome válido</strong></span>
                <div>
                    <button class="new-category-button text-success">
                        <i class="fas fa-check"></i>
                    </button>
                    <button onclick="cancelCategory()" class="new-category-button text-danger">
                        <i class="fas fa-times"></i>
                    </button>
                </div>`;
            //DEFINE OS EVENTOS DE CLICK PARA OS BOTÕES DE AÇÕES
            let saveButton = newCategory.querySelector('.new-category-button.text-success');
            let inputCategory = newCategory.querySelector('.new-category-input');
            if(parentId != "root-list") {
                inputCategory.setAttribute('data-parent', parentId);
                saveButton.onclick = storeSubcategory;
            } else {
                saveButton.onclick = storeCategory;
            }

            //ATRIBUI O NOVO ITEM À GRADE
            parent.append(newCategory);
            $(document.body).scrollTop(parent.offset().top);
            $("#category_name").focus();
        }
        //DELETA O INPUT PARA CADASTRAR A CATEGORIA 
        function cancelCategory() {
            $("#new-category").remove();
            lastParent = "";
        }
        
        //CATEGORIAS DE PRODUTOS
        function storeCategory() {
            $("#new-category").first().addClass("was-validated");
            var categoryName = document.getElementById('category_name');
            if(!categoryName.checkValidity()) return false;
            let value = categoryName.value;
           
            $.ajax({
                url: '{{ route("categoria.store") }}',
                method: 'POST',
                datatype: "JSON",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: {
                    nome: value,
                },
                success: function(response) {
                    cancelCategory();
                    $(".toast-success .toast-body").html('Categoria <strong>cadastrada</strong> com sucesso!');
                    $('.toast-success').toast('show')
                    let categoria = response.categoria;
                    let li = document.createElement('li');
                    li.setAttribute('data-id', categoria.id);
                    li.innerHTML =
                    `<span class="tree-grid-item toggle">
                        <i class="fas fa-caret-right tree-grid-indicator"></i>
                        <span class="tree-grid-text">${categoria.nome}</span>
                        <div class="dropdown dropleft">
                            <button class="btn toggle-actions-menu" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>
                            <div class="dropdown-menu shadow-sm" data-list="list_${categoria.id}" data-nome="${categoria.nome}" data-id="${categoria.id}">
                                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalUpdate">Editar</button>
                                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalDelete">Excluir</button>
                                <button class="dropdown-item" onclick="newCategory(this)" type="button">Adicionar subcategoria</button>
                            </div>
                        </div>
                    </span>
                    <ul class="tree-grid nested" id="list_${categoria.id}">
                        <li class="tree-grid-item default-item">
                            <span class="tree-grid-text">Nenhuma subcategoria cadastrada</span>
                        </li>
                    </ul>`; 
                    makeSortable(li.querySelector('.tree-grid.nested'));
                    makeToggable(li.querySelector('.tree-grid-indicator'));

                    $("#root-list").append(li);
                },
                error: function(xhr) {
                    if(xhr.status = 422) {
                        let response = JSON.parse(xhr.responseText);
                        $("#new-category").removeClass("was-validated");
                        $("#category_name").addClass("is-invalid");
                        $("#info_category_name strong").html(response.errors.nome);
                    }
                },
            })
        }
        function deleteCategory() {
            let input = $("#delete_category_id");
            $.ajax({
                url: '/dashboard/categoria/' + input.val(),
                method: 'DELETE',
                datatype: "JSON",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response) {
                    $('li[data-id="'+input.val()+'"]').remove();
                    $(".toast-success .toast-body").html('Categoria <strong>deletada</strong> com sucesso!');
                    $('.toast-success').toast('show')
                },
                error: function(xhr, status, code) {
                    $(".toast-error .toast-body").html("Falha ao deletar a categoria. Reinicie a página e tente novamente");
                    $(".toast-error").toast("show");
                },
                complete: function() {
                    $('#modalDelete').modal('hide');
                }
            })
        }
        function updateCategory() {
            $("#modalUpdate .modal-body").addClass("was-validated");
            var categoryName = document.getElementById('update_nome');
            if(categoryName.checkValidity()) {
                let id = $("#update_category_id").val();
                $.ajax({
                    url: '/dashboard/categoria/' + id,
                    method: 'PUT',
                    datatype: "JSON",
                    data: {
                        nome: categoryName.value,
                        _token: '{{ csrf_token() }}',
                    },
                    beforeSend: function() {
                    },
                    success: function(response) {
                        $("#modalUpdate").modal('hide');
                        $('li[data-id="'+id+'"] > .tree-grid-item .tree-grid-text').html(response.categoria.nome);
                        dropdownMenu.attr('data-nome', response.categoria.nome);
                        $(".toast-success .toast-body").html('Nome <strong>alterado</strong> com sucesso!');
                        $('.toast-success').toast('show')
                        $("#info_update_nome strong").html("Digite um nome válido");
                    },
                    error: function(xhr, status, code) {
                        if(xhr.status == 422) {
                            let response = JSON.parse(xhr.responseText);
                            $("#update_nome").addClass('is-invalid');
                            $("#info_update_nome strong").html(response.errors.nome);
                        }
                    },
                    complete: function() {
                        $("#modalUpdate .modal-body").removeClass("was-validated");
                        $("#update_nome").val('');
                    },
                })               
            }
        }

        //SUBCATEGORIAS DE PRODUTOS
        function storeSubcategory() {
            $("#new-category").first().addClass("was-validated");
            var categoryName = document.getElementById('category_name');
            if(!categoryName.checkValidity()) return false;

            let parent = categoryName.getAttribute('data-parent');
            let index = parent.indexOf('_');
            let id = parent.substr(++index);
            let value = categoryName.value;

            $.ajax({
                url: '{{ route("subcategoria.store") }}',
                method: 'POST',
                datatype: "JSON",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: {
                    categoria: id,
                    nome: value,
                },
                beforeSend: function() {
                },
                success: function(response) {
                    cancelCategory();
                    $(".toast-success .toast-body").html('Subcategoria <strong>cadastrada</strong> com sucesso!');
                    $('.toast-success').toast('show')
                    let subcategoria = response.subcategoria;
                    let li = document.createElement('li');
                    li.setAttribute('data-id', subcategoria.id);
                    li.classList.add("tree-grid-item");
                    li.innerHTML = 
                    `<span class="tree-grid-text">${subcategoria.nome}</span>
                    <div class="dropdown dropleft">
                        <button class="btn toggle-actions-menu" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>
                        <div class="dropdown-menu shadow-sm" data-nome="${subcategoria.nome}" data-id="${subcategoria.id}">
                            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalUpdate">Editar</button>
                            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalDelete">Excluir</button>
                            <a href="/dashboard/produto/create?subcategoria=${subcategoria.id}" class="dropdown-item">Adicionar produto</a>
                        </div>
                    </div>`;
                    $("#" + parent).append(li);
                },
                error: function(xhr) {
                    if(xhr.status = 422) {
                        let response = JSON.parse(xhr.responseText);
                        $("#new-category").removeClass("was-validated");
                        $("#category_name").addClass("is-invalid");
                        $("#info_category_name strong").html(response.errors.nome);
                    }
                },
                complete: function() {

                },
            })  
        }
        function deleteSubcategory() {
            let input = $("#delete_category_id");
            $.ajax({
                url: '/dashboard/subcategoria/' + input.val(),
                method: 'DELETE',
                datatype: "JSON",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response) {
                    $('li[data-id="'+input.val()+'"]').remove();
                    $(".toast-success .toast-body").html('Subcategoria <strong>deletada</strong> com sucesso!');
                    $('.toast-success').toast('show')
                },
                error: function(xhr, status, code) {
                    $(".toast-error .toast-body").html("Falha ao deletar a subcategoria. Reinicie a página e tente novamente");
                    $(".toast-error").toast("show");
                },
                complete: function() {
                    $('#modalDelete').modal('hide');
                }
            })
        }
        function updateSubcategory() {
            $("#modalUpdate .modal-body").addClass("was-validated");
            var categoryName = document.getElementById('update_nome');
            if(categoryName.checkValidity()) {
                let id = $("#update_category_id").val();
                $.ajax({
                    url: '/dashboard/subcategoria/' + id,
                    method: 'PUT',
                    datatype: "JSON",
                    data: {
                        nome: categoryName.value,
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(response) {
                        $("#modalUpdate").modal('hide');
                        $('li[data-id="'+id+'"] .tree-grid-text').html(response.subcategoria.nome);
                        dropdownMenu.attr('data-nome', response.subcategoria.nome);
                        $(".toast-success .toast-body").html('Nome da subcategoria <strong>alterado</strong> com sucesso!');
                        $('.toast-success').toast('show');
                        $("#update_nome").removeClass('is-invalid');
                        $("#info_update_nome strong").html("Digite um nome válido");
                    },
                    error: function(xhr, status, code) {
                        if(xhr.status == 422) {
                            let response = JSON.parse(xhr.responseText);
                            $("#update_nome").addClass('is-invalid');
                            $("#info_update_nome strong").html(response.errors.nome);
                        }
                    },
                    complete: function() {
                        $("#modalUpdate .modal-body").removeClass("was-validated");
                        $("#update_nome").val('');
                    },
                })               
            }
        }
        function updateSubcategoryParent(subcategory_id, category_id) {
            $.ajax({
                    url: '/dashboard/subcategoria/' + subcategory_id + '/categoria',
                    method: 'PUT',
                    datatype: "JSON",
                    data: {
                        categoria: category_id,
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(response) {
                        $(".toast-success .toast-body").html('Subcategoria <strong>movida</strong> com sucesso!');
                        $('.toast-success').toast('show');
                    },
                    error: function(xhr, status, code) {
                        $(".toast-error .toast-body").html("Falha ao mover a subcategoria. Reinicie a página e tente novamente");
                        $(".toast-error").toast("show");
                    },
                })
        }


        function makeSortable(ul) {
            if(ul.length == undefined) {
                lists = $(ul);
            } else {
                lists = ul;
            }
            lists.sortable({
                revert: true,
                cancel: ".default-item, #new-category",
                connectWith: '.tree-grid.nested',
                cursor: 'move',
                receive: function(event, ui) {
                    let item = ui.item;
                    let listID = item.parent().prop('id');
                    let start = listID.indexOf("_");
                    let categoryId = listID.substr(++start);
                    updateSubcategoryParent(item.attr("data-id"), categoryId);
                }
            });
        }
        function makeToggable(ind) {
            if(ind.length == undefined) {
                inds = $(ind);
            } else {
                inds = ind;
            }
            inds.click(function(){
                parent =  $(this).parent();
                parent.toggleClass('grid-down');
                parent.next().toggle(400);
            })
        } 
    </script>
@endsection
