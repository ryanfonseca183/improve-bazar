@extends('layouts.dashboard')

@section('title', 'Produtos')

@section('pl-custom-styles')
    <!-- Custom styles for this page -->
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection


@section('icon')
    <i class="fas fa-mobile-alt"></i>
@endsection

@section('desc', 'Cadastre, atualize ou remova os produtos ofertados na loja.')

@section('content')
    <div class="row my-4 justify-content-center my-5">
        <div class="col-lg-10 mb-5">
            <div class="card content-card">
                <div class="flex-between mb-5">
                    <div>
                        <h2 class="h5 mb-0">Produtos cadastrados</h2>
                        <p class="mb-0">Use o campo de busca para pesquisar por um nome, SKU ou outro valor</p>
                    </div>
                    <a href="{{ route('produto.create') }}" class="btn btn-primary float-right"><i class="fas fa-plus mr-2"></i>Novo</a>
                </div>
                <div class="table-responsive">
                    <table class="table " id="productDataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Nome</th>
                                <th>SKU</th>
                                <th>Subcategoria</th>
                                <th>Visível</th>
                                <th class="small">Estoque</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produtos as $prod)
                                <tr>
                                    <td>
                                        <div class="listable-product-image img-xs thumb">
                                            <img src="{{ asset('storage/' . $prod->fotosProduto()->where('is_main', 1)->first()->caminho) }}" alt="" class="img-fluid">
                                        </div>
                                    </td>
                                    <td><a href="{{ route('produto.show', [$prod->id]) }}">{{ $prod->nome }}</a></td>
                                    <td>
                                        @if($prod->filhos()->exists())
                                            @foreacH($prod->filhos as $filho)
                                                {{$filho->sku}}<br>
                                            @endforeach
                                        @else 
                                            {{ $prod->sku }}
                                        @endif
                                    </td>
                                    <td>{{ $prod->subcategoria->nome }}</td>
                                    <td>
                                        @if($prod->is_visible) Sim @else Não @endif
                                    </td>
                                    <td>
                                        @if($prod->is_avaliable) 
                                            <span class="badge badge-success">Disponível</span> 
                                        @else 
                                            <span class="badge badge-danger">Indisponível</span> 
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-action"  data-toggle="modal" data-target="#modalDelete" data-id="{{$prod->id}}">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div> 

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content py-4">
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-auto mb-3 modal-icon bg-white shadow-sm">
                            <i class="fas fa-exclamation-circle text-reddish"></i>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-sm-10 mb-4 text-justify">
                            <h3 class="h4 mb-4 text-gray-800 text-center">Atenção</h3>
                            <p>Ao deletar esse produto, ele não será mais listado no painel de controle ou catálogo da loja, mas ainda poderá ser visto nos pedidos dos usuários. Deseja continuar?</p>
                        </div>
                        <div class="w-100"></div>
                        <div class="col">
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <button type="button" class="btn btn-block btn-lg btn-secondary" data-dismiss="modal">Cancelar</button>
                                </div>
                                <div class="col-4">
                                <form method="POST" id="formDelete" action="{{ route('produto.destroy', [0]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-block btn-lg btn-danger">Excluir</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('pl-plugins')
    <!--Plugins-->
    <script src="{{ asset('js/dependencies/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dependencies/dataTables.bootstrap4.min.js') }}"></script>
@endsection

@section('pl-custom-scripts')
    <script src="{{ asset('js/custom/dashboard/index.js') }}"></script>
    <script>
        
        $(document).ready(function() { 
            //Carrega a datatables
            var table = $('#productDataTable').DataTable({
                order: [1, 'asc'],
                columns: [
                    {"orderable": false, },
                    { "className": 'small'},
                    {"className": "text-nowrap small"},
                    { "className": 'small'},
                    { "className": 'small'},
                    {},
                    {
                        "className": 'text-center small',
                        "orderable":      false,
                    },
                ],
                initComplete: function () {
                    this.api().columns([3]).every( function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
        
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );
        
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                },
            });
        });
    </script>
@endsection
