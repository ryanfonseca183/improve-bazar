@extends('layouts.dashboard')

@section('title', 'Produtos')

@section('icon')
    <i class="fas fa-mobile-alt"></i>
@endsection

@section('pl-custom-styles')
    <style>
        .attribute-group {
            padding: 1.5rem;
        }
        .attribute-label {
            font-size: 1.25rem;
        }
        .variation-general-attribute:not(:last-child) {
            margin-bottom: 3rem;
        }
        .variation-sku {
            font-size:1rem;
            margin-bottom: 0;
        }
        .variation-sku-separator {
            margin: 0.5rem 0 1rem;
            opacity: 0.5;
        }
    </style>
@endsection

@section('desc', 'Cadastre, atualize ou remova os produtos ofertados na loja.')

@section('content')
    @php 
        $categorias = session()->get('storeProduto.categorias');
        $selectedCategoria = session()->get('storeProduto.categoria');
        $selectedSubcategoria = session()->get('storeProduto.subcategoria');
        $simpleAttributes = session()->get('storeProduto.simple');
        $configurableAttributes = session()->get('storeProduto.configurable');
    @endphp

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 bg-transparent">
        <li class="breadcrumb-item"><a href="{{ route('produto.index') }}">Produtos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cadastrar</li>
        </ol>
    </nav>
    
    <div class="row justify-content-center my-5">
        @isset($categorias)
            <div class="col-lg-10 col-xl-8">
                <div class="card content-card">
                    <div class="vflex-center mb-4">
                        <h1 class="h5 mb-0">
                            Novo produto
                        </h1>
                        @isset($selectedSubcategoria)
                            <span class="mx-2">
                                <i class="fas fa-chevron-right"></i>
                            </span>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb p-0 bg-transparent mb-0">
                                    <li class="breadcrumb-item">{{$selectedSubcategoria->categoria->nome}}</li>
                                    <li class="breadcrumb-item">{{$selectedSubcategoria->nome}}</li>
                                </ol>
                            </nav>
                        @endisset
                    </div>
                    <div id="carousel" class="carousel" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner" id="cadProdInner">
                            {{-- CLASSIFICAÇÃO DO PRODUTO --}}
                            <x-choose-categorie :categorias="$categorias" :selected="['categoria' => $selectedCategoria, 'subcategoria' => $selectedSubcategoria]" action="getAtributes"></x-choose-categorie>
                            
                            @isset($selectedSubcategoria)
                                {{-- Atributos simples--}}
                                @if(count($simpleAttributes) != 0) 
                                    @foreach($simpleAttributes as $key => $atr)
                                        <div class="carousel-item @if($key == 0) active @endif">
                                            <x-card card-role="select" card-title="Características">
                                                <x-slot name="card_body">
                                                    @switch($atr->tipoDeDados->frontend_type_label)
                                                        @case("Caractere")
                                                           <x-text-attribute-group :attribute="$atr"></x-text-attribute-group>
                                                        @break

                                                        @case("Cor")
                                                            <x-color-attribute-group :attribute="$atr"></x-color-attribute-group>
                                                        @break

                                                        @case('Lógico')
                                                            <x-logic-attribute-group :attribute="$atr"></x-logic-attribute-group>
                                                        @break;
                                                    @endswitch
                                                </x-slot>
                                            </x-card>
                                        </div>
                                    @endforeach
                                @endif
                                {{-- Nome --}}
                                <div class="carousel-item @if(count($simpleAttributes) == 0) active @endif">
                                    <x-card card-title="Dados do produto">
                                        <x-slot name="card_body">
                                            <div class="form-group" id="name_group">
                                                <label for="nome" class="mb-0 h5">Nome</label>
                                                <p class="text-muted mb-3">Esse será o nome de exibição do produto na loja</p>
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-lg mb-2" 
                                                    id="nome"
                                                    name="nome"
                                                    required 
                                                    maxlength="255"/>
                                                <span class="invalid-feedback position-absolute" id="info_nome">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </x-slot>
                                        <x-slot name="card_footer">
                                            <button onclick="checkInput('name_group')" class="btn btn-primary float-right">
                                                Próximo
                                            </button>
                                        </x-slot>
                                    </x-card>
                                </div>
    
                                {{-- Descrição --}}
                                <div class="carousel-item">
                                    <x-card  card-title="Dados do produto">
                                        <x-slot name="card_body">
                                            <div class="form-group" id="desc_group">
                                                <label for="descricao" class="mb-1 h5">Descrição <span class="ml-2 text-muted small">(Opcional)</span></label>
                                                <p class="text-muted mb-3">Fornece informações adicionais sobre o produto e será exibida quando o cliente visualizar o produto.</p>
                                                <textarea 
                                                    id="descricao"
                                                    name="descricao"
                                                    rows="5" 
                                                    class="form-control mb-2"></textarea>
                                            </div>
                                        </x-slot>
                                        <x-slot name="card_footer">
                                            <button 
                                                onclick="checkInput('desc_group')" 
                                                class="btn btn-primary float-right">
                                                    Próximo
                                            </button>
                                        </x-slot>
                                    </x-card>
                                </div>
                               
                                {{-- Peso --}}
                                <div class="carousel-item">
                                    <x-card card-title="Dados do produto">
                                        <x-slot name="card_body">
                                            <div class="form-group" id="peso_group">
                                                <label for="preco" class="mb-0 h5">Peso (kg)</label>
                                                <p>Será utilizado para calculo do frete na sacola de compras.</p>
                                                <input 
                                                type="number" 
                                                class="form-control form-control-lg mb-2" 
                                                id="peso"
                                                name="peso"
                                                required 
                                                max="30"
                                                step="0.01" />
                                                <span class="invalid-feedback position-absolute" id="info_peso">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </x-slot>
                                        <x-slot name="card_footer">
                                            <button onclick="checkInput('peso_group')" class="btn btn-primary float-right">
                                                Próximo
                                            </button>
                                        </x-slot>
                                    </x-card>
                                </div>
                                
                                {{-- Dimensões --}}
                                <div class="carousel-item">
                                    <x-card card-title="Dados do produto">
                                        <x-slot name="card_body">
                                            <div class="form-group" id="dim_group">
                                                <label class="mb-0 h5">Dimensões (cm)</label>
                                                <p>As medidas devem ser <strong>relativas a embalagem</strong> para calculo do frete</p>
                                                <div class="form-row">
                                                    {{-- ALTURA --}}
                                                    <div class="form-group col">
                                                        <input 
                                                        type="number" 
                                                        class="form-control" 
                                                        id="altura"
                                                        name="altura"
                                                        placeholder="Altura"
                                                        required 
                                                        max="100"
                                                        step="0.01" />
                                                        <span class="invalid-feedback position-absolute" id="info_altura">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                    {{-- LARGURA --}}
                                                    <div class="form-group col">
                                                        <input 
                                                        type="number" 
                                                        class="form-control" 
                                                        id="largura"
                                                        name="largura"
                                                        placeholder="Largura"
                                                        required
                                                        min="10"
                                                        max="100"
                                                        step="0.01" />
                                                        <span class="invalid-feedback position-absolute" id="info_largura">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                    {{-- COMPRIMENTO --}}
                                                    <div class="form-group col">
                                                        <input 
                                                        type="number" 
                                                        class="form-control" 
                                                        id="comprimento"
                                                        name="comprimento"
                                                        placeholder="Profundidade"
                                                        required
                                                        min="15"
                                                        max="100"
                                                        step="0.01" />
                                                        <span class="invalid-feedback position-absolute" id="info_comprimento">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </x-slot>
                                        <x-slot name="card_footer">
                                            <button onclick="checkGroup('dim_group')" class="btn btn-primary float-right">
                                                Próximo
                                            </button>
                                        </x-slot>
                                    </x-card>
                                </div>

                                @if(count($configurableAttributes) != 0)
                                    {{-- Variações --}}
                                    <div class="carousel-item">
                                        <x-card card-title="Dados gerais">
                                            <x-slot name="card_body">
                                                <h5 class="mb-0">Configurações</h5>
                                                <p>Definem variações do produto para seleção no momento da compra.</p>
                                                
                                                {{-- INPUTS --}}
                                                <div id="listMult">
                                                    <div class="form-row">
                                                        @foreach($configurableAttributes as $key => $atr)
                                                            @switch($atr->tipoDeDados->frontend_type_label)
                                                                @case('Caractere')
                                                                    <x-configurable-text-attribute-group :attribute="$atr"></x-configurable-text-attribute-group>
                                                                @break;

                                                                @case("Cor")
                                                                    <x-configurable-color-attribute-group :attribute="$atr"></x-configurable-color-attribute-group>
                                                                @break;
                                                            @endswitch
                                                        @endforeach
                                                        {{-- SKU --}}
                                                        <div class="col-md-6 form-group">
                                                            <label for="sku">SKU</label>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <input 
                                                                    type="text" 
                                                                    class="form-control valor" 
                                                                    required 
                                                                    id="sku"
                                                                    data-atr-id="sku" />
                                                                    <span class="invalid-feedback" id="info_sku">
                                                                        <strong></strong>
                                                                    </span>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button 
                                                                    type="button" 
                                                                    onclick="checkVariation()" 
                                                                    class="btn btn-primary">
                                                                        <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <span class="invalid-feedback d-block" id="info_variations">
                                                    <strong></strong>
                                                </span>
                                                <div class="table-responsive text-center">
                                                    <table class="table">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                @foreach($configurableAttributes as $atr)
                                                                    <th>{{ $atr->nome }}</th>
                                                                @endforeach
                                                                <th>SKU</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="variations">
                                                            <tr id="default-row">
                                                                <td colspan="{{ $configurableAttributes->count() + 2 }}">Nenhuma configuração cadastrada</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </x-slot>
                                            <x-slot name="card_footer">
                                                <button onclick="checkNumVariations()" class="btn btn-primary float-right">Próximo</button>
                                            </x-slot>
                                        </x-card>
                                    </div>
                                @endif

                                {{-- Estoque --}}
                                <div class="carousel-item">
                                    <div class="card shadow-sm">
                                        <div class="card-header bg-orange text-white">
                                            <h2 class="h5 mb-0 p-2">Dados gerais</h2>
                                        </div>
                                        <div class="card-body py-5" id="quantidadeContainer">
                                            @if(count($configurableAttributes) == 0)
                                                <div class="form-row align-items-baseline" id="quantidade_group">
                                                    <div class="form-group col-md-6">
                                                        <label for="estoque" class="mb-3 h5">Quantidade em estoque</label>
                                                        <input 
                                                            type="number" 
                                                            class="form-control form-control-lg mb-2" 
                                                            id="estoque"
                                                            name="estoque"
                                                            required 
                                                            step="1"/>
                                                        <span class="invalid-feedback position-absolute" id="info_estoque">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="sku" class="mb-3 h5">SKU</label>
                                                        <input 
                                                            type="text" 
                                                            class="form-control form-control-lg mb-2" 
                                                            id="sku"
                                                            name="sku"
                                                            required />
                                                        <span class="invalid-feedback position-absolute" id="info_sku">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="card-footer">
                                            @if(count($configurableAttributes) != 0)
                                                <button onclick="checkVariationsGeneralAttribute('quantidade')" class="btn btn-primary float-right">
                                                    Próximo
                                                </button>
                                            @else 
                                                <button onclick="checkGroup('quantidade_group')" class="btn btn-primary float-right">
                                                    Próximo
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                {{-- Preço --}}
                                <div class="carousel-item">
                                    <div class="card shadow-sm">
                                        <div class="card-header bg-orange text-white">
                                            <h2 class="h5 mb-0 p-2">Dados gerais</h2>
                                        </div>
                                        <div class="card-body py-5" id="precoContainer">
                                            @if(count($configurableAttributes) == 0)
                                                <div class="form-group" id="preco_group">
                                                    <label for="preco" class="mb-3 h5">Preço</label>
                                                    <input 
                                                        type="number" 
                                                        class="form-control form-control-lg mb-2" 
                                                        id="preco"
                                                        name="preco"
                                                        data-name="preço" 
                                                        required 
                                                        step="0.01" />
                                                    <span class="invalid-feedback position-absolute" id="info_preco">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="card-footer">
                                            @if(count($configurableAttributes) != 0)
                                                <button onclick="checkVariationsGeneralAttribute('preco')" class="btn btn-primary float-right">
                                                    Próximo
                                                </button>
                                            @else 
                                                <button onclick="checkInput('preco_group')" class="btn btn-primary float-right">
                                                    Próximo
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- Fotos --}}
                                <div class="carousel-item">
                                    <x-card card-title="Fotos do produto">
                                        <x-slot name="card_body">
                                            <form id="form-photos">
                                                <div class="form-group">
                                                    <h2 class="h5 mb-0"> Fotos de exibição</h2>
                                                    <p>Clique no campo abaixo para começar a escolher ou arraste as fotos do seu computador</p>
                                                    <span class="invalid-feedback" id="info_fotos">
                                                        <strong></strong>
                                                    </span>
                                                    <label id="drop-photos">
                                                        <input 
                                                        type="file" 
                                                        name="photos" 
                                                        id="imagens" 
                                                        accept="image/jpg, image/jpeg, image/png, image/webp, image/JPG, image/JPEG, image/PNG, image/WEBP" 
                                                        multiple="" 
                                                        class="d-none" 
                                                        autocomplete="off"/>
                                                        <i class="far fa-images" style="font-size: 50px;"></i>
                                                    </label>
                                                </div>
                                            </form>
                                            <div id="preview-photos" class="form-row mb-4">
                                            </div>
                                        </x-slot>
                                        <x-slot name="card_footer">
                                            <form method="POST" action="{{ route('produto.store') }}" id="formCad">
                                                @csrf
                                                <button 
                                                    type="button" 
                                                    onclick="checkMinPhotos('formCad')" 
                                                    class="btn btn-success float-right">
                                                        Finalizar
                                                </button>
                                            </form>
                                        </x-slot>
                                    </x-card>
                                </div>
                            @endisset
                        </div>
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                </div>
            </div>
        @endisset
    </div>

    
@endsection

@section('pl-custom-scripts')
    <!-- Page level custom scripts -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/custom/dashboard/create.js') }}"></script>

    <script>
        {{--/*
        |--------------------------------------------------------------------------
        | Classificação do produto
        |--------------------------------------------------------------------------
        | A função abaixo define a categoria selecionada e lista as subcategorias da
        | categoria selecionada.
        | 
        */--}}

        function getSubcategorias(id) {
            $.ajax({
                url: '{{ route("categoria.getSubcategorias")}}',
                method: 'GET',
                datatype: "JSON",
                data: {
                    categoria: id,
                },
                beforeSend: function() {
                    $('#carousel').addClass('carousel-sliding');
                },
                success: function(response) {
                    var subcategorias = response.subcategorias;
                    $('#subcatList .list-group-item').remove();

                    //Carrega as subcategorias
                    for(var i = 0; i < subcategorias.length; i++) {
                        let option = `
                            <li 
                            onclick="loadLiValue(this, null)"
                            class="list-group-item"
                            data-submit="getAtributes"
                            data-parent="subcatList"
                            data-option-value="subcatInput/${subcategorias[i].id}">
                                <span>${subcategorias[i].nome}</span>
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </li>
                        `;
                        $('#subcatList').append(option);
                    }
                    $('#carousel').carousel('next');
                },
                error: function(xhr, status, code) {
                    console.log(xhr);
                },
                complete: function() {
                    $('#carousel').removeClass('carousel-sliding');
                }
            })
        }
    </script>

    @if(isset($selectedSubcategoria))

        @if(count($simpleAttributes) != 0)
            
            <script>
                {{--/*
                |--------------------------------------------------------------------------
                | Atributos simples, não configuráveis
                |--------------------------------------------------------------------------
                | As funções abaixos são utilizadas para armazenar em sessão o valor 
                | selecionado ou digitado de atributos que podem receber apenas um valor.
                | 
                */--}}

                //Define o valor de atributos específicos 
                function setAtribute( idAtr, valor, label, element = null) {
                    $.ajax({
                        url:"{{ route('produto.setSimpleAttribute') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: idAtr,
                            valor: valor,
                            label: label,
                        },
                        beforeSend: function() {
                            $('#carousel').addClass('carousel-sliding');
                        },
                        success: function(response) {
                            // REDEFININDO VALOR SELECIONADO
                            if(element) {
                                let attrList = $(element.getAttribute('data-parent'));
                                $('li.active', attrList).removeClass("active");
                                element.classList.add('active');
                            }
                            // CONSTRUINDO O NOME COM BASE NOS VALORES SELECIONADOS
                            let nome = "";
                            let array = Object.values(response.valSimAttr);
                            array.forEach(function(val){
                                let attrName = "";
                                // SELECIONA O VALOR PARA ADICIONAR O NOME
                                if(val.label) attrName = val.label; else if(val.valor != "Não") attrName = val.valor;

                                // ADICIONA O VALOR AO NOME
                                if(nome) nome += (" " +  attrName); else nome = attrName;
                            });
                            $("#nome").val(nome);
                            $("#carousel").carousel("next");
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                        },
                        complete: function() {
                            $('#carousel').removeClass('carousel-sliding');
                        }
                    })
                }
                //Recupera o valor do input e define como valor do atributo
                function setAtributeFromInput(inputID) {
                    let attrValue = document.getElementById(inputID);
                    let attrLabel = document.getElementById('label_' + inputID);
                    let attrGroup = document.getElementById('group_' + inputID);
                    let attrList = document.getElementById('list_' + inputID);

                    //Constroi um array com o valor e label para o atributo
                    let inputs = [];
                    inputs.push(attrValue);
                    if(attrLabel) inputs.push(attrLabel);

                    //VALIDANDO OS VALORES
                    let flag = validate(inputs);
                    attrGroup.classList.add('was-validated');
                    if(!flag) return;

                    //Redefine o valor selecionado
                    if(attrList) $('li.active', attrList).removeClass('active')

                    //Armazena os valores dos campos em sessão
                    let atrId = attrValue.getAttribute('data-atr-id');
                    let labelValue = attrLabel != null ? attrLabel.value : null;
                    let valueValue = attrValue.value;
                    setAtribute(atrId, valueValue, labelValue);
                    attrGroup.classList.remove("was-validated");
                }
            </script>
        @endif

        @if(count($configurableAttributes) != 0)
            
            <script>
                {{--/*
                |--------------------------------------------------------------------------
                | Atributos configuráveis 
                |--------------------------------------------------------------------------
                | As funções abaixos são utilizadas para armazenar os valores de atributos
                | configuráveis em sessão, bem como a quantidade em estoque e sku para cada 
                | variação
                | 
                */--}}
                function checkNumVariations() {
                    let rows = $('#variations tr:not(#default-row)');
                    if(rows.length == 0) {
                        $("#info_variations strong").html("Para prosseguir, cadastre ao menos uma configuração.");
                    } else {
                        $("#info_variations strong").html("");
                        $("#carousel").carousel("next");
                    }
                }
                function loadDataListOption(li) {
                    let name = loadLiValue(li, null);
                    let collapse = li.parentNode.parentNode;
                    $("#" + collapse.id).collapse("hide");
                    let span = $("[data-target='#" + collapse.id + "'] span");
                    span.html(name);
                }
                function checkVariation(){
                    var inputs = $('.valor');
                    var labels = $('.label');
                    //MONTA UM ARRAY COM TODOS OS VALORES
                    var allInputs = $.merge($.merge([], inputs), $.merge([], labels));
                    $("#listMult").addClass("was-validated");
                    if(validate(allInputs)) {
                        let valores = [];
                        let listVal = new Object();
                        for(var i = 0; i < inputs.length; i++) {
                            if(inputs[i].getAttribute('data-atr-id') == "sku") {
                                listVal.sku = inputs[i].value;
                            } else {
                                let label = document.getElementById('label_' + inputs[i].id);
                                let labelValue = label != null ? label.value : null;
                                valores.push([inputs[i].getAttribute('data-atr-id'), inputs[i].value, labelValue]);
                            }
                            listVal.valores = valores;
                        }
                        setVariation(listVal, inputs);
                    }
                }
                function setVariation(listValues, inputs) {
                    $.ajax({
                        url:"{{ route('produto.setVariation') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            list: listValues,
                        },
                        beforeSend: function() {
                            $('#carousel').addClass('carousel-sliding');
                        },
                        success: function(response) {
                            $("#listMult").removeClass('was-validated');
                            if(response.hasList) {
                                $("#info_variations strong").html("Essa configuração já foi cadastrada");
                            } else {
                                $("#info_variations strong").html("");
                                loadVariation(inputs, response);
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                        },
                         complete: function() {
                            $('#carousel').removeClass('carousel-sliding');
                        }
                    })
                }
                function loadVariation(inputs, res) {
                    let variationID = res.idList;
                    let sku = res.listVal.sku;
                    let valores = res.listVal.valores;
                    let tr, td;

                    tr = document.createElement('tr');
                    tr.setAttribute('data-variation-id', variationID);
                    //CARREGANDO OS VALROES DA LINHA
                    for(var i = 0; i < inputs.length; i++) {
                        dataAtrId = inputs[i].getAttribute('data-atr-id');
                        if(dataAtrId == "sku") {
                            td = document.createElement('td');
                            td.innerHTML = sku;
                            tr.append(td);
                        } else {
                            let valor = valores[i];
                            let input = inputs[i];
                            if(input.tagName == "INPUT") {
                                switch(input.type) {
                                    case 'text':
                                    case 'number':
                                        td = document.createElement('td');
                                        td.innerHTML = valor[1];
                                    break;
                                    case 'color':
                                        td = document.createElement('td');
                                        td.classList.add('td-color');
                                        td.innerHTML = 
                                            '<div class="input-group-transparent">'+
                                                '<i class="input-show-color" style="background-color:'+valor[1] +';"></i>'+
                                                '<span class="ml-2">'+ valor[2] +'</span>'+
                                            '</div>';
                                    break;
                                }
                            } else if (input.tagName == "SELECT") {
                                td = document.createElement('td');
                                td.innerHTML = valor[1];
                            }
                            tr.append(td);
                        }
                    }

                    // CARREGANDO O BOTÃO DE DELETE
                    td = document.createElement('td');
                    td.innerHTML = `
                        <button type="button" class="btn btn-action" onclick="destroyVariation(${variationID})">
                            <i class="fas fa-trash-alt"></i>
                        </button>`;
                    tr.append(td);

                    //INSERINDO A LINHA NA TABELA
                    $("#variations").append(tr);
                    let rows = $('#variations tr');

                    // ESCONDENDO A LINHA PADRÃO
                    $('#default-row').hide();   

                    //CARREGANDO OS INPUTS DE ATRIBUTOS GERAIS DAS VARIAÇÕES
                    loadVariationInputs(sku, variationID);
                }
                function destroyVariation(id) {
                    $.ajax({
                        url: "{{ route('produto.destroyVariation') }}",
                        type: 'delete', // replaced from put
                        dataType: "text",
                        data: {
                            'id': id,
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function() {
                            $("[data-variation-id='"+id+"']").remove();
                            if($("#variations tr").length == 0) $("#default-row").show();
                        },
                        error: function(xhr, status, erro) {
                            console.log(xhr, status, erro);
                        },
                    });
                }
                let priceContainer = $("#precoContainer");
                let priceCheck = "";
                function loadVariationInputs(sku, variationID) {
                    let inputs = [
                        ['Quantidade', 'quantidade', 'type="number" required min="1"'], 
                        ['Preço', 'preco', 'type="number" required min="1" step="0.01"'],
                    ];
                    for(var i =0; i < inputs.length; i++) {
                        let that = inputs[i];
                        let div = document.createElement('div');
                        div.setAttribute('data-variation-id', variationID);
                        div.classList.add('variation-general-attribute');
                        div.innerHTML = `
                            <h3 class="variation-sku">${sku}</h3>
                            <hr class="variation-sku-separator">
                            <label for="${that[1]}_${variationID}">${that[0]}</label>
                            <input ${that[2]} name="${that[1]}[]" id="${that[1]}_${variationID}" class="form-control w-50">
                            <span class="invalid-feedback">
                                <strong>Digite uma ${that[0]} válida</strong>
                            </span>
                        `;
                        $("#" + that[1] + "Container").append(div);
                    }
                    loadPriceCheck();
                }
                function loadPriceCheck() {
                    let variationAttributes = $(".variation-general-attribute", priceContainer);
                    if(priceCheck == "" && variationAttributes.length > 1) {
                        let price = variationAttributes.first();
                        let check = document.createElement('div');
                        check.innerHTML = `<div class="form-check mt-3">
                            <input class="form-check-input" type="checkbox" id="priceCheck">
                            <label class="form-check-label" for="priceCheck">
                                Definir o mesmo preço para todas as variações
                            </label>
                        </div>`;
                        price.append(check);
                        priceCheck = check;
                        $("#priceCheck", check).on('change', function(){
                            let input = price.find('[name="preco[]"]');
                            if($(this).prop('checked')) {
                                $('[name="preco[]"]').val(input.val());
                                input.on('input', function() {
                                    $('[name="preco[]"]').val(input.val());
                                });
                            } else {
                                input.off('input');
                                $('[name="preco[]"]').val("");
                            }
                        });
                    }
                }
                function checkVariationsGeneralAttribute(name) {
                    let inputs =  $("[name='"+name+"[]']");
                    console.log(inputs);
                    $("#" + name + "Container").addClass("was-validated");
                    if(validate(inputs)) {
                        let values = [];
                        for(var i =0; i < inputs.length; i++) {
                            let variationID = inputs[i].parentNode.getAttribute('data-variation-id');
                            values.push([variationID, inputs[i].value]);
                        }
                        setVariationsGeneralAttribute(name, values);
                    }
                }
                function setVariationsGeneralAttribute(name, values) {
                    $.ajax({
                        url:"{{ route('produto.setVariationsGeneralAttribute') }}",
                        method: "POST",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            'name': name,
                            'values': values,
                        },
                        beforeSend: function() {
                            $('#carousel').addClass('carousel-sliding');
                        },
                        success: function(response) {
                            $("#carousel").carousel('next');
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                        },
                         complete: function() {
                            $('#carousel').removeClass('carousel-sliding');
                        }
                    })
                }
            </script>
        @endif

        <script>
            
            {{--/*
            |--------------------------------------------------------------------------
            | Atributos gerais
            |--------------------------------------------------------------------------
            | As funções abaixos são utilizadas para armazenar os valores de atributos 
            | gerais (aplicáveis a todos os produtos) em sessão.
            | 
            */--}}
            function checkInput(groupId) {
                let group = $("#" + groupId);
                let input = $("input:visible:not([type=hidden]):not([type=color]), textarea, select", group).first();
                let flag = validate(input);
                group.addClass('was-validated');
                if(flag) {
                    setGeneralAtribute(input.prop('name'), input.val());
                }
            }
            function checkGroup(groupId) {
                let group = $("#" + groupId);
                let inputs = $("input:visible:not([type=hidden]):not([type=color]), textarea, select", group);
                let flag = validate(inputs);
                group.addClass('was-validated');
                if(flag) {
                    $('#carousel').addClass('carousel-sliding');
                    for(var i = 0; i < inputs.length; i++) {
                        setGeneralAtribute(inputs[i].name, inputs[i].value, true);
                    }
                    $("#carousel").carousel("next");
                    $('#carousel').removeClass('carousel-sliding');
                }
            }
            function setGeneralAtribute(nameGenAtr, valorGenAtr, isForm = false) {
                $.ajax({
                    url:"{{ route('produto.setGeneralAttribute') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        nome: nameGenAtr,
                        valor: valorGenAtr,
                    },
                    beforeSend: function() {
                        if(!isForm) $('#carousel').addClass('carousel-sliding');
                    },
                    success: function(response) {
                        if(!isForm) {
                            $("#carousel").carousel("next");
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr);
                    },
                    complete: function() {
                        $('#carousel').removeClass('carousel-sliding');
                    }
                })
            }
           
            {{--/*
            |--------------------------------------------------------------------------
            | Fotos do produto
            |--------------------------------------------------------------------------
            | As funções abaixos são utilizadas para armazenar as fotos do produto no
            | Storage, bem como o caminho das fotos em sessão.
            | 
            */--}}

            $(document).ready(function () { 
                $("#preview-photos").sortable({
                    revert: true,
                    stop: function(event, ui) {
                        let first = $("#preview-photos .show-photo").first();
                        idPhoto = first.attr('id-photo');
                        if(idPhoto != mainPhoto) {
                            setMainPhoto(first.attr('id-photo'));
                        }
                    },
                });
                $("#drop-photos").droppable({
                    accept: "*:not(.ui-sortable-helper)",
                });
                $("#drop-photos").on('dragenter', function(e) {
                    e.preventDefault();
                    $(this).css('border-color', 'var(--avermelhado)');
                });
                $("#drop-photos").on('dragover', function(e) {
                    e.preventDefault();
                });
                $("#drop-photos").on('drop', function(e) {
                    e.preventDefault();
                    $(this).css('border-color', '#e3e6f0');
                    var unsetPhotos = e.originalEvent.dataTransfer.files;
                    setPhotos(unsetPhotos)
                });
            });
            var mainPhoto;
            var asset = "{{ asset('storage') }}";
            $("#imagens").change(function(){
                setPhotos($(this).prop('files'));
            })
            function checkMinPhotos(form) {
                let photos = $('#preview-photos .listable-product-image');
                if(photos.length == 0) {
                    $("#info_fotos").addClass('d-block');
                    $("#info_fotos").html("Voce deve cadastrar ao menos uma foto para o produto.")
                } else {
                    $("#info_fotos").removeClass('d-block');
                    $("#" + form).submit();
                }
            }
            function setPhotos(files) {
                // VALIDANDO AS FOTOS
                if(!validatePhotos(files)) return;

                // CRIANDO O CORPO DA REQUISIÇÃO
                var formData = new FormData();
                for(var i = 0; i < files.length; i++) {
                    formData.append('photos[]', files[i]);
                }
                // ARMAZENANDO AS FOTOS
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('produto.setPhotos') }}",
                    type: "POST",
                    data: formData,
                    contentType:false,
                    cache: false,
                    processData: false,
                    success: function(response){
                        loadPhoto(response);
                        $("#imagens").val("");
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    },
                   
                });
            }
            function setMainPhoto(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('produto.setMainPhoto') }}",
                    type: "POST",
                    data: {
                        idPhoto: id,
                    },
                    success: function(response) {
                        mainPhoto = response.main;
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    }
                });
            }
            function loadPhoto(response) {
                let path = response.path;
                mainPhoto = response.main;
                path.forEach(function(item){
                    //Container de exibição da imagem
                    let col = document.createElement('div');
                    col.classList.add('col-sm-auto', 'show-photo');
                    col.setAttribute('id-photo', item[1]);
                    col.innerHTML = "<div class='listable-product-image img-sm thumb'></div>";

                    //Imagem do produto
                    let img = document.createElement('img')
                    img.src = asset + "/" + item[0]; 
                    img.classList.add('img-fluid')

                    //Botão de delete
                    let deleteContainer = document.createElement('div');
                    deleteContainer.classList.add('delete-photo');
                    deleteContainer.innerHTML =
                    "<button type='button' class='btn btn-danger' onclick='deletePhoto("+item[1]+", this)'>"+
                        '<i class="fas fa-trash-alt"></i>'+
                    "</button>";

                    col.appendChild(deleteContainer);
                    col.querySelector('.thumb').appendChild(img);
                    $("#preview-photos").append(col);
                })
            }
            function deletePhoto(id) {
                $.ajax({
                    url: '{{ route("produto.destroyPhoto") }}',
                    method: 'DELETE',
                    datatype: "JSON",
                    data: {
                        idPhoto: id,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        $("[id-photo='"+id+"']").remove()
                        if(response.main == null) {
                            let first = $(".show-photo").first();
                            if(first.attr("id-photo") != null) {
                                setMainPhoto(first.attr("id-photo"));
                            }
                        }
                    },
                    error: function(xhr, status, code) {
                        console.log(xhr);
                    },
                })
            }
            
        </script>
    @endif

@endsection
