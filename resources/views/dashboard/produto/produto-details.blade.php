@extends('layouts.dashboard')

@extends('layouts.modal-delete')

@section('route-delete', route('produto.destroy', [$produto->id]))

@section('modal-text')
    <p>Ao excluir esse produto, ele não será mais listado no painel de controle ou catálogo da loja, mas ainda poderá ser visto nos pedidos dos usuários. Deseja continuar?</p>
@endsection

@section('title', 'Produtos')

@section('pl-custom-styles')
    <!-- Custom styles for this page -->
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style>
        .group-info {
            margin-bottom: 1.5rem;
            display:flex;
            flex-direction:column;
        }
        .group-info-header {
            margin-bottom: 0.5rem;
        }
        .group-info-title {
            font-size: 1.25rem;
            display:inline-block;
            margin-bottom: 0;
            margin-right: 0.5rem;
        }
        .card.card-body {
            padding-bottom: 2rem;
        }
        .was-validated .form-control.wt-st {
            padding-right: 0.75rem !important;
            background: none !important;
        }
        .unsaved {
            display:none;
            margin-left: 0.5rem;
            color: #f6c23e !important;
        }
        @media only screen and (min-width: 998px) {
            .variation-group {
                margin-bottom: 0;
            }
            .position-lg-absolute {
                position:absolute;
            }
        }
       .form-control-plaintext {
            display: block;
            width:100%;
            height: calc(1.5em + 0.75rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            border-bottom: 1px solid #d1d3e2;
       }
       .card-config {
           background:transparent;
           display: flex;
           justify-content: space-between;
           align-items:center;
           border-bottom: none;
       }
       .product-sku {
           margin-bottom:0;
           font-size: 1rem;
       }
       
    </style>
@endsection

@section('icon')
    <i class="fas fa-mobile-alt"></i>
@endsection

@section('desc', 'Cadastre, atualize ou remova os produtos ofertados na loja.')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb px-0 bg-transparent">
            <li class="breadcrumb-item"><a href="{{ route('produto.index') }}">Produtos</a></li>
            <li class="breadcrumb-item active">{{$produto->nome}}</li>
        </ol>
    </nav>
    <div class="row my-4 justify-content-center my-5">
        <div class="col-lg-10 col-xl-8 mb-5">
            <div class="card content-card">
                {{-- NOME DO PRODUTO --}}
                <div class="flex-between mb-2">
                    <h2 class="h5 mb-0 d-inline-block">
                        <span id="product-nome">{{$produto->nome}}</span>
                        <button class="btn btn-action ml-1" data-toggle="modal" data-target="#modal_form_name">
                            <i class="fas fa-edit"></i> 
                        </button>
                    </h2>
                    <button type="button" class="btn btn-action" data-toggle="modal" data-target="#modalDelete">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                </div>

                {{--  VISIBILIDADE --}}
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" name="is_visible" id="visibility" @if($produto->is_visible) checked @endif value="1" class="custom-control-input" >
                    <label class="custom-control-label" for="visibility">Exibir produto no catálogo</label>
                </div>
                <p class="mb-4">Ao desabilitar essa opção, não será mais possível encontra-lo na loja </p>
                
                {{-- IMAGENS DO PRODUTO --}}
                <div class="group-info mb-5">
                    <div class="group-info-header">
                        <h2 class="group-info-title">Imagens</h2>
                        <span class="invalid-feedback" id="info_fotos">
                            <strong></strong>
                        </span>
                    </div>
                    <div class="group-info-content">
                        <div id="preview-photos" class="form-row">
                            @php 
                                $fotos = $produto->fotosProduto()->orderBy('is_main', 'desc')->get();
                                $mainPhoto = $fotos->first();
                                $count = $fotos->count();
                            @endphp 
                            @foreach($fotos as $key => $ft)
                                <div class="col-auto show-photo @if($key == 0 && $count == 1) only-photo @endif" data-id-photo="{{ $ft->id }}">
                                    <div class="listable-product-image img-sm thumb">
                                        <img src="{{ asset("storage/" . $ft->caminho) }}">
                                    </div>
                                    <div class="delete-photo">
                                        <button type='button' class='btn btn-danger' onclick="deletePhoto({{$ft->id}}, this)"><i class="fas fa-trash-alt"></i></button>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-auto new-photo">
                                <label class="new-photo-label">
                                    <input 
                                    type="file" 
                                    name="photos" 
                                    id="imagens" 
                                    accept="image/jpg, image/jpeg, image/png, image/webp, image/JPG, image/JPEG, image/PNG, image/WEBP" 
                                    multiple
                                    class="d-none" 
                                    autocomplete="off"/>
                                    <i class="fas fa-images"></i>
                                </label>
                            </div>
                        </div>
                        
                    </div>
                </div>

                {{-- DESCONTO DO PRODUTO --}}
                <div class="group-info">
                    <div class="group-info-header">
                        @php
                            $hasPromo = $produto->desconto != $produto->preco;
                            $class = $hasPromo ? "badge-success" : "badge-danger";
                            $message = $hasPromo ? "Ativo" : "Inativo";
                        @endphp
                        <h2 class="group-info-title">Desconto <span id="promo_status" class="badge {{ $class }}">{{$message}}</span> </h2>
                    </div>
                    <div class="group-info-content">
                        <form id="descount">
                            <div class="card card-body">
                                <div class="form-group row align-items-start">
                                    <label for="promo_value" class="col-sm-3 col-form-label">Valor (%)</label>
                                    <div class="col-sm-7">
                                        <input type="number" name="promo_value" id="promo_value" class="form-control" max="100" value="{{ $produto->promo_value }}" >
                                        <span class="invalid-feedback" id="info_promo_value"><strong></strong></span>
                                        <div class="form-row my-3">
                                            <div class="col-lg-6">
                                                <label for="promo_starts_at" class="mb-0">Começa em</label>
                                                <input type="date" name="promo_starts_at" id="promo_starts_at" class="form-control" data-name="data de ínicio" value="{{ $produto->promo_starts_at }}" min="{{ $produto->promo_starts_min }}">
                                                <span class="invalid-feedback" id="info_promo_starts_at"><strong></strong></span>
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="promo_ends_at" class="mb-0">Termina em</label>
                                                <input type="date" name="promo_ends_at" id="promo_ends_at" class="form-control" data-name="data de término" min="{{ $produto->promo_ends_min }}" value="{{ $produto->promo_ends_at }}">
                                                <span class="invalid-feedback" id="info_promo_ends_at"><strong></strong></span>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary" type="button" onclick="checkFormGeneralAttributes('#descount')">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                {{-- CONFIGURAÇÕES DO PRODUTO --}}
                <div class="accordion mb-5" id="accordion-config">
                    @php  if($produto->filhos()->exists()) $produtos = $produto->filhos; else $produtos = collect([$produto]); @endphp
                    @foreach($produtos as $key => $filho)
                        <div class="card" id="card_form_{{ $key }}">
                            <div class="card-header card-config">
                                {{-- SKU DA CONFIGURAÇÃO --}}
                                <h3 class="product-sku">{{$filho->sku}}
                                    <span class="badge badge-danger ml-2" @if($filho->is_avaliable) style="display:none;" @endif>Indisponível</span>
                                </h3>
                                <span class="unsaved"><i class="fas fa-exclamation-circle mr-2"></i> Salve as alterações</span>
                                <button class="btn btn-action" type="button" data-toggle="collapse" data-target="#collapse_{{ $key }}">
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                            </div>
                            <div id="collapse_{{ $key }}" class="collapse" data-parent="#accordion-config">
                                <form id="form_{{ $key }}">
                                    <input type="hidden" name="product_id" value="{{ $filho->id }}">
                                    <div class="card-body mb-4">
                                        {{-- NOME DO PRODUTO --}}
                                        @if($loop->count > 1)
                                            <div class="form-group row">
                                                <label for="name_{{ $key }}" class="col-sm-3 col-form-label">Nome</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="nome" data-parent="#card_form_{{ $key }}" id="name_{{ $key }}" class="form-control" required max="255" value="{{ $filho->nome }}" >
                                                    <span class="invalid-feedback" id="info_name_{{ $key }}"><strong></strong></span>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- QUANTIDADE --}}
                                        <div class="form-group row">
                                            <label for="quantity_{{ $key }}" class="col-sm-3 col-form-label">Quantidade</label>
                                            <div class="col-sm-7">
                                                <input type="number" name="quantidade" data-parent="#card_form_{{ $key }}" id="quantity_{{ $key }}" class="form-control" required min="0"  value="{{ $filho->quantidade }}" >
                                                <span class="invalid-feedback" id="info_quantity_{{ $key }}"><strong></strong></span>
                                            </div>
                                        </div>
                                        {{-- PREÇO --}}
                                        <div class="form-group row">
                                            <label for="price_{{ $key }}" class="col-sm-3 col-form-label">Preço (R$)</label>
                                            <div class="col-sm-7">
                                                <input type="number" name="preco" data-name="preço" data-parent="#card_form_{{ $key }}" id="price_{{ $key }}" class="form-control" required min="0" step="0.01"  value="{{ $filho->preco }}" >
                                                <span class="invalid-feedback" id="info_price_{{ $key }}"><strong></strong></span>
                                                @if($loop->first && $loop->count > 1)
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" data-parent="#card_form_{{ $key }}" value="1" name="same_price" id="same_price">
                                                        <label class="custom-control-label" for="same_price">Aplicar o preço em todas as configurações</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                       
                                        {{-- DETALHES --}}
                                        @if($filho->atributos()->where('is_configurable', 1)->exists())
                                            <div class="collapse" id="details_{{ $key }}">
                                                @foreach($filho->atributos()->where('is_configurable', 1)->get() as $atr) 
                                                    <div class="form-group row">
                                                        <span class="col-sm-3 col-form-label">{{$atr->nome}}</span>
                                                        <div class="col-sm-7">
                                                            <span class="form-control-plaintext">
                                                                @if($atr->tipoDeDados->frontend_type_label == "Cor")
                                                                    {{$filho->atributos()->where('atributos.id', $atr->id)->first()->pivot->label}}
                                                                @else 
                                                                    {{$filho->atributos()->where('atributos.id', $atr->id)->first()->pivot->valor}}
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                        {{-- AÇÕES --}}
                                        <div class="form-row">
                                            <div class="col-7 offset-3">
                                                <button type="button" class="btn btn-primary btn-check" onclick="checkFormGeneralAttributes('#form_{{ $key }}')" disabled>Salvar</button>
                                                @if($filho->atributos()->where('is_configurable', 1)->exists())
                                                    <button type="button" data-toggle="collapse" data-target="#details_{{$key}}" class="btn btn-link">Detalhes</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
                 
                {{-- DESCRIÇÃO DO PRODUTO --}}
                <div class="group-info">
                    <div class="group-info-header">
                        <h2 class="group-info-title">Descrição</h2>
                        <button class="btn btn-action ml-1" data-toggle="modal" data-target="#modal_form_description">
                            <i class="fas fa-edit"></i> 
                        </button>
                    </div>
                    <div class="group-info-content">
                        <pre id="product-descricao" class="product-description">@if($produto->descricao) {{$produto->descricao}} @else O produto não possui uma descrição @endif</pre>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- EDITAR NOME--}}
    <div class="modal" tabindex="-1" role="dialog" id="modal_form_name">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-4">
                    <form id="form_name">
                        <h3 class="h4 mb-4">Editar nome</h3>
                        <div class="form-group">
                            <label for="nome">Novo nome</label>
                            <input 
                            type="text" 
                            class="form-control" 
                            required 
                            value="{{ $produto->nome }}"
                            maxlength="255"
                            name="nome"
                            id="nome">
                            <span class="invalid-feedback" id="info_nome">
                                <strong>
                                   Digite um nome válido
                                </strong>
                            </span>
                        </div>
                        <p class="m-0 text-gray-600" style="font-weight:600;">Lembre-se:</p>
                        <ul class="list-unstyled text-muted small mb-4">
                            <li>O nome deve ter no máximo 60 caracteres</li>
                            <li>Deve conter apenas letras, números e espaços.</li>
                        </ul>
                        <button type="button" onclick="checkFormGeneralAttributes('#form_name', '#update_nome')" class="btn btn-lg btn-success">Atualizar</button>
                        <button type="button" class="btn btn-lg btn-outline-secondary ml-md-2" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- EDITAR DESCRIÇÃO --}}
    <div class="modal" tabindex="-1" role="dialog" id="modal_form_description">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body p-4">
                    <form id="form_description">
                        <h3 class="h4 mb-4">Editar descrição</h3>
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea name="descricao" id="descricao" class="form-control" rows="7">{{ $produto->descricao }}</textarea>
                            <span class="invalid-feedback" id="info_nome">
                                <strong>
                                   Digite uma descrição válida
                                </strong>
                            </span>
                        </div>
                        <button type="button" onclick="checkFormGeneralAttributes('#form_description', '#update_descricao')" class="btn btn-lg btn-success">Atualizar</button>
                        <button type="button" class="btn btn-lg btn-outline-secondary ml-md-2" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pl-custom-scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $("#accordion-config input").on('change', function(){
            let parent = $($(this).data('parent'));
            parent.find('.unsaved').show();
            parent.find('.btn-check').prop('disabled', false);
        });
        $("[name='promo_value']").on('change', function(){
            var relatedInputs = $(this).siblings().find('input');
            if($(this).val() == 0)
                relatedInputs.val("");
            else 
                relatedInputs.get(0).focus();
        });
        $("[name='promo_starts_at']").on('change', function() {
            let value = $(this).val();
            $(this).parent().next().find('[name="promo_ends_at"]').prop('min', addDays(value, 1));
        })
        $("[name='is_visible']").on('change', function(){
            let isChecked = $(this).prop('checked') ? 1 : 0;
            updateGeneralAttribute('is_visible', isChecked, true);
        })
        function checkFormGeneralAttributes(formID) {
            let form = $(formID);
            form.addClass("was-validated");
            // RECUPERANDO OS VALORES DOS INPUTS
            let inputs = $("input:enabled, select, textarea", form);

            //VALIDANDO OS CAMPOS
            if(validate(inputs)) updateMultipleGeneralAttributes(form);
        }
        function updateGeneralAttribute(name, value, cascade = false) {
            $.ajax({
                url: "{{ route('produto.updateGeneralAttribute', [$produto->id]) }}",
                type: "PUT",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'name' : name, 
                    'value' : value, 
                    'cascade' : cascade,
                },
                error: function(xhr) {
                    console.log(xhr.responseText)
                },
            });
        }
        function updateMultipleGeneralAttributes(form) {
            let formData = new FormData(form.get(0));
            formData.append('_method', 'PUT');
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            if(!formData.has('product_id')) formData.append('product_id', {{ $produto->id }});
            $.ajax({
                url: "{{ route('produto.updateMultipleGeneralAttributes') }}",
                type: "POST",
                data: formData,
                processData:false,
                contentType:false,
                statusCode: {
                    400: function(xhr) {
                        let errors = JSON.parse(xhr.responseText);
                        Object.keys(errors).forEach(function(item){
                            let input = form.find('[name="'+item+'"]').addClass('is-invalid');
                            input.next().find('strong').html(errors[item]);
                        });
                    },
                    404: function() {
                        alert('Desculpe, não foi possível completar sua requisição. Por favor, tente recarregar a página.')
                    }
                },
                success: function(response) {
                    Object.keys(response).forEach(function(item) {
                        $("#product-" + item).html(response[item]);
                        $("[name='"+item+"']").val(response[item]);
                    });
                    $("#modal_" + form.prop('id')).modal('hide');
                    let card = $("#card_" + form.prop('id'));
                    card.find('.unsaved').hide();
                    if(response.quantidade) {
                        if(response.quantidade == 0) {
                           card.find('.badge-danger').show();
                        } else {
                           card.find('.badge-danger').hide();
                        }
                    }
                    form.find('.is-invalid').removeClass('is-invalid');
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                },
                complete: function(){
                    form.removeClass('was-validated');
                },
            });
        }

        var mainPhoto = {{ $mainPhoto->id }};
        let newPhoto = document.getElementsByClassName('new-photo')[0];
        var asset = "{{ asset('storage') }}";
        $("#imagens").change(function(){
            setPhotos($(this).prop('files'));
        })
        $(document).ready(function () { 
            $("#preview-photos").sortable({
                revert: true,
                items: '.show-photo',
                stop: function(event, ui) {
                    let first = $(".show-photo").first();
                    idPhoto = first.attr('data-id-photo');
                    if(idPhoto != mainPhoto) {
                        setMainPhoto(idPhoto);
                    }
                },
            });
            if($(".show-photo").length <= 1) {
                $("#preview-photos").sortable('disable');
            }
            $(".collapse").first().collapse('show');
        });
        function setPhotos(files) {
            if(!validatePhotos(files)) return;
            var formData = new FormData();
            for(var i = 0; i < files.length; i++) {
                formData.append('photos[]', files[i]);
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('produto.setPhotosToDatabase', [$produto->id]) }}",
                type: "POST",
                data: formData,
                contentType:false,
                cache: false,
                processData: false,
                success: function(response){
                    showPhotos(response);
                    $("#imagens").val("");
                },
                error: function(xhr) {
                    console.log(xhr);
                },
                
            });
        }
        function showPhotos(response) {
            let path = response.path;
            // CARREGANDO AS FOTOS
            path.forEach(function(item){
                //Container de exibição da imagem
                let col = document.createElement('div');
                col.classList.add('col-6', 'col-sm-auto', 'show-photo');
                col.setAttribute('data-id-photo', item[1]);
                col.innerHTML = "<div class='listable-product-image img-sm thumb'></div>";

                //Imagem do produto
                let img = document.createElement('img')
                img.src = asset + "/" + item[0]; 
                img.classList.add('img-fluid')

                //Botão de delete
                let deleteContainer = document.createElement('div');
                deleteContainer.classList.add('delete-photo');
                deleteContainer.innerHTML =
                "<button type='button' class='btn btn-danger' onclick='deletePhoto("+item[1]+", this)'>"+
                    '<i class="fas fa-trash-alt"></i>'+
                "</button>";

                col.appendChild(deleteContainer);
                col.querySelector('.thumb').appendChild(img);
                document.getElementById('preview-photos').insertBefore(col, newPhoto)
            })
            // HABILITANDO O ORDENAMENTO
            if($(".show-photo").length > 1) {
                $(".show-photo").first().removeClass("only-photo");
                $("#preview-photos").sortable('enable');
            }
        }
        function setMainPhoto(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('produto.updateMainPhoto', [$produto->id]) }}",
                type: "PUT",
                data: {
                    'idPhoto': id,
                },
                beforeSend: function() {
                    $("#preview-photos").sortable('disable');
                },
                success: function(response) {
                    mainPhoto = response;
                },
                error: function(xhr) {
                    console.log(xhr);
                },
                complete: function() {
                    $("#preview-photos").sortable('enable');
                }
            });
        }
        function deletePhoto(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route("produto.destroyPhotoFromDatabase") }}',
                method: 'DELETE',
                datatype: "JSON",
                data: {
                    idPhoto: id,
                },
                success: function(response) {
                    // DELETANDO O CONTAINER DA FOTO
                    $("[data-id-photo='"+id+"']").remove()

                    // ATUALIZANDO A FOTO PRINCIPAL
                    if(response.hasMain == 0) {
                        let first = $(".show-photo").first();
                        if(first.length > 0) setMainPhoto(first.attr('data-id-photo'));
                    }
                    //DESABILITANDO O ORDENAMENTO
                    if($(".show-photo").length <= 1) {
                        $(".show-photo").first().addClass('only-photo');
                        $("#preview-photos").sortable('disable');
                    } else {
                        $(".show-photo").first().removeClass('only-photo');
                    }
                },
                error: function(xhr, status, code) {
                    console.log(xhr);
                },
            })
        }
        $('.accordion .collapse').on('show.bs.collapse', function () {
            let chevron = $(this).siblings('.card-header').find('.btn-action i');;
            chevron.addClass('fa-chevron-up');
            chevron.removeClass('fa-chevron-down');
        })
        $('.accordion .collapse').on('hide.bs.collapse', function () {
            let chevron = $(this).siblings('.card-header').find('.btn-action i');;
            chevron.addClass('fa-chevron-down');
            chevron.removeClass('fa-chevron-up');
        })
    </script>
@endsection
