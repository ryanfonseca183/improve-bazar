@extends('layouts.auth')

@section('title', 'Iniciar sessão')

@section('content')
    <main>
        <div class="row hflex-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <form method="POST" action="{{ route('login') }}" id="login-form">
                @csrf
                    <div id="login-steps" class="carousel shadow p-0" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="form-step p-sm-5" id="step1">
                                    <h3 class="card-title">Olá</h3>
                                    <p>Para continuar, informe seu email</p>
                                    <div class="form-group mb-4">
                                        <label for="email">{{ __('Email') }}</label>
                                        <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <span class="invalid-feedback" id="info_email" role="alert">
                                            <strong>
                                                @error('email')
                                                    {{ $message }}
                                                @enderror
                                            </strong>
                                        </span>
                                    </div>
                                    <button type="button" onclick="checkGroupValidity('step1', 'next')" class="btn btn-primary btn-block mb-3">
                                        {{ __('Próximo') }}
                                    </button>
                                    <p class="text-center mb-0">
                                        Ainda não tem uma conta? <br>
                                        <a href="{{ route('register') }}">
                                            {{ __('Cadastrar') }}
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="form-step p-sm-5">
                                    <span class="badge badge-light badge-email">
                                        <span id="show-email"></span>
                                        <a href="#login-steps" data-slide="prev">
                                            <i class="fas fa-times"></i>
                                        </a>
                                    </span>
                                    <h3 class="card-title">Quase lá</h3>
                                    <p>Agora informe a sua senha</p>
                                    <div class="form-group"  id="step2">
                                        <label for="password">{{ __('Senha') }}</label>
                                        <input id="password" type="password" class="form-control form-control-lg mb-2 @error('password') is-invalid @enderror" name="password" minlength="8" required autocomplete="current-password" data-name="senha" >
                                        <span class="invalid-feedback" id="info_password" role="alert">
                                            <strong>
                                                @error('password')
                                                    {{ $message }}
                                                @enderror
                                            </strong>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="remember" class="custom-control-input" id="remember"  checked>
                                            <label class="custom-control-label" for="remember">Mantenha-me conectado</label>
                                        </div>
                                    </div>
                                    <button type="button" onclick="checkGroupValidity('step2', 'submit', 'login-form')" class="btn btn-success btn-block mb-2">
                                        {{ __('Logar') }}
                                    </button>
                                    <a class="btn btn-link w-100" href="{{ route('password.request') }}">
                                        {{ __('Esqueceu sua senha?') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection

@section('pl-custom-scripts')
    <script>
        $(function() {
            $("#show-email").html($("#email").val());
        })
        $("#email").on('input', function() {
            $("#show-email").html($("#email").val());
        })
    </script>
@endsection
