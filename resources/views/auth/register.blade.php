@extends('layouts.auth')

@section('title', 'Cadastrar conta')
@section('pl-custom-styles')
    <style>
        .background {
            position: absolute;
            width: 100vw;
            height: 100vh;
            left:0;
            top: 0;
            background-image: url('{{ asset("img/background-left.png") }}'), url('{{ asset("img/background-right.png") }}');
            background-repeat: no-repeat;
            background-size: 50%, 50%;
            background-position: bottom left, top right;
        }
        #form-container {
            background:white;
            min-height: 450px;
        }
        .logo {
            padding-top: 1rem;
        }
       .logo a{
            width: 50%;
        }
       .logo a img {
            width: 100%;
            height: auto;
        }
        @media only screen and (min-width: 768px) {
            .logo {
                padding-top: 0;
                background-color: var(--amarelo-claro);
            }
            .logo  a{
                width: 75%;
                height: auto; 
            }
            .background {
                background-size: 25%, 25%;
            }
        }
    </style>
@endsection
@section('content')
    <div class="background"></div>
    <main>
        <div class="row no-gutters hflex-center" style="height: 100vh;">
            <div class="col-lg-10 shadow">
                <div id="form-container" class="row no-gutters">
                    <div class="col-md-6 logo hflex-center">
                        <a href="{{ route('shop.home') }}" title="Página inicial da loja">
                            <img src="{{ asset('img/1 gra.png') }}" alt="">
                        </a>
                    </div>
                    <div class="col-md-6 vflex-center">
                        <form method="POST" action="{{ route('register') }}" id="user-cad" class="needs-validation w-100" novalidate>
                            @csrf
                            <div id="form-steps" class="carousel p-0" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    <div class="carousel-item @if(!$errors->any() || $errors->has('name')) active @endif">
                                        <div class="form-step p-sm-5" id="step1">
                                            <h3 class="card-title">Bem vindo</h3>
                                            <p>Como devemos te chamar?</p>
                                            <div class="form-group">
                                                <label for="name">{{ __('Nome completo') }}</label>
                                                <input 
                                                id="name" 
                                                name="name" 
                                                data-name="nome" 
                                                type="text" 
                                                class="form-control form-control-lg @error('name') is-invalid @enderror" 
                                                value="{{ old('name') }}" 
                                                required 
                                                autocomplete="name" 
                                                autofocus>
                                                <span class="invalid-feedback" id="info_name" role="alert">
                                                    <strong>
                                                        @error('name')
                                                            {{ $message }}
                                                        @enderror
                                                    </strong>
                                                </span>
                                            </div>
                                            <button type="button" onclick="checkGroupValidity('step1', 'next')" class="btn btn-primary btn-block mb-3">
                                                {{ __('Próximo') }}
                                            </button>
                                            <p class="text-center mb-0">
                                                Já possui uma conta? <br>
                                                <a href="{{ route('login') }}">
                                                    {{ __('Entrar') }}
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="carousel-item @if(!$errors->has('name') && $errors->has('cpf')) active @endif">
                                        <div class="form-step p-sm-5" id="step2">
                                            <a href="#form-steps" data-slide="prev" class="d-block mb-3">
                                                <i class="fas fa-long-arrow-alt-left mr-2"></i>
                                                <span>Anterior</span>
                                            </a>
                                            @php
                                                if($errors->any())
                                                    $strings = explode(" ", old('name'));
                                            @endphp
                                            <h3 class="card-title">Prazer, <span id="nome_user">@isset($strings[0]){{  $strings[0] }}@endisset</span></h3>
                                            <p>Qual o seu número de CPF?</p>
                                            <div class="form-group">
                                                <label for="cpf">{{ __('CPF') }}</label>
                                                <input 
                                                id="cpf" 
                                                name="cpf" 
                                                type="text"
                                                class="form-control form-control-lg @error('cpf') is-invalid @enderror"
                                                value="{{ old('cpf') }}" 
                                                required 
                                                minlength="14" 
                                                autocomplete="cpf" 
                                                autofocus >
                                                <span class="invalid-feedback" id="info_cpf" role="alert">
                                                    <strong>
                                                        @error('cpf')
                                                            {{ $message }}
                                                        @enderror
                                                    </strong>
                                                </span>
                                            </div>
                                            <button type="button" onclick="checkGroupValidity('step2', 'next')" class="btn btn-primary btn-block mb-3">
                                                {{ __('Próximo') }}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="carousel-item @if(!($errors->has('name') && $errors->has('cpf')) && ($errors->has('email') || $errors->has('password'))) active @endif">
                                        <div class="form-step p-sm-5" id="step3">
                                            <a href="#form-steps" data-slide="prev" class="d-block mb-3">
                                                <i class="fas fa-long-arrow-alt-left mr-2"></i>
                                                <span>Anterior</span>
                                            </a>
                                            <h3 class="card-title">Quase lá</h3>
                                            <p>Como voce irá se autenticar no site?</p>
                                            <div class="form-group">
                                                <label for="email">{{ __('Email') }}</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                                <span class="invalid-feedback" id="info_email" role="alert">
                                                    <strong>
                                                        @error('email')
                                                            {{ $message }}
                                                        @enderror
                                                    </strong>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">{{ __('Senha') }}</label>
                                                <input id="password" data-name="senha" type="password" class="form-control @error('password') is-invalid @enderror" name="password" minlength="8" required autocomplete="new-password">
                                                <span class="invalid-feedback" id="info_password" role="alert">
                                                    <strong>
                                                        @error('password')
                                                            {{ $message }}
                                                        @enderror
                                                    </strong>
                                                </span>
                                            </div>
                                            <button type="button" onclick="checkGroupValidity('step3', 'submit', 'user-cad')" class="btn btn-success btn-block">
                                                {{ __('Cadastrar') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('pl-custom-scripts')
    <script type="text/javascript" src="{{ asset('js/dependencies/mask.js') }}"></script>
    <script>
        $("#cpf").mask('000.000.000-00');
        $("#name").change(function(){
            let val = $(this).val();
            let termino = val.indexOf(' ');
            if(termino == -1) termino = undefined; 
            val = val.slice(0, termino);
            $("#nome_user").html(" " + val);
        })
    </script>
@endsection
