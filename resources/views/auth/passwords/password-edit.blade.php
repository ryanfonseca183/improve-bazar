@extends('layouts.auth')

@section('title', 'Alterar senha')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6">
            <form action="{{ route('password.change') }}" method="POST" class="form-step p-sm-5 rounded shadow" id="update-password">
                @csrf 
                @method('PUT')
                <h1 class="h4 mb-3">Alterar senha</h1>
                <div class="form-row">
                    <div class="form-group col-lg">
                        <label for="password" >{{ __('Nova senha') }}</label>
                        <input id="password" type="password" data-name="senha" class="form-control @error('password') is-invalid @enderror" name="password" required minlength="8" autocomplete="new-password">
                        <span class="invalid-feedback" role="alert" id="info_password">
                            <strong>
                                @error('password')
                                    {{ $message }}
                                @enderror
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-lg">
                        <label for="password-confirm">{{ __('Confirmar senha') }}</label>
                        <input id="password-confirm" data-name="confirmar senha" type="password" minlength="8" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <span class="invalid-feedback" role="alert" id="info_password-confirm">
                            <strong>
                                @error('password_confirmation')
                                    {{ $message }}
                                @enderror
                            </strong>
                        </span>
                    </div>
                </div>
                <button type="button" onclick="checkFormValidity('update-password')" class="btn btn-primary btn-block mb-3">
                    {{ __('Alterar') }}
                </button>
                <h2 class="h6"><strong>Construa uma senha forte</strong></h2>
                <ul>
                    <li>Use letras, números e caracteres especiais</li>
                    <li>Combine letras maiúsculas e minúsculas</li>
                    <li>Não utilize nomes e datas</li>
                </ul>
            </form>
        </div>
    </div>
</div>

@endsection