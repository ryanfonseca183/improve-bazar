@extends('layouts.auth')

@section('title', 'Confirmação de senha')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6">
            <div class="form-step rounded shadow p-sm-5">
                <a href="{{ route('user.profile') }}" class="mb-3 d-block">
                    <i class="fas fa-long-arrow-alt-left mr-2"></i>
                    <span>Voltar</span>
                </a>
                <h1 class="h4 mb-2">Confirmação de senha</h1>
                <p class="mb-3">
                    {{ __('Por medidas de segurança, pedimos que confirme sua senha antes de continuar.') }}
                </p>
    
                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf
    
                    <div class="form-group">
                        <label for="password">{{ __('Senha') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
    
                    <div class="row justify-content-center">
                        <div class="col-lg order-last mt-3 mt-lg-0 order-lg-first">
                            <a class="btn btn-link btn-block" href="{{ route('password.request') }}">
                                {{ __('Esqueceu sua senha?') }}
                            </a>
                        </div>
                        <div class="col-lg">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Continuar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
