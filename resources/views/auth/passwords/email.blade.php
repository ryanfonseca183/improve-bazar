@extends('layouts.auth')

@section('title', 'Redefinir senha')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6">
            <form method="POST" class="form-step p-sm-5 shadow rounded" action="{{ route('password.email') }}" id="password-email">
                @csrf
                <a href="{{ route('login') }}" class="mb-3 d-block">
                    <i class="fas fa-long-arrow-alt-left mr-2"></i>
                    <span>Voltar</span>
                </a>
                <h1 class="h4 mb-3">Redefinir senha</h1>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        Um link foi enviado ao seu email de recuperação!
                    </div>
                @endif
                <div class="form-group">
                    <label for="email">{{ __('Email da conta') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <span class="invalid-feedback" role="alert" id="info_email">
                        <strong>
                            @error('email')
                                {{ $message }}
                            @enderror
                        </strong>
                    </span>
                </div>
                <button type="button" onclick="checkFormValidity('password-email')" class="btn btn-primary btn-block ">
                    {{ __('Enviar link') }}
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
