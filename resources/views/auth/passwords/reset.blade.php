@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6">
            <form method="POST" action="{{ route('password.update') }}" novalidate class="form-step rounded shadow p-sm-5" id="reset-password">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                <span class="badge badge-light badge-email">
                    <span id="show-email">{{ $email}}</span>
                </span>
                <h1 class="h4 mb-0">Tudo certo</h1>
                <p class="mb-4">Agora nos informe sua nova senha</p>
                <div class="form-row">
                    <div class="form-group col-lg">
                        <label for="password" >{{ __('Nova senha') }}</label>
                        <input id="password" type="password" data-name="senha" class="form-control @error('password') is-invalid @enderror" name="password" required minlength="8" autocomplete="new-password">
                        <span class="invalid-feedback" role="alert" id="info_password">
                            <strong>
                                @error('password')
                                    {{ $message }}
                                @enderror
                            </strong>
                        </span>
                    </div>
                    <div class="form-group col-lg">
                        <label for="password-confirm">{{ __('Confirmar senha') }}</label>
                        <input id="password-confirm" data-name="confirmar senha" type="password" minlength="8" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <span class="invalid-feedback" role="alert" id="info_password-confirm">
                            <strong>
                                @error('password_confirmation')
                                    {{ $message }}
                                @enderror
                            </strong>
                        </span>
                    </div>
                </div>
                <button type="button" onclick="checkFormValidity('reset-password')" class="btn btn-primary btn-block">
                    {{ __('Redefinir senha') }}
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
