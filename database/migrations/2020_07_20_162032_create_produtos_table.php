<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subcategorias_id')->constrained()->onDelete('cascade');
            $table->string('nome', 255);
            $table->decimal('preco', 12, 2);
            $table->string('sku');
            $table->text('descricao')->nullable();
            $table->unsignedSmallInteger('quantidade');
            $table->boolean('is_visible')->default(1);
            $table->boolean('is_avaliable')->default(1);
            $table->foreignId('produtos_id')->nullable()->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
