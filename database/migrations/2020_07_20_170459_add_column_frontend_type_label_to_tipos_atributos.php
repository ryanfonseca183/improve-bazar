<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFrontendTypeLabelToTiposAtributos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipos_atributos', function (Blueprint $table) {
            $table->string('frontend_type_label', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipos_atributos', function (Blueprint $table) {
            $table->dropColumn('frontend_type_label');
        });
    }
}
