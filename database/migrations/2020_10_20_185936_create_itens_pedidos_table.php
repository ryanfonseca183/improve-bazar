<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItensPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_pedidos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pedidos_id')->constrained()->onDelete('cascade');
            $table->foreignId('produtos_id')->constrained()->onDelete('cascade');
            $table->unsignedMediumInteger('quantidade');
            $table->decimal('preco', 12, 2);
            $table->decimal('desconto', 12, 2);
            $table->decimal('total', 12, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_pedidos');
    }
}
