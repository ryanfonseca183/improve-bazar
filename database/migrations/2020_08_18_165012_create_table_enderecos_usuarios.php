<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEnderecosUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos_usuarios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('users_id')->constrained()->onDelete('cascade');
            $table->char('cep', 9);
            $table->char('uf_sigla', 2);
            $table->string('cidade');
            $table->string('bairro');
            $table->string('rua');
            $table->unsignedTinyInteger('numero');
            $table->string('complemento')->nullable();
            $table->char('telefone', 13)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos_usuarios');
    }
}
