<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atributos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subcategorias_id')->constrained()->onDelete('cascade');
            $table->string('nome', '60');
            $table->string('label', '60');
            $table->foreignId('tipos_atributos_id')->constrained()->onDelete('cascade');
            $table->boolean('is_required');
            $table->boolean('is_definable')->default(1);
            $table->boolean('is_configurable');
            $table->boolean('is_visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atributos');
    }
}
