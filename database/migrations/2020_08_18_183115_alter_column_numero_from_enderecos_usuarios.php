<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnNumeroFromEnderecosUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enderecos_usuarios', function (Blueprint $table) {
            $table->unsignedSmallInteger('numero')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enderecos_usuarios', function (Blueprint $table) {
            $table->unsignedTinyInteger('numero')->change();
        });
    }
}
