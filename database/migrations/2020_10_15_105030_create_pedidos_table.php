<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('users_id')->constrained()->onDelete('cascade');
            $table->decimal('subTotal', 12, 2);
            $table->decimal('frete', 10, 2);
            $table->decimal('total', 12, 2);
            $table->string('status', 60);
            $table->string('rastreio', 60);
            $table->foreignId('metodos_pagamento_id')->constrained('metodos_pagamento')->onDelete('cascade');
            $table->tinyInteger('num_parcelas')->nullable();
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('delivery_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
