<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosHasAtributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_has_atributos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('produtos_id')->constrained()->onDelete('cascade');
            $table->foreignId('atributos_id')->constrained()->onDelete('cascade');
            $table->string('valor');
            $table->string('label')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos_has_atributos');
    }
}
