<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSacolasHasProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sacolas_has_produtos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('produtos_id')->constrained()->onDelete('cascade');
            $table->foreignId('sacolas_id')->constrained()->onDelete('cascade');
            $table->unsignedSmallInteger('quantidade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sacolas_has_produtos');
    }
}
