<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\MetodoPagamento;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MetodoPagamento::insert([
            ['nome' => 'Cartão de crédito', 'label' => 'cartao_de_credito', 'icone' =>'fab fa-cc-mastercard '],
            ['nome' => 'Cartão de débito', 'label' => 'cartao_de_debito', 'icone' =>'fas fa-credit-card'],
            ['nome' => 'Boleto bancário', 'label' => 'boleto_bancario', 'icone' =>'fas fa-barcode']
        ]);
    }
}
