<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Categoria;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Categoria::insert([
        ['nome' => 'Tecnologia'],
        ['nome' => 'Eletrodomésticos'],
        ['nome' => 'Moda'],
        ['nome' => 'Brinquedos'],
        ['nome' => 'Móveis'],
      ]);
    }
}
