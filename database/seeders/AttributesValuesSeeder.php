<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Subcategoria;
use App\Atributos;

class AttributesValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $atributos = Subcategoria::where('nome', 'Celulares')->first()->atributos;

        $atributos->where('nome', 'Marca')->first()->valoresAtributos()->createMany([
            ['valor' => 'Samsung'],
            ['valor' => 'Apple'],
            ['valor' => 'Xiaomi'],
            ['valor' => 'Motorola'],
        ]);

        $atributos->where('nome', 'Linha')->first()->valoresAtributos()->createMany([
            ['valor' => 'Galaxy J'],
            ['valor' => 'iPhone'],
            ['valor' => 'Redmi'],
            ['valor' => 'Moto G'],
        ]);

        $atributos->where('nome', 'Modelo')->first()->valoresAtributos()->createMany([
            ['valor' => 'J5 Prime'],
            ['valor' => 'J6 Metal'],
            ['valor' => 'J7 Duos'],
            ['valor' => 'iPhone 5'],
            ['valor' => 'iPhone 6'],
            ['valor' => 'iPhone 7'],
            ['valor' => 'Note 9'],
            ['valor' => 'Note 10'],
            ['valor' => 'Note 11'],
            ['valor' => 'G5 Plus'],
            ['valor' => 'G5 Play'],
            ['valor' => 'G5 Power'],
        ]);

        $atributos->where('nome', 'Memória Interna')->first()->valoresAtributos()->createMany([
            ['valor' => '128 GB'],
            ['valor' => '64 GB'],
            ['valor' => '32 GB'],
            ['valor' => '16 GB'],
        ]);

        $atributos->where('nome', 'Memoria RAM')->first()->valoresAtributos()->createMany([
            ['valor' => '2 GB'],
            ['valor' => '4 GB'],
            ['valor' => '6 GB'],
            ['valor' => '8 GB'],
        ]);

        $atributos->where('nome', 'Cor do celular')->first()->valoresAtributos()->createMany([
            ['valor' => '#000000', 'label' => 'Preto'],
            ['valor' => '#ff0000', 'label' => 'Vermelho'],
            ['valor' => '#008cff', 'label' => 'Azul'],
        ]);
    }
}
