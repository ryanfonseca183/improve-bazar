<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AttributesDataTypeSeeder::class,
            UserAdminSeeder::class,
            CategorySeeder::class,
            SubcategorySeeder::class,
            AttributesSeeder::class,
            AttributesValuesSeeder::class,
            PaymentMethodSeeder::class,
        ]);
    }
}
