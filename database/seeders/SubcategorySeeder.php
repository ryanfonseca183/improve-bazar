<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Categoria;
use App\Subcategoria;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = Categoria::all();

        $categorias->where('nome', 'Tecnologia')->first()->subcategorias()->createMany([
            ['nome' => 'Celulares'],
            ['nome' => 'Computadores'],
            ['nome' => 'Televisões'],
            ['nome' => 'Notebooks'],
            ['nome' => 'Câmeras'],
            ['nome' => 'Video Games'],
        ]);

        $categorias->where('nome', 'Eletrodomésticos')->first()->subcategorias()->createMany([
            ['nome' => 'Maquina de Lavar'],
            ['nome' => 'Geladeiras'],
            ['nome' => 'Ventiladores'],
            ['nome' => 'Aquecedores de ar'],
            ['nome' => 'Forno e Fogões'],
            ['nome' => 'Liquidificadores'],
        ]);

        $categorias->where('nome', 'Moda')->first()->subcategorias()->createMany([
            ['nome' => 'Blusas'],
            ['nome' => 'Calças'],
            ['nome' => 'Casacos'],
            ['nome' => 'Moletons'],
            ['nome' => 'Chapéus e Bonés'],
            ['nome' => 'Sapatos'],
        ]);

        $categorias->where('nome', 'Brinquedos')->first()->subcategorias()->createMany([
            ['nome' => 'Bonecos'],
            ['nome' => 'Casinhas'],
            ['nome' => 'Carrinhos'],
            ['nome' => 'Pelúcias'],
            ['nome' => 'Lançadores de brinquedo'],
            ['nome' => 'Piscinas e Infláveis'],
        ]);

        $categorias->where('nome', 'Móveis')->first()->subcategorias()->createMany([
            ['nome' => 'Jardim'],
            ['nome' => 'Banheiro'],
            ['nome' => 'Quarto'],
            ['nome' => 'Escritório'],
            ['nome' => 'Cozinha'],
            ['nome' => 'Sala de estar'],
        ]);

    }
}
