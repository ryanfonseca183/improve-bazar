<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Subcategoria;
use App\TipoAtributo;


class AttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = TipoAtributo::all();
        $typeText = $tipos->where('frontend_type', 'text')->first()->id;
        $typeColor = $tipos->where('frontend_type', 'color')->first()->id;
        $typeRadio = $tipos->where('frontend_type', 'radio')->first()->id;

        Subcategoria::where('nome', 'Celulares')->first()->atributos()->createMany([
            ['nome' => 'Marca', 'label' => 'marca', 'tipos_atributos_id' => $typeText, 'is_required' => 1, 'is_configurable' => 0, 'is_visible' => 1],
            ['nome' => 'Linha', 'label' => 'linha', 'tipos_atributos_id' => $typeText, 'is_required' => 1, 'is_configurable' => 0, 'is_visible' => 1],
            ['nome' => 'Modelo', 'label' => 'modelo', 'tipos_atributos_id' => $typeText, 'is_required' => 1, 'is_configurable' => 0, 'is_visible' => 1],
            ['nome' => 'Dual Chip', 'label' => 'dual_chip', 'tipos_atributos_id' => $typeRadio, 'is_required' => 1, 'is_configurable' => 0, 'is_visible' => 1],
            ['nome' => 'Memória Interna', 'label' => 'memoria_interna', 'tipos_atributos_id' => $typeText, 'is_required' => 1, 'is_configurable' => 1, 'is_visible' => 1],
            ['nome' => 'Memoria RAM', 'label' => 'memoria_ram', 'tipos_atributos_id' => $typeText, 'is_required' => 1, 'is_configurable' => 1, 'is_visible' => 1],
            ['nome' => 'Cor do celular', 'label' => 'cor_do_celular', 'tipos_atributos_id' => $typeColor, 'is_required' => 1, 'is_configurable' => 1, 'is_visible' => 1],
        ]);
    }
}
