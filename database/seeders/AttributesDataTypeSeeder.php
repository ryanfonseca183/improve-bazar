<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\TipoAtributo;

class AttributesDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoAtributo::insert([
            ['backend_type' => 'VARCHAR', 'frontend_type' => 'text', 'frontend_type_label' => 'Caractere'],
            ['backend_type' => 'VARCHAR', 'frontend_type' => 'color', 'frontend_type_label' => 'Cor'],
            ['backend_type' => 'BOOLEAN', 'frontend_type' => 'radio', 'frontend_type_label' => 'Lógico'],
        ]);
    }
}
