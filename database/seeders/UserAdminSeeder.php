<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use App\Sacola;
use App\Favorito;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => env('ADMIN_NAME', 'Administrador'),
            'primeiro_nome' => 'Admin',
            'cpf' => "000.000.000-00",
            'email' => env('ADMIN_EMAIL', 'admin@admin'),
            'password' => bcrypt(env('ADMIN_PASSWORD', 'improve@admin')),
            'is_admin' => 1
        ]);
        Sacola::create(['users_id' => $user->id]);
        Favorito::create(['users_id' => $user->id]);
    }
}
