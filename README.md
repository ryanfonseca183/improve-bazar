<div align="center">
  <h1><img src="public/img/1 gra.png" alt="Animavita"></h1>
  Uma loja com visual minimalista, elegante e intuitivo para venda de produtos diversos.
</div>
<br/><br/>
<div align="center">
  <kbd>
    <img src="public/img/prints/img0.png" width="400" alt="Catálogo de produtos">
  </kbd>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <kbd>
    <img src="public/img/prints/img1.png" width="400" alt="Detalhes do produto">
  </kbd>
  <br/>
  <br/>
  <kbd>
    <img src="public/img/prints/img2.png" width="400" alt="Sacola de compras">
  </kbd>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <kbd>
    <img src="public/img/prints/img3.png" width="400" alt="Checkout">
  </kbd>
</div>

## **Sobre** 

Improve Bazar é um protótipo de loja virtual de departamentos que possibilita a venda de produtos diversos, apresentado ao Instituto Federal de Educação, Ciência e Tecnologia de Minas Gerais – Campus Formiga como parte das exigências para a obtenção do título de Técnico em Informática.

A aplicação conta com um painel de controle, para que o usuário com perfil de administrador possa gerenciar os produtos anunciados, e a loja propriamente dita, que permite a realização de todo o processo de compra. Vale observar que, como se trata de um protótipo, a efetivação da compra através de plataformas de pagamentos online não foi implementada.

## **Tecnologias utilizadas** 

- Laravel
- Bootstrap
- jQuery
- Mysql
- DataTable
- Carbon


## **Instalação**
Para obter uma copia do projeto e executa-la na sua máquina, siga as instruções abaixo:  
- Primeiro, gere um clone da aplicação utilizando os comandos abaixo
```
  1. git clone https://gitlab.com/ryanfonseca183/improve-bazar.git
  2. cd improve-bazar
```

- Instale as dependencias do projeto
```
  1. composer install
  2. npm install
```
- Renomeie o arquivo .env-exemple para .env e defina as varíaveis de ambiente
- Para informar as credenciais de acesso do usuário administrador, defina os valores para as variáveis de ambiente ADMIN_*
- Gere a chave do projeto
```
  1. php artisan key:generate
```
- Execute os migrations e seeders
```
  1. php artisan migrate
  2. php artisan db:seed
  3. php artisan storage:link
```

