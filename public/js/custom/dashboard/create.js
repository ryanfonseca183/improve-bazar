
                        
/*------------------------Atributos data:-------------------------

data-option-value: 
Indica o input alvo. No caso de list items, indica também o valor
selecionado

data-option-name: 
Indica os elementos de exibição. No caso de list items, indica também
o nome de exibição.

data-submit: 
Indica se o elemento deve chamar uma função do ajax, submeter um 
formulário ou só passar para a próxima etapa.

------------------------------------------------------------------*/

function loadInputValue(element) {
    let optionValue = element.getAttribute('data-option-value');

    if(optionValue != null) {
       var inputTarget = document.getElementById(optionValue);
       inputTarget.value=element.value
    }
    let optionName = element.getAttribute('data-option-name');
    if(optionName != null) {
       var showTarget = document.getElementsByClassName(optionName);
       for(let j = 0; j < showTarget.length; j++) {
           showTarget[j].innerHTML = element.value;
       }
    }
}
function loadLiValue(element, getResources) {
    let parent = element.getAttribute('data-parent');
    if(parent != null) {
        let li = $("#"+ parent + " li.active");
        li.removeClass("active");
    }
    element.classList.add('active');
   
    let idResource = null;

    let optionValue = element.getAttribute('data-option-value');
    if(optionValue != null && optionValue.indexOf('/') != -1) {
        let index = optionValue.indexOf('/');

        //Recupera o valor
        let target = optionValue.slice(0, index);

        //Recupera o alvo
        idResource = optionValue.slice((index+1));
        let inputTarget = document.getElementById(target);
        
        inputTarget.value= idResource;
    
    } else if(optionValue != null) {
        idResource = optionValue;
    }
    let optionName = element.getAttribute('data-option-name');
    if(optionName != null && optionName.indexOf('/') != -1) {
        let index = optionName.indexOf('/');

        //Recupera os elementos de exibição
        let target = optionName.slice(0, index);

        //Recupera o nome de exibição
        let name = optionName.slice((index+1));

        showTarget = document.getElementById(target);
        showTarget.value = name;
        return name;
    }

    //Verifica se o atributo data-submit foi declarado
    var dataSubmit = element.getAttribute('data-submit');
    if(dataSubmit == "ajax") {
        getResources(idResource);
    } else if(dataSubmit != null && dataSubmit != "next") {
        $('#' + dataSubmit).submit();
    } else if(dataSubmit != null) {
        $('#carousel').carousel('next')
    } 
}



/*

Carrega os eventos para os elementos selecionáveis. Para que isso tenha 
efeito, o elemento deve possuir a classe option

*/
var option = document.getElementsByClassName('option');
for(let i =0; i < option.length; i++) {
    let element = option[i].tagName;
    switch(element) {
        case "LI":
            option[i].addEventListener('click', function(){
                loadLiValue(this, null);
            });
        break;
        case "INPUT":
            loadInputValue(option[i]);
            option[i].addEventListener('change', function(){
                loadInputValue(this);
            });
        break;
        case "SELECT":
            loadSelectValue(option[i]);
            option[i].addEventListener('change', function(){
                loadSelectValue(this);
            });
        break;
    }
}