$.extend( $.fn.dataTable.defaults, {
    language: {
        "decimal":        "",
        "emptyTable":     "Nenhum registro cadastrado",
        "info":           "Mostrando _END_ de _TOTAL_ registros",
        "infoEmpty":      "Nenhum registro mostrado",
        "infoFiltered":   "(Filtrado de _MAX_ registros)",
        "infoPostFix":    "",
        "thousands":      ",",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Carregando dados...",
        "processing":     "Processando...",
        "search":         "Buscar",
        "zeroRecords":    "Nenhum registro encontrado",
        "paginate": {
            "first":      "Primeiro",
            "last":       "Ultimo",
            "next":       "Proximo",
            "previous":   "Anterior"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
  });


//Prepara o modal de exclusão, quando o usuário clica no botão
$('#modalDelete').on('show.bs.modal', function (e) {
    let form = document.getElementById('formDelete');
    var button = e.relatedTarget;
  
    //Recupera a posição da ultima barra, no action do form
    var str = form.action;
    var lastIndexOf = str.lastIndexOf("/") + 1;
  
    //Exclui todo caractere que vem após a ultima barra
    var substract = str.substr(0, lastIndexOf);
  
    //Adiciona um novo id ao action
    var newAction = substract + button.getAttribute('data-id');
    form.action = newAction;
});