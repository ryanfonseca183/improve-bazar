/* MENU DROPDOWN DE CATEGORIAS */
var toggler = document.getElementsByClassName('dropdown-toggler');
for(var i = 0; i < toggler.length; i++) {
   toggler[i].addEventListener('click', function() {
       $(this).next().toggle('fast');
       $(document.body).toggleClass('display-categories');
   })
}
var dropItem = document.getElementsByClassName('drop-item');
var submenu = document.getElementsByClassName('subdrop-item');

var dropItemActive;
var subDropItemActive;

function resetState(element, classe) {
   if(element != null) {
       element.classList.remove(classe);
   }
}
for(var i = 0; i < dropItem.length; i++) {
   let order = dropItem[i].getAttribute('data-order');
   if(order != null) {
       dropItem[i].addEventListener('mouseover', function(){
           resetState(dropItemActive, 'drop-item-active');
           resetState(subDropItemActive, 'd-block');

           //Define o dropdown-item ativo
           dropItemActive = this;
           this.classList.add('drop-item-active');

           //Mostra o subdropdown do item ativo
           let order = this.getAttribute('data-order');
           submenu = document.querySelector('.subdrop-item[data-order="'+ order +'"]');
           subDropItemActive =  submenu;
           submenu.classList.add('d-block');
       });
   } else {
       dropItem[i].addEventListener('mouseover', function(){
           resetState(dropItemActive, 'drop-item-active');
           resetState(subDropItemActive, 'd-block');
       });
   }
}
function validateCEP() {
    let cep = $("#cep")
    if(cep.val().length != 9) {
        cep.addClass("is-invalid");
        return;
    }
    $("#store_address").submit();
}

$(function(){
    $(".star-rating-control").each(function(){
        if($(this).val() == val) $(this).prop('checked', true);
    })
    loadStars($('.star-rating-control:checked'));

    $(".star-rating-control").on('change', function(){
        loadStars($(this))
    });
})
function loadStars(control) {
    let group = control.parent();
    let prevGroups = group.prevAll();
    let nextGroups = group.nextAll();
    prevGroups.each(function(index){
        $(".star-rating-label", $(this)).addClass("active");
    });
    nextGroups.each(function(index){
        $(".star-rating-label", $(this)).removeClass("active");
    });
}