(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  $("#pre-loader").addClass('pre-loader-disabled');

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

})(jQuery); // End of use strict

$('.carousel').on('slid.bs.carousel', checkitem);
$(document).ready(function () {   
    checkitem();
});
function checkitem()                        
{   
    var $this = $('.carousel');
    if ($('.carousel-inner .carousel-item:first').hasClass('active')) {
        $('.carousel-control-prev', $this).hide();
    } else if ($('.carousel-inner .carousel-item:last').hasClass('active')) {
        $('.carousel-control-prev', $this).show();
    } else {
        $('.carousel-control-prev', $this).show();
    }
}
function validatePhotos(files) {
    //Verifica se alguma foto foi selecionada
    if(files.length == 0) return false;

    //Verifica a quantidade de fotos selecionadas
    let photos = $(".show-photo");
    if((files.length + photos.length) > 10) {
        $("#info_fotos").addClass('d-block');
        $("#info_fotos strong").html("É permitido a seleção de no máximo 10 fotos.")
        return false;
    }

    //Verifica se as fotos possuem um formato permitido.
    let flag = true;
    var extPermitidas = ['jpg', 'jpeg', 'png', 'webp'];
    for(var i = 0; i < files.length; i++) {
        file = files[i];
        let type = typeof extPermitidas.find(function(ext){ 
                fileExt = file.name.split('.').pop();
                fileExt = fileExt.toLowerCase();
                return fileExt == ext; 
            });
                
        if(type == 'undefined') {
            $("#info_fotos").addClass('d-block');
            $("#info_fotos strong").html("Os formatos válidos são JPG, JPEG, PNG e WEBP");
            flag = false;
            break;
        }
    }
    if(flag) $("#info_fotos").removeClass('d-block');
    return flag;
}