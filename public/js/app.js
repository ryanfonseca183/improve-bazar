// Scroll to top button appear
$(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
    } else {
        $('.scroll-to-top').fadeOut();
    }
});
// Smooth scrolling using jQuery easing
$(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  $(function () {
    $('[data-toggle="popover"]').popover()
})

//Valida o campo nome do atributo e submete o formulário de cadastro
function checkGroupValidity(groupId, action, formId = null) {
    let group = $("#" + groupId);
    var formElements = $("input:visible:not([type=submit]):not([type=color]):not([type=button]):not([type=reset])", group);
    if(validate(formElements)) {
        if(action == "next") {
            $('.carousel').carousel('next');
        } else {
            $("#" + formId).submit();
        }
    }
    group.addClass('was-validated');
}
function checkFormValidity(formId) {
    let form = $("#" + formId);
    var formElements = $("input:not([type=hidden]):not([type=submit]):not([type=color]):not([type=button]):not([type=reset]), textarea", form);
    if(validate(formElements)) {
        form.submit();
    }
    form.addClass('was-validated');
}
function validate(inputs) {
    let flag = true;
    for(var i = 0; i < inputs.length; i++) {
        if(flag) {
            flag = isValid(inputs[i]);
        } else {
            isValid(inputs[i]);
        }
    }
    return flag;
}
function isValid(input) {
    var state = input.validity;
    let info = $("#info_" + input.id + " strong");
    if(state.valid) {
        info.html("");
        return true;
    } else {
        if(input.getAttribute('data-name') != null) {
            atrNome = input.getAttribute('data-name');
        }else if(input.name == null || input.name == "") {
            atrNome = input.id;
        } else {
            atrNome = input.name;
        }
        if (state.valueMissing) {
            info.html("O campo "+ atrNome +" é obrigatório!");
        } else if (state.stepMismatch) {
            info.html("O valor digitado precisa ser um número inteiro");
        } else if (state.rangeUnderflow) {
            info.html("O valor deve ser maior ou igual a " + input.min);
        }else if (state.rangeOverflow) {
            info.html("O valor deve ser menor ou igual a " + input.max);
        } else if (state.typeMismatch) {
            info.html("O valor digitado não corresponde ao tipo exigido.");
        } else if (state.tooLong) {
            info.html("O campo "+ atrNome +" deve ter no máximo "+input.maxLength+" caracteres!");
        } else if (state.tooShort) {
            info.html("O campo "+ atrNome +" deve ter no mínimo "+ input.minLength +" caracteres!");
        } else if (state.patternMismatch) {
            info.html("O valor digitado não corresponde ao padrão");
        } 
        return false;
    }
}
function format(amount, decimalCount = 2, decimal = ",", thousands = ".") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
}
function addDays(date, days) {
    var d = new Date(date);
    d.setDate(d.getDate() + days);
    d = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getUTCDate()).slice(-2);
    return d;
}
function removeURLParameter(url, parameter) {
    var urlparts = url.split('?');   
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);
        for (var i = pars.length; i-- > 0;) {    
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}
function consultarCEP(cep){
    if(cep.length != 9) return;
    $.ajax({
        type: 'GET',
        url: "http://viacep.com.br/ws/"+ cep +"/json/",
        beforeSend: function() {
            spinner.show('slow');
            toggleAddressControls(true);
            $(".btn-action").prop('disabled', true);
        },
        error: function(response) {
            address_cep_feedback.html('Por favor, informe um CEP válido');
            address_cep.addClass("is-invalid");
            cleanAddressControls();
        },
        success: function(data) { 
            if(!('erro' in data)){
                address_uf.val(data.uf);
                address_city.val(data.localidade);
                address_bairro.val(data.bairro);
                address_street.val(data.logradouro);
                address_complement.val(data.complemento);
                address_cep.removeClass("is-invalid");
            }else{ //CEP NAO ENCONTRADO
                address_cep.addClass('is-invalid');
                address_cep_feedback.html('CEP não encontrado. Por favor, tente novamente');
                cleanAddressControls();
            }
        },
        complete: function() {
            spinner.hide('slow');
            toggleAddressControls(false);
            $(".btn-action").prop('disabled', false);
        }
    });
}
function toggleAddressControls(bool) {
    addressControls.forEach(function(item) {
        $(item).prop('disabled', bool);
    });
}
function cleanAddressControls() {
    addressControls.forEach(function(item) {
        $(item).val("");
    });
}
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
  