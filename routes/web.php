<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'Shop\HomeController@index')->name('shop.home');



//ROTAS DO PAINEL DE CONTROLE (DASHBOARD)
Route::middleware(['auth', 'checkUserRole'])->group(function () {
        Route::get('dashboard/categoria/get', 'Dashboard\CategoriaController@getCategorias')->name('getCategorias');

        Route::get('dashboard/categoria/subcategorias', 'Dashboard\CategoriaController@getSubcategorias')->name('categoria.getSubcategorias');

        Route::resource('dashboard/categoria', 'Dashboard\CategoriaController')
        ->parameters([
                'categoria' => 'categoria',
        ])
        ->except('create', 'edit', 'show');


        Route::put('dashboard/subcategoria/{subcategoria}/categoria', 'Dashboard\SubcategoriaController@updateParent')->name('subcategoria.updateParent');

        Route::resource('dashboard/subcategoria', 'Dashboard\SubcategoriaController')
        ->parameters([
                'subcategoria' => 'subcategoria',
        ])
        ->only('store', 'destroy', 'update');
        

                
        //Rotas de atributos 
        Route::get('dashboard/atributos/get', 'Dashboard\AtributoController@getAtributos')->name('getAttributes');
        Route::resource('dashboard/atributo', 'Dashboard\AtributoController');

        Route::resource('dashboard/atributo.valores', 'Dashboard\ValorAtributoController')
                ->only('store', 'destroy')
                ->shallow();


        

        Route::PUT('dashboard/atributo/{atributo}/config', 'Dashboard\AtributoController@setConfig');

        

        //ATRIBUTOS DAS CONFIGURAÇÕES
        Route::POST('dashboard/produto/variation', 'Dashboard\ProdutoController@setVariation')->name('produto.setVariation');
        
        Route::POST('dashboard/produto/variation/generalAttribute', 'Dashboard\ProdutoController@setVariationsGeneralAttribute')->name('produto.setVariationsGeneralAttribute');

        Route::DELETE('dashboard/produto/variation', 'Dashboard\ProdutoController@destroyVariation')->name('produto.destroyVariation');

        // ATRIBUTOS DO PRODUTO
        Route::POST('dashboard/produto/generalAtribute', 'Dashboard\ProdutoController@setGeneralAttribute')->name('produto.setGeneralAttribute');
        
        Route::POST('dashboard/produto/simpleAttribute', 'Dashboard\ProdutoController@setSimpleAttribute')->name('produto.setSimpleAttribute');

        Route::PUT('dashboard/produto/multipleGeneralAttributes', 'Dashboard\ProdutoController@updateMultipleGeneralAttributes')->name('produto.updateMultipleGeneralAttributes');

        Route::PUT('dashboard/produto/{produto}/generalAttribute', 'Dashboard\ProdutoController@updateGeneralAttribute')->name('produto.updateGeneralAttribute');


        // FOTOS DO PRODUTO
        Route::POST('dashboard/produto/photos', 'Dashboard\ProdutoController@setPhotos')->name('produto.setPhotos');

        Route::POST('dashboard/produto/{produto}/photos/database', 'Dashboard\ProdutoController@setPhotosToDatabase')->name('produto.setPhotosToDatabase');

        Route::DELETE('dashboard/produto/photos', 'Dashboard\ProdutoController@destroyPhoto')->name('produto.destroyPhoto');
        
        Route::DELETE('dashboard/produto/photos/database', 'Dashboard\ProdutoController@destroyPhotoFromDatabase')->name('produto.destroyPhotoFromDatabase');


        // FOTO PRINCIPAL
        Route::POST('dashboard/produto/photos/main', 'Dashboard\ProdutoController@setMainPhoto')->name('produto.setMainPhoto');

        Route::PUT('dashboard/produto/{produto}/photos/main', 'Dashboard\ProdutoController@updateMainPhoto')->name('produto.updateMainPhoto');


       
        //CRUD Produtos
        Route::resource('dashboard/produto', 'Dashboard\ProdutoController')->except('edit');

});

//ROTAS DO SHOP
Route::get('shop/catalogo', 'Shop\ShopController')->name('shop.catalogo');

Route::get('shop/categorias', 'Shop\ShowCategorias')->name('shop.categorias');

Route::get('shop/produto/unavaliable', 'Shop\ShowProduto@unavaliable')->name('shop.produto.unavaliable');

Route::get('shop/produto/{produto}', 'Shop\ShowProduto@show')->name('shop.produto')->middleware('checkProductStatus');

Route::get('shop/produto/{produto}/avaliacoes', 'User\AvalController@index')->name('shop.produtos.avaliacoes');

Route::get('shop/produto/{produto}/variation', 'Dashboard\ProdutoController@getVariation')->name('produto.getVariation');


//ROTAS PERFIL DO USUÁRIO
Route::middleware(['auth'])->group(function () {
        //ROTAS PERFIL DE USUÁRIO
        Route::get('user/profile', 'UserController@show')->name('user.profile');

        Route::put('user/profile', 'UserController@update')->name('user.profile.update');

        Route::get('user/profile/edit', 'UserController@edit')->name('user.profile.edit');

        Route::resource('user/address', 'User\EnderecoUsuarioController')->except('show', 'index');

        Route::put('user/address/{address}/setMain', 'User\EnderecoUsuarioController@setMain')->name('address.setMain');

        Route::get('password/edit', 'UserController@editPassword')->middleware('password.confirm')->name('password.edit');

        Route::put('password', 'UserController@changePassword')->name('password.change');

        //ROTAS SACOLA DE PRODUTOS
        Route::get('user/bag', 'User\BagController@show')->name('user.bag')->middleware('checkUserBag');

        Route::get('user/bag/empty', 'User\BagController@empty')->name('user.bag.empty');

        Route::post('user/bag/produto', 'User\BagController@addProductToBag')->name('bag.produto.store');

        Route::delete('user/bag/produto/{produto}', 'User\BagController@removeProductFromBag')->name('bag.produto.delete');

        Route::put('user/bag/produto/{produto}/quantity', 'User\BagController@updateProductBagQuantity')->name('bag.produto.quantity');
        
        Route::post("user/bag/address", 'User\EnderecoUsuarioController@store')->name('bag.address.store');

        Route::put('user/bag/address/{address}', 'User\BagController@updateBagAddress')->name('bag.address.update');


        // ROTAS FAVORITOS
        Route::get('user/favorites', 'User\FavoritesController@index')->name('user.favorites');

        Route::POST('user/favorites/produto/{produto}', 'User\FavoritesController@addProductToFavorites')->name('favorites.produto.store');
        
        Route::DELETE('user/favorites/produto/{produto}', 'User\FavoritesController@removeProductFromFavorites')->name('favorites.produto.delete');


        // ROTAS DE PEDIDOS
        Route::get('user/orders', 'User\OrdersController@index')->name('user.orders');

        Route::get('user/orders/{order}', 'User\OrdersController@show')->name('user.orders.show');

        Route::put('shop/checkout/finish/shipping', 'Shop\Checkout@loadShippingValue')->name('shop.checkout.shipping');

        // ROTAS DE AVALIACOES
        Route::resource('user/orders.produto.avaliacao', 'User\AvalController')->except('index', 'show');

});

//ROTAS DE CHECKOUT
Route::middleware(['auth', 'checkProductsInBagStatus'])->group(function () {
        Route::get('shop/checkout/address', 'Shop\Checkout@address')->name('shop.checkout.address');

        Route::get('shop/checkout/personal-info', 'Shop\Checkout@personal_info')->name('shop.checkout.personal-info');

        Route::get('shop/checkout/finish', 'Shop\Checkout@finish')->middleware('checkUserInfo')->name('shop.checkout');

        Route::post('shop/checkout/finish', 'Shop\Checkout@order')->name('shop.checkout.order');
});
